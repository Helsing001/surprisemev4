import { Component } from '@angular/core';

import { Platform, NavController, ToastController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Router } from '@angular/router';
import { StorageServiceProvider, SettingsCache } from './services/other/storage-service/storage-service';
import { DataServiceProvider } from './services/other/data-service/data-service';
import { OauthServiceProvider } from './services/other/oauth-service/oauth-service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['./side-menu/side-menu.scss']
})
export class AppComponent {
  public appPages = [
    {
      title: 'Recommender',
      url: '/surprise-me-page',
      icon: 'gift'
    },
    {
      title: 'My Lists',
      url: '/my-lists-page',
      icon: 'list'
    },
    {
      title: 'Followed',
      url: '/followed',
      icon: 'man'
    },    
    {
      title: 'Search',
      url: '/search-page',
      icon: 'search'
    },
    {
      title: 'Listening',
      url: '/artist-info-page',
      icon: 'musical-note',
      state: { listening: 'true' }
    },
    {
      title: 'Results',
      url: '/queued-results-page',
      svg: './assets/sample-icons/food/reviews.svg',
      state: { fromMenu: 'true' }
    },
    {
      title: 'Settings',
      url: '/settings-page',
      icon: 'hammer'
    }
    ,
    {
      title: 'Walkthrough',
      url: '/getting-started-page',
      icon: 'walk'
    }
    ,
    {
      title: 'Support Me',
      url: '/support-me-page',
      icon: 'cafe'
    }    
  ];

  constructor(
    public platform: Platform,
    public splashScreen: SplashScreen,
    public statusBar: StatusBar,
    public router: Router,
    public navCtrl: NavController,
    public storageService: StorageServiceProvider,
    public toastCtrl: ToastController,
    public dataService: DataServiceProvider,
    public oauthService: OauthServiceProvider
  ) {
    this.initializeApp();
  }

  public itemTapped(page) {
    this.dataService.state = page.state;
    this.navCtrl.navigateForward(page.url, {
      state: page.state
    })
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      (window as any).handleOpenURL = url => this.oauthService.handleOAuthResponse(url);   
      // this.storageService.clearStorage();
    });
  }

}
