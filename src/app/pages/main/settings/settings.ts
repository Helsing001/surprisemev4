import { Component, Provider, ChangeDetectorRef, NgZone } from '@angular/core';
import { NavController, AlertController, ToastController, Platform, LoadingController } from '@ionic/angular';
import { orderBy } from 'lodash';
import { StorageServiceProvider } from '../../../services/other/storage-service/storage-service';
import { AlertInput } from '@ionic/core';
import { ProviderSettings } from '../../../models/providers/provider-settings';
import { GenericService } from '../../../models/generic/generic-service';
import { ProviderService } from '../../../services/other/provider-service/provider-service';
import { ProviderOperations } from '../../../models/providers/provider-operations';
import { MusicApp } from '../../../models/other/music-app';
import { SelectMusicAppsAlert } from '../../alerts/select-music-apps-alert';
import { OauthServiceProvider } from '../../../services/other/oauth-service/oauth-service';

declare var navigator: any;

@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
  styleUrls: [
    'settings.scss'
  ]
})
export class SettingsPage {

  public providerOperations = ProviderOperations;

  public localSourceWeight: number;
  public lastFmSourceWeight: number;
  public lastFmSimWeight: number;
  public spotifySourceWeight: number;
  public spotifySimWeight: number;
  public deezerSourceWeight: number;
  public deezerSimWeight: number;
  public keepAwakeLoading: boolean = true;
  public keepAwakeListening: boolean = true;
  public maxQueueHistory: number = 10;
  public discographyFilter: string = ""

  constructor(public navCtrl: NavController, public changeDetector: ChangeDetectorRef, public storageService: StorageServiceProvider,
    public alertCtrl: AlertController, public toastCtrl: ToastController, public providerService: ProviderService, public oauthService: OauthServiceProvider,
    public platform: Platform, public loadingController: LoadingController, public ngZone: NgZone) {
    this.localSourceWeight = providerService.getProviderSettings('Local').getSourceWeight();
    this.lastFmSourceWeight = providerService.getProviderSettings('Last.fm').getSourceWeight();
    this.lastFmSimWeight = providerService.getProviderSettings('Last.fm').getSimilarityWeight();
    this.spotifySourceWeight = providerService.getProviderSettings('Spotify').getSourceWeight();
    this.spotifySimWeight = providerService.getProviderSettings('Spotify').getSimilarityWeight();
    this.deezerSourceWeight = providerService.getProviderSettings('Deezer').getSourceWeight();
    this.deezerSimWeight = providerService.getProviderSettings('Deezer').getSimilarityWeight();

    this.storageService.getSettingsCache().then(settingsCache => {
      if (settingsCache) { this.keepAwakeLoading = settingsCache.keepAwakeLoading; }
      if (settingsCache) { this.keepAwakeListening = settingsCache.keepAwakeListening; }
      if (settingsCache && settingsCache.maxQueueHistory) { this.maxQueueHistory = settingsCache.maxQueueHistory; }
      if (settingsCache && settingsCache.discographyFilters) { this.discographyFilter = settingsCache.discographyFilters.join(','); }
    })
  }

  presentAllProviders() {
    var inputs: Array<AlertInput> = [];

    this.providerService.getAllProviderSettings().forEach((providerSettings) => {
      inputs.push(this.createCheckboxInput(providerSettings.name, providerSettings.name, this.providerService.isProviderEnabled(providerSettings.name), providerSettings.alwaysEnabled));
    })

    this.presentAlert('Providers', inputs).then((providers: Array<string>) => {
      this.providerService.setEnabledProviders(providers);
    });
  }

  presentLibraryProviders() {
    var inputs: Array<AlertInput> = [];

    this.providerService.getAllProvidersFor(ProviderOperations.LIBRARY).forEach((providerSettings) => {
      inputs.push(this.createCheckboxInput(providerSettings.name, providerSettings.name, this.providerService.isEnabledFor(providerSettings.name, ProviderOperations.LIBRARY), false));
    })

    this.presentAlert('Library Providers', inputs).then((providers: Array<string>) => {
      this.providerService.setProvidersFor(providers, ProviderOperations.LIBRARY);
    });
  }

  presentSourceProviders() {
    var inputs: Array<AlertInput> = [];

    this.providerService.getAllProvidersFor(ProviderOperations.SOURCE).forEach((providerSettings) => {
      inputs.push(this.createCheckboxInput(providerSettings.name, providerSettings.name, this.providerService.isEnabledFor(providerSettings.name, ProviderOperations.SOURCE), false));
    })

    this.presentAlert('Source Providers', inputs).then((providers: Array<string>) => {
      this.providerService.setProvidersFor(providers, ProviderOperations.SOURCE);
    });
  }

  presentSimilarityProviders() {
    var inputs: Array<AlertInput> = [];

    this.providerService.getAllProvidersFor(ProviderOperations.SIMILARITY).forEach((providerSettings) => {
      inputs.push(this.createCheckboxInput(providerSettings.name, providerSettings.name, this.providerService.isEnabledFor(providerSettings.name, ProviderOperations.SIMILARITY), false));
    })

    this.presentAlert('Similarity Providers', inputs).then((providers: Array<string>) => {
      if (providers.length != 0) {
        this.providerService.setProvidersFor(providers, ProviderOperations.SIMILARITY);
      }
      else {
        this.presentToast("At least one similarity provider needed");
      }
    });
  }

  presentInfoProviders() {
    var inputs: Array<AlertInput> = [];

    this.providerService.getAllProvidersFor(ProviderOperations.INFO).forEach((providerSettings) => {
      inputs.push(this.createRadioInput(providerSettings.name, providerSettings.name, this.providerService.isEnabledFor(providerSettings.name, ProviderOperations.INFO)));
    })

    this.presentAlert('Info Providers', inputs).then((provider: string) => {
      this.providerService.setProviderFor(provider, ProviderOperations.INFO);
    });
  }

  presentScrobbleProviders() {
    var inputs: Array<AlertInput> = [];

    this.providerService.getAllProvidersFor(ProviderOperations.SCROBBLE).forEach((providerSettings) => {
      inputs.push(this.createRadioInput(providerSettings.name, providerSettings.name, this.providerService.isEnabledFor(providerSettings.name, ProviderOperations.SCROBBLE)));
    })

    this.presentAlert('Scrobble Providers', inputs).then((provider: string) => {
      this.providerService.setProviderFor(provider, ProviderOperations.SCROBBLE);
    });
  }

  presentDiscographyProviders() {
    var inputs: Array<AlertInput> = [];

    this.providerService.getAllProvidersFor(ProviderOperations.DISCOGRAPHY).forEach((providerSettings) => {
      inputs.push(this.createRadioInput(providerSettings.name, providerSettings.name, this.providerService.isEnabledFor(providerSettings.name, ProviderOperations.DISCOGRAPHY)));
    })

    this.presentAlert('Discography Providers', inputs).then((provider: string) => {
      this.providerService.setProviderFor(provider, ProviderOperations.DISCOGRAPHY);
    });
  }

  presentMusicApps() {
    SelectMusicAppsAlert.presentMusicApps(this.alertCtrl, this.platform, this.loadingController, navigator, this.providerService)
      .then((selectedApps: Array<MusicApp>) => {
        this.ngZone.run(() => {
          this.providerService.setMusicApps(selectedApps);
          this.changeDetector.detectChanges();
        });
      })
  }

  setSourceWeight(event: CustomEvent, provider: string) {
    if (this.providerService.getProviderSettings(provider).getSourceWeight() !== event.detail.value) {
      this.providerService.getProviderSettings(provider).setSourceWeight(this.storageService, event.detail.value);
    }
  }

  setSimilarityWeight(event: CustomEvent, provider: string) {
    if (this.providerService.getProviderSettings(provider).getSimilarityWeight() !== event.detail.value) {
      this.providerService.getProviderSettings(provider).setSimilarityWeight(this.storageService, event.detail.value);
    }
  }

  setKeepAwake(event: CustomEvent, type: string) {
    var oldValue;
    switch (type) {
      case "loading": oldValue = this.storageService.settingsCache.keepAwakeLoading; break;
      case "listening": oldValue = this.storageService.settingsCache.keepAwakeListening; break;
    }
    if (oldValue !== event.detail.checked) {
      this.storageService.setKeepAwake(type, event.detail.checked);
    }
  }

  setMaxQueueHistory(event: CustomEvent) {
    var maxQueueHistory: number = Number(event.detail.value);
    if (maxQueueHistory > 0 && this.storageService.settingsCache.maxQueueHistory != maxQueueHistory) {
      this.storageService.setMaxQueueHistory(maxQueueHistory);
    }
    else if (maxQueueHistory <= 0) {
      this.presentToast("Value must be greater than 0");
    }
  }

  setDiscographyFilter(event: CustomEvent) {
    var filter: string = event.detail.value;
    var filters = filter.split(',');
    for (let i = 0; i < filters.length; i++) {
      var f = filters[i];
      var isValid = true;
      try {
        new RegExp(f);
      } catch (e) {
        this.presentToast(f + " is not a valid regular expression");
        break;
      }
    }
    this.storageService.setDiscographyFilters(filters);
  }

  presentAuth(service: string) {
    if (this.providerService.getProviderSettings(service).getUsername()) {
      this.alertCtrl.create({
        header: 'Are you sure you want to clear your credentials?',
        buttons: [
          {
            text: 'Cancel',
            role: 'cancel',
            handler: data => { }
          },
          {
            text: 'OK',
            handler: data => {
              this.ngZone.run(() => {
                var serviceProvider = this.providerService.getServiceProvider(service);
                serviceProvider.forgetAuth();
                this.providerService.disableAuthOperations(service);
                this.changeDetector.detectChanges();
              });
            }
          }
        ]
      }).then(alert => alert.present());
    }
    else {
      if (this.providerService.getServiceProvider(service).isLongAuth()) {
        const loading = this.loadingController.create({
          message: 'Please wait...'
        }).then(l => l.present().then(r => {
          this.providerService.getServiceProvider(service).auth()
            .then(result => {
              this.loadingController.dismiss();
              if (result) {
                this.providerService.enableDefaultSettings(service, false);
              }
            })
        }));
      }
      else {
        this.providerService.getServiceProvider(service).auth()
          .then(result => {
            if (result) {
              this.providerService.enableDefaultSettings(service, false);
            }
          });
      }
    }
  }

  forceAuthCode(service: string) {
    this.alertCtrl.create({
      header: 'Enter auth url response',
      inputs: [
        {
          name: 'url',
          placeholder: 'URL'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: data => { }
        },
        {
          text: 'OK',
          handler: data => {
            this.ngZone.run(() => {
              this.oauthService.handleOAuthResponse(data.url);
            });
          }
        }
      ]
    }).then(alert => alert.present());
  }

  createCheckboxInput(label, value, checked, disabled): AlertInput {
    var input = {
      type: 'checkbox',
      label: label,
      value: value,
      checked: checked,
      disabled: disabled
    };
    return input as AlertInput;
  }

  createRadioInput(label, value, checked): AlertInput {
    var input = {
      type: 'radio',
      label: label,
      value: value,
      checked: checked
    };
    return input as AlertInput;
  }

  createNumericInput(label, value): AlertInput {
    var input = {
      type: 'number',
      placeholder: label,
      value: value
    };
    return input as AlertInput;
  }

  presentAlert(title: string, inputs: Array<AlertInput>) {
    return new Promise(resolve => {
      this.alertCtrl.create({
        header: title,
        inputs: inputs,
        buttons: [
          'Cancel',
          {
            text: 'OK',
            handler: result => {
              resolve(result);
            }
          }
        ]
      }).then(alert => alert.present());
    });
  }

  presentToast(message: string) {
    this.toastCtrl.create({
      message: message,
      duration: 3000,
      position: 'bottom'
    }).then(toast => toast.present());
  }

}
