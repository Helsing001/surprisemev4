import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';


import { GettingStartedPage } from './getting-started.page';
import { RouterModule } from '@angular/router';
import { ShellModule } from '../../../shell/shell.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ShellModule,
    RouterModule.forChild([
      {
        path: '',
        component: GettingStartedPage
      }
    ])    
  ],
  declarations: [GettingStartedPage]
})
export class GettingStartedPageModule {}
