import { Component, OnInit } from '@angular/core';
import { ProviderService } from '../../../services/other/provider-service/provider-service';
import { AlertInput } from '@ionic/core';
import { AlertController, ToastController, Platform, LoadingController, NavController } from '@ionic/angular';
import { concat, forkJoin } from 'rxjs';
import { Router } from '@angular/router';
import { StorageServiceProvider } from '../../../services/other/storage-service/storage-service';
import { SelectMusicAppsAlert } from '../../alerts/select-music-apps-alert';
import { MusicApp } from '../../../models/other/music-app';

@Component({
  selector: 'app-getting-started',
  templateUrl: './getting-started.page.html',
  styleUrls: ['./getting-started.page.scss'],
})
export class GettingStartedPage implements OnInit {

  constructor(public alertCtrl: AlertController, public toastCtrl: ToastController, public router: Router, public platform: Platform,
    public navCtrl: NavController, public loadingController: LoadingController, public providerService: ProviderService) { }

  ngOnInit() {
  }

  public appRanBefore() {
    var appRanBefore = false;
    this.providerService.getAllLogInProviders().forEach(providerSettings => {
      if (providerSettings && providerSettings.getUsername()) {
        appRanBefore = true;
      }
    });
    return appRanBefore;
  }

  public providersLogIn() {
    var inputs: Array<AlertInput> = [];

    this.providerService.getAllLogInProviders().forEach((providerSettings) => {
      inputs.push(this.createCheckboxInput(providerSettings.name, providerSettings.name, false, false));
    })

    this.presentAlert('Providers', inputs).then((providers: Array<string>) => {
      this.authFirstProviderInList(providers)
        .catch(error => {
          this.presentToast("An error occurred");
        })
        .finally(() => {
          this.navCtrl.navigateForward('/');
        });
    });
  }

  public authFirstProviderInList(providers: Array<string>) {
    return new Promise((resolve, error) => {
      if (this.providerService.getServiceProvider(providers[0]).isLongAuth()) {
        const loading = this.loadingController.create({
          message: 'Please wait...'
        }).then(l => l.present().then(r => {
          this.callAuth(providers, true).then(r => resolve(r)).catch(e => error(e));
        }))
      }
      else {
        this.callAuth(providers, false).then(r => resolve(r)).catch(e => error(e));
      }
    });
  }

  public callAuth(providers: Array<string>, longAuth: boolean) {
    return new Promise((resolve, error) => {
      this.providerService.getServiceProvider(providers[0]).auth()
        .then(result => {
          if (longAuth) {
            this.loadingController.dismiss();
          }
          if (result) {
            this.providerService.enableDefaultSettings(providers[0]);
          }
          if (providers.length > 1) {
            this.authFirstProviderInList(providers.slice(1)).then(r => resolve(1)).catch(e => error(e));
          }
          else {
            resolve(1);
          }
        })
        .catch(e => error(e));
    });
  }

  public selectMusicApps() {
    SelectMusicAppsAlert.presentMusicApps(this.alertCtrl, this.platform, this.loadingController, navigator, this.providerService)
      .then((selectedApps: Array<MusicApp>) => {
        this.providerService.setMusicApps(selectedApps);
      })
  }

  presentAlert(title: string, inputs: Array<AlertInput>) {
    return new Promise(resolve => {
      this.alertCtrl.create({
        header: title,
        inputs: inputs,
        buttons: [
          'Cancel',
          {
            text: 'OK',
            handler: result => {
              resolve(result);
            }
          }
        ]
      }).then(alert => alert.present());
    });
  }

  createCheckboxInput(label, value, checked, disabled): AlertInput {
    var input = {
      type: 'checkbox',
      label: label,
      value: value,
      checked: checked,
      disabled: disabled
    };
    return input as AlertInput;
  }

  public presentToast(message: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 2000,
      position: 'bottom'
    }).then(toast => toast.present());
  }

}
