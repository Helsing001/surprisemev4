import { Component } from '@angular/core';
import { NavController, ToastController } from '@ionic/angular';
import { SearchServiceProvider } from '../../../services/main/search-service/search-service';
import { FullArtist } from '../../../models/artist/full-artist';
import { ArtistInfoPage } from '../../secondary/artist-info/artist-info';
import { ThousandSuffixesPipePipe } from '../../../pipes/genre-filter/thousand-suffixes-pipe';
import { first } from 'rxjs/operators';
import { DataServiceProvider } from '../../../services/other/data-service/data-service';

/**
 * Generated class for the SearchPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-search',
  templateUrl: 'search.html',
  providers: [ThousandSuffixesPipePipe],
  styleUrls: [
    'search.scss'
  ],
})
export class SearchPage {

  public queryText;
  public resultArtists: Array<FullArtist>;

  constructor(public navCtrl: NavController, public searchServiceProvider: SearchServiceProvider, public dataService: DataServiceProvider,
    public toastCtrl: ToastController, public thousandSuff: ThousandSuffixesPipePipe) {
  }

  searchArtist() {
    if (this.queryText) {
      this.searchServiceProvider.searchForName(this.queryText)
        .then((recArtists: Array<FullArtist>) => {
          this.resultArtists = recArtists;
        })
        .catch(err => {
          this.presentToast(err);
        });
    }
    else {
      this.resultArtists = undefined;
    }
  }

  public itemTapped(item) {
    var localState = {
      fullArtist: item,
      simpleSearch: true
    };
    this.dataService.state = localState;
    this.navCtrl.navigateForward('artist-info-page');
  }

  public formatListeners(resultArtist: FullArtist) {
    var listeners = resultArtist.listeners;
    if (listeners) {
      var firstFormat = this.thousandSuff.transform(listeners, '2');
      if (firstFormat) {
        var finalFormat = firstFormat.concat(' listeners');
        return finalFormat;
      }
    }
    return "";
  }

  public presentToast(message: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 2000,
      position: 'bottom'
    }).then(toast => toast.present());
  }

}
