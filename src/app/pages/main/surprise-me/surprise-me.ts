import { Component, ViewChild, ChangeDetectorRef } from '@angular/core';
import { NavController, AlertController, ToastController, MenuController, IonSelect, ModalController, LoadingController } from '@ionic/angular';
import { LoadingPage } from '../../secondary/loading/loading'
import { RecResultsPage } from '../../secondary/rec-results/rec-results'
import { LastFmServiceProvider } from '../../../services/main/last-fm-service/last-fm-service'
import { BasicArtist } from '../../../models/artist/basic-artist';
import { FullArtist } from '../../../models/artist/full-artist'
import { SpotifyServiceProvider } from '../../../services/main/spotify-service/spotify-service';
import { SearchServiceProvider } from '../../../services/main/search-service/search-service';
import { SearchParams, GenericParams, LastFmParams, SpotifyParams, LocalParams, DeezerParams } from '../../../models/params/search-params';
import { MylistsServiceProvider } from '../../../services/other/mylists-service/mylists-service';
import { MyLists } from '../../../models/list/my-lists';
import { ProviderService } from '../../../services/other/provider-service/provider-service';
import { LocalServiceProvider } from '../../../services/main/local-service/local-service';
import { NavigationOptions } from '@ionic/angular/dist/providers/nav-controller';
import { Router } from '@angular/router';
import { CalendarModalOptions, CalendarModal, CalendarResult } from 'ion2-calendar';
import { DatePipe } from '@angular/common';
import { ExternalList } from '../../../models/list/external-list';
import { AlertInput } from '@ionic/core';
import { ProviderOperations } from '../../../models/providers/provider-operations';
import { LoadingServiceProvider } from '../../../services/other/loading-service/loading-service';
import { clone, orderBy } from 'lodash';
import { LoadingQueueServiceProvider } from '../../../services/other/loading-queue-service/loading-queue-service';
import { GenericService } from '../../../models/generic/generic-service';
import { ProviderSettings } from '../../../models/providers/provider-settings';
import { StorageServiceProvider, SettingsCache } from '../../../services/other/storage-service/storage-service';
import { QueuedParams } from '../../../models/params/queued-params';
import { GenerateArtistAlert } from '../../alerts/generate-artist-alert';
import { DeezerServiceProvider } from '../../../services/main/deezer-service/deezer-service';
import { RefreshDiscoAlert } from '../../alerts/refresh-disco-alert';
import { SearchTemplate } from '../../../models/params/search-template';

@Component({
  selector: 'page-home',
  templateUrl: 'surprise-me.html',
  providers: [DatePipe],
  styleUrls: [
    'surprise-me.scss'
  ]
})
export class SupriseMePage {

  @ViewChild('localSources', undefined) localSourcesSelect: IonSelect;
  @ViewChild('lastFmSources', undefined) lastFmSourcesSelect: IonSelect;
  @ViewChild('spotifySources', undefined) spotifySourcesSelect: IonSelect;
  @ViewChild('deezerSources', undefined) deezerSourcesSelect: IonSelect;

  public providerOperations = ProviderOperations;

  public allParams: Array<Params> = new Array();
  public genericParams = new GenericParams();
  public lastFmParams: Array<LastFmParams> = new Array();
  public spotifyParams: Array<SpotifyParams> = new Array();
  public deezerParams: Array<DeezerParams> = new Array();
  public localParams: Array<LocalParams> = new Array();

  public selectedLastFmParams: Params = new Params(LastFmServiceProvider.PROVIDER, "dummy", "dummy");
  public selectedSpotifyParams: Params = new Params(SpotifyServiceProvider.PROVIDER, "dummy", "dummy");
  public selectedDeezerParams: Params = new Params(DeezerServiceProvider.PROVIDER, "dummy", "dummy");
  public selectedLocalParams: Params = new Params(LocalServiceProvider.PROVIDER, "dummy", "dummy");

  public searchTemplates: Array<SearchTemplate>;
  public selectedTemplate = "No Template" as any;

  public settingsCache: SettingsCache;
  public myLists: MyLists;
  public runState: number = 0; // 0 -> not set; 1 -> first run; 2 -> not first run

  constructor(public navCtrl: NavController, public providerService: ProviderService, public toastCtrl: ToastController,
    public alertCtrl: AlertController, public changeDetector: ChangeDetectorRef, public loadingQueueProvider: LoadingQueueServiceProvider,
    public myListsService: MylistsServiceProvider, public spotifyProvider: SpotifyServiceProvider,
    public deezerProvider: DeezerServiceProvider, public loadingService: LoadingServiceProvider,
    public menu: MenuController, public modalCtrl: ModalController, public router: Router, public datepipe: DatePipe,
    public storageService: StorageServiceProvider, public loadingController: LoadingController) {

  }

  ionViewWillEnter() {
    if (this.runState == 0) {
      const loading = this.loadingController.create({
        message: 'Initialising...'
      }).then(l => l.present().then(r => {
        this.myListsService.getMyLists().then(myLists => {
          this.myLists = myLists;
          this.initProviders();
          this.storageService.getSettingsCache().then(settingsCache => {
            this.searchTemplates = settingsCache.searchTemplates;
            this.settingsCache = settingsCache;
            this.loadingController.dismiss();
            if (!settingsCache.appRanBefore) {
              this.runState = 1;
              this.storageService.setAppRanBefore();
              this.navCtrl.navigateForward('/getting-started-page');
            }
            else {
              this.runState = 2;
            }
          });
        });
      }));
    }
    else if (this.runState == 1) {
      this.initProviders();
      this.runState = 2;
    }
    else {
      this.checkAndRemoveDisabledProviders();
    }
  }

  public initProviders() {
    this.providerService.whenReady().then(() => {
      this.lastFmParams = new Array();
      this.spotifyParams = new Array();
      this.deezerParams = new Array();
      this.localParams = new Array();
      if (this.providerService.isEnabledFor(LastFmServiceProvider.PROVIDER, ProviderOperations.SOURCE)) {
        this.lastFmParams.push(new LastFmParams(-1, this.providerService.getProviderSettings(LastFmServiceProvider.PROVIDER).getSourceWeight()));
        this.selectedLastFmParams = new Params(LastFmServiceProvider.PROVIDER, this.lastFmParams[0], this.lastFmParams[0].lastfmPeriod);
      }
      if (this.providerService.isEnabledFor(SpotifyServiceProvider.PROVIDER, ProviderOperations.SOURCE)) {
        this.spotifyParams.push(new SpotifyParams(-2, this.providerService.getProviderSettings(SpotifyServiceProvider.PROVIDER).getSourceWeight()));
        this.selectedSpotifyParams = new Params(SpotifyServiceProvider.PROVIDER, this.spotifyParams[0], this.spotifyParams[0].spotifySource);
      }
      if (this.providerService.isEnabledFor(LocalServiceProvider.PROVIDER, ProviderOperations.SOURCE)) {
        this.localParams.push(new LocalParams(-3, this.providerService.getProviderSettings(LocalServiceProvider.PROVIDER).getSourceWeight()));
        this.selectedLocalParams = new Params(LocalServiceProvider.PROVIDER, this.localParams[0], undefined);
      }
      if (this.providerService.isEnabledFor(DeezerServiceProvider.PROVIDER, ProviderOperations.SOURCE)) {
        this.deezerParams.push(new DeezerParams(-4, this.providerService.getProviderSettings(DeezerServiceProvider.PROVIDER).getSourceWeight()));
        this.selectedDeezerParams = new Params(DeezerServiceProvider.PROVIDER, this.deezerParams[0], this.deezerParams[0].deezerSource);
      }
      this.allParams = this.getOrderedProviderParams();
    })
  }

  public checkAndRemoveDisabledProviders() {
    var enabledProviders = this.providerService.getEnabledProviderSettings();
    //nu se verifica local si last.fm pt ca nu pot fi dezactivate
    if (!enabledProviders.find(p => p.name === SpotifyServiceProvider.PROVIDER)) {
      if (this.spotifyParams.length > 0) this.spotifyParams = new Array();
      this.selectedSpotifyParams = new Params(SpotifyServiceProvider.PROVIDER, "dummy", "dummy");
      this.allParams = this.getOrderedProviderParams();
    }
    if (!enabledProviders.find(p => p.name === DeezerServiceProvider.PROVIDER)) {
      if (this.deezerParams.length > 0) this.deezerParams = new Array();
      this.selectedDeezerParams = new Params(DeezerServiceProvider.PROVIDER, "dummy", "dummy");
      this.allParams = this.getOrderedProviderParams();
    }
  }

  public getOrderedProviderParams() {
    var allParams = this.localParams.map(localParam => new Params(LocalServiceProvider.PROVIDER, localParam, undefined))
      .concat(this.spotifyParams.map(spotifyParam => new Params(SpotifyServiceProvider.PROVIDER, spotifyParam, spotifyParam.spotifySource)))
      .concat(this.deezerParams.map(deezerParam => new Params(DeezerServiceProvider.PROVIDER, deezerParam, deezerParam.deezerSource)))
      .concat(this.lastFmParams.map(lastParam => new Params(LastFmServiceProvider.PROVIDER, lastParam, lastParam.lastfmPeriod)));
    return allParams;
  }

  public search() {
    if (this.getSearchButtonDisplay() === 'SEARCH') {
      this.loadingService.minimized = false;
      this.navCtrl.navigateForward('loading-page', { skipLocationChange: true });
    }
    else {
      this.presentToast("Queued");
    }
    // BUG, nu tine cont de skip la back; alternativa, care nici ea nu merge:
    // this.router.navigateByUrl('loading-page', { replaceUrl: true, skipLocationChange: true } );
    this.callSearchProvider();
  }

  public callSearchProvider() {
    var searchParams = new SearchParams(clone(this.genericParams), clone(this.lastFmParams), clone(this.spotifyParams), clone(this.localParams), clone(this.deezerParams));
    this.loadingQueueProvider.queueSearch(searchParams)
      .then(recArtists => {
        if (!this.loadingService.minimized) {
          this.navCtrl.navigateForward('rec-results-page', {
            state: {
              previousUrl: 'surprise-me-page',
              fullArtists: recArtists,
              simpleSearch: searchParams.genericParams.simpleSearch
            }
          });
        }
      })
      .catch(err => {
        if (!this.loadingService.minimized) {
          this.navCtrl.pop();
          this.presentToast(err);
        }
      });
  }

  public openSideMenu() {
    this.menu.toggle('side');
  }

  public openLoadingOrLastResult() {
    var lastResult = this.loadingQueueProvider.getLastResult();
    if (this.loadingService.searchTimestamp === -1 && lastResult) {
      if (QueuedParams.TYPE_SEARCH === lastResult.type) {
        this.navCtrl.navigateForward('rec-results-page', {
          state: {
            previousUrl: 'surprise-me-page',
            fullArtists: lastResult.searchResults,
            simpleSearch: lastResult.queueParams.searchParams.genericParams.simpleSearch
          }
        });
      }
      else if (QueuedParams.TYPE_GENERATE === lastResult.type) {
        GenerateArtistAlert.showGenerateResult(this.alertCtrl, this.toastCtrl, lastResult.generateResult);
      }
      else if (QueuedParams.TYPE_REFRESH === lastResult.type) {
        RefreshDiscoAlert.showArtistsWithNewAlbums(this.alertCtrl, lastResult.refreshResult);
      }
    }
    else if (this.loadingService.searchTimestamp !== -1) {
      this.navCtrl.navigateForward('loading-page', { skipLocationChange: true });
    }
  }

  public openSearchQueue() {
    this.navCtrl.navigateForward('queued-results-page');
  }

  public openLastResultOrLoading() {
    var lastResult = this.loadingQueueProvider.getLastResult();
    if (lastResult) {
      if (QueuedParams.TYPE_SEARCH === lastResult.type) {
        this.navCtrl.navigateForward('rec-results-page', {
          state: {
            previousUrl: 'surprise-me-page',
            fullArtists: lastResult.searchResults,
            simpleSearch: lastResult.queueParams.searchParams.genericParams.simpleSearch
          }
        });
      }
      else if (QueuedParams.TYPE_GENERATE === lastResult.type) {
        GenerateArtistAlert.showGenerateResult(this.alertCtrl, this.toastCtrl, lastResult.generateResult);
      }
      else if (QueuedParams.TYPE_REFRESH === lastResult.type) {
        RefreshDiscoAlert.showArtistsWithNewAlbums(this.alertCtrl, lastResult.refreshResult);
      }
    }
    else if (this.loadingService.searchTimestamp !== -1) {
      this.navCtrl.navigateForward('loading-page', { skipLocationChange: true });
    }
  }

  public checkSearchDisabled() {
    return this.allParams.length == 0;
  }

  public presentToast(message: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 2000,
      position: 'bottom'
    }).then(toast => toast.present());
  }

  public displaySources(event: UIEvent, params: Params, clickOnSource: boolean = false) {
    switch (params.type) {
      case 'Local':
        this.selectedLocalParams = params;
        this.changeDetector.detectChanges();
        this.localSourcesSelect.open(); break;
      case 'Last.fm':
        this.selectedLastFmParams = params;
        this.changeDetector.detectChanges();
        if (clickOnSource && (params.providerParams.lastfmPeriod === 'Custom' || params.providerParams.lastfmPeriod === 'Tag'))
          this.sourceChange(event, LastFmServiceProvider.PROVIDER);
        else
          this.lastFmSourcesSelect.open(event); break;
      case 'Spotify':
        this.selectedSpotifyParams = params;
        this.changeDetector.detectChanges();
        if (clickOnSource && params.providerParams.spotifySource === 'Followed List')
          this.displayPrivateLists(this.spotifyProvider);
        else
          this.spotifySourcesSelect.open(event); break;
      case 'Deezer':
        this.selectedDeezerParams = params;
        this.changeDetector.detectChanges();
        if (clickOnSource && params.providerParams.deezerSource === 'Followed List')
          this.displayPrivateLists(this.deezerProvider);
        else
          this.deezerSourcesSelect.open(event); break;
    }
  }

  public getImage(provider: string) {
    return this.providerService.getServiceProvider(provider).getProviderImage(true);
  }

  public sourceChange(event: UIEvent, provider: string) {
    switch (provider) {
      case 'Last.fm':
        var lastFmParams: LastFmParams = this.selectedLastFmParams.providerParams;
        if (lastFmParams.lastfmPeriod === 'Custom') this.openCalendarLastFm();
        else if (lastFmParams.lastfmPeriod === 'Tag') this.openTagAlert().then((tag: string) => { lastFmParams.tag = tag; });
        else this.selectedLastFmParams.oldSource = lastFmParams.lastfmPeriod;
        break;
      case 'Spotify':
        var spotifyParams: SpotifyParams = this.selectedSpotifyParams.providerParams;
        if (spotifyParams.spotifySource === 'Followed List') this.displayPrivateLists(this.spotifyProvider);
        else this.selectedSpotifyParams.oldSource = spotifyParams.spotifySource;
        break;
      case 'Deezer':
        var deezerParams: DeezerParams = this.selectedDeezerParams.providerParams;
        if (deezerParams.deezerSource === 'Followed List') this.displayPrivateLists(this.deezerProvider);
        else this.selectedDeezerParams.oldSource = deezerParams.deezerSource;
        break;
    }
  }

  public openCalendarLastFm() {
    const options: CalendarModalOptions = {
      pickMode: 'range',
      title: 'Last.fm Range',
      color: 'secondary',
      to: new Date(),
      canBackwardsSelected: true
    };

    this.modalCtrl.create({
      component: CalendarModal,
      componentProps: { options }
    }).then(modal => {
      modal.onDidDismiss().then(event => {
        var lastFmParams: LastFmParams = this.selectedLastFmParams.providerParams;
        if (event.role === 'done') {
          lastFmParams.from = event.data.from.unix;
          lastFmParams.to = event.data.to.unix;
          this.selectedLastFmParams.oldSource = lastFmParams.lastfmPeriod;
        }
        else {
          lastFmParams.lastfmPeriod = this.selectedLastFmParams.oldSource;
        }
      });
      modal.present()
    });
  }

  public displayPrivateLists(provider: GenericService) {
    const loading = this.loadingController.create({
      message: 'Fetching lists...'
    }).then(l => l.present().then(r => {
      provider.fetchPrivateLists()
        .then((externalLists: Array<ExternalList>) => {
          var inputs: Array<AlertInput> = [];
          var oneValueSelected = false;
          externalLists.forEach(playlist => {
            var shouldSelect = this.isPlaylistSelected(provider, playlist);
            if (shouldSelect) oneValueSelected = true;
            inputs.push(this.createRadioInput(playlist.name, playlist, shouldSelect));
          });
          if (!oneValueSelected && inputs.length > 0) {
            inputs[0].checked = true;
          }
          this.loadingController.dismiss();
          this.presentAlert('Playlists', inputs)
            .then((selectedList: ExternalList) => {
              if (selectedList) {
                this.updatePlaylist(provider, selectedList);
              }
            })
            .catch(() => this.rollbackSource(provider))
        })
        .catch(error => {
          this.loadingController.dismiss();
          this.presentToast(error);
        })
    }));
  }

  public isPlaylistSelected(provider: GenericService, playlist: ExternalList): boolean {
    var selected = false;
    switch (provider.getOwnName()) {
      case SpotifyServiceProvider.PROVIDER:
        var spotifyParams: SpotifyParams = this.selectedSpotifyParams.providerParams;
        selected = spotifyParams.playlist && playlist.id === spotifyParams.playlist.id;
        break;
      case DeezerServiceProvider.PROVIDER:
        var deezerParams: DeezerParams = this.selectedDeezerParams.providerParams;
        selected = deezerParams.playlist && playlist.id === deezerParams.playlist.id;
        break;
      default: console.log("Unknown provider " + provider.getOwnName());
    }
    return selected;
  }

  public updatePlaylist(provider: GenericService, selectedList: ExternalList) {
    switch (provider.getOwnName()) {
      case SpotifyServiceProvider.PROVIDER:
        var spotifyParams: SpotifyParams = this.selectedSpotifyParams.providerParams;
        spotifyParams.playlist = selectedList;
        this.selectedSpotifyParams.oldSource = spotifyParams.spotifySource;
        break;
      case DeezerServiceProvider.PROVIDER:
        var deezerParams: DeezerParams = this.selectedDeezerParams.providerParams;
        deezerParams.playlist = selectedList;
        this.selectedDeezerParams.oldSource = deezerParams.deezerSource;
        break;
    }
  }

  public rollbackSource(provider: GenericService) {
    switch (provider.getOwnName()) {
      case SpotifyServiceProvider.PROVIDER:
        var spotifyParams: SpotifyParams = this.selectedSpotifyParams.providerParams;
        spotifyParams.spotifySource = this.selectedSpotifyParams.oldSource;
        break;
      case DeezerServiceProvider.PROVIDER:
        var deezerParams: DeezerParams = this.selectedDeezerParams.providerParams;
        deezerParams.deezerSource = this.selectedDeezerParams.oldSource;
        break;
    }
  }

  public getParamsDisplay(params: Params) {
    switch (params.type) {
      case 'Last.fm': return this.getLastDisplay(params.providerParams);
      case 'Spotify': return this.getSpotifyDisplay(params.providerParams);
      case 'Deezer': return this.getDeezerDisplay(params.providerParams);
    }
  }

  public getLastDisplay(lastFmParams: LastFmParams) {
    if (lastFmParams.lastfmPeriod !== 'Custom' && lastFmParams.lastfmPeriod !== 'Tag') return lastFmParams.lastfmPeriod;
    if (lastFmParams.lastfmPeriod === 'Custom') {
      if (lastFmParams.from && lastFmParams.to) {
        var dateFrom = new Date(lastFmParams.from * 1000);
        var dateTo = new Date(lastFmParams.to * 1000);
        return this.datepipe.transform(dateFrom, 'dd MMM yy') + "-" + this.datepipe.transform(dateTo, 'dd MMM yy');
      }
      else return "";
    }
    else if (lastFmParams.lastfmPeriod === 'Tag') {
      return lastFmParams.tag;
    }
  }

  public getSpotifyDisplay(spotifyParams: SpotifyParams) {
    if (spotifyParams.spotifySource !== 'Followed List') return spotifyParams.spotifySource;
    else {
      if (spotifyParams.playlist) return spotifyParams.playlist.name;
      else return "";
    }
  }

  public getDeezerDisplay(deezerParams: DeezerParams) {
    if (deezerParams.deezerSource !== 'Followed List') return deezerParams.deezerSource;
    else {
      if (deezerParams.playlist) return deezerParams.playlist.name;
      else return "";
    }
  }

  public getSearchButtonDisplay() {
    if (this.loadingQueueProvider.queueParamsArray.length > 0) {
      return 'QUEUE';
    }
    else {
      return 'SEARCH';
    }
  }

  public displayProviders() {
    var providers = this.providerService.getAllProvidersFor(ProviderOperations.SOURCE);
    var inputs: Array<AlertInput> = [];
    providers.forEach(provider => {
      inputs.push(this.createRadioInput(provider.name, provider, provider.name === LocalServiceProvider.PROVIDER));
    });
    this.presentAlert('Providers', inputs)
      .then((provider: ProviderSettings) => {
        switch (provider.name) {
          case LastFmServiceProvider.PROVIDER: this.lastFmParams.push(new LastFmParams(new Date().getTime().valueOf(),
            this.providerService.getProviderSettings(LastFmServiceProvider.PROVIDER).getSourceWeight())); break;
          case SpotifyServiceProvider.PROVIDER: this.spotifyParams.push(new SpotifyParams(new Date().getTime().valueOf(),
            this.providerService.getProviderSettings(SpotifyServiceProvider.PROVIDER).getSourceWeight())); break;
          case DeezerServiceProvider.PROVIDER: this.deezerParams.push(new DeezerParams(new Date().getTime().valueOf(),
            this.providerService.getProviderSettings(DeezerServiceProvider.PROVIDER).getSourceWeight())); break;
          case LocalServiceProvider.PROVIDER: this.localParams.push(new LocalParams(new Date().getTime().valueOf(),
            this.providerService.getProviderSettings(LocalServiceProvider.PROVIDER).getSourceWeight())); break;
        }
        this.allParams = this.getOrderedProviderParams();
      })
      .catch((e) => { });
  }

  public setWeight(params: Params) {
    var inputs: Array<AlertInput> = [];
    inputs.push(this.createNumberInput("Weight", params.providerParams.weight));
    this.presentAlert('Weight', inputs)
      .then(result => {
        var weight = Number(result[0]);
        if (weight > 0 && weight <= 100) {
          params.providerParams.weight = Number(result[0]);
        }
        else {
          this.presentToast("Weight should be between 0 and 100");
        }
      })
      .catch(e => { });
  }

  public removeProvider(params: Params) {
    this.allParams = this.allParams.filter(param => param.providerParams.id != params.providerParams.id);
    switch (params.type) {
      case LastFmServiceProvider.PROVIDER: this.lastFmParams = this.lastFmParams.filter(param => param.id != params.providerParams.id); break;
      case SpotifyServiceProvider.PROVIDER: this.spotifyParams = this.spotifyParams.filter(param => param.id != params.providerParams.id); break;
      case DeezerServiceProvider.PROVIDER: this.deezerParams = this.deezerParams.filter(param => param.id != params.providerParams.id); break;
      case LocalServiceProvider.PROVIDER: this.localParams = this.localParams.filter(param => param.id != params.providerParams.id); break;
    }
  }

  public templateChange(event: any) {
    if (event !== "No Template") {
      console.log(event)
      this.updateParamsFromTemplate();
    }
  }

  public async saveTemplate() {
    if (this.selectedTemplate === "No Template") {
      var templateName = await this.openTemplateAlert() as string;
      if(this.searchTemplates.find(s => s.name === templateName)) {
        this.presentToast("A template with this name already exists");
        return;
      }
      var searchParams = new SearchParams(clone(this.genericParams), clone(this.lastFmParams), clone(this.spotifyParams), clone(this.localParams), clone(this.deezerParams));
      var searchTemplate = new SearchTemplate(templateName, searchParams);
      this.storageService.saveSearchTemplate(searchTemplate);
      this.selectedTemplate = searchTemplate;
    }
    else {
      var template = this.selectedTemplate as SearchTemplate;
      template.searchParams = new SearchParams(clone(this.genericParams), clone(this.lastFmParams), clone(this.spotifyParams), clone(this.localParams), clone(this.deezerParams));
      this.storageService.saveSettingsToStorage();
    }
  }

  public async deleteTemplate() {
    try {
      await this.presentAlert("Delete template?", []) as string;
      this.storageService.deleteSearchTemplate(this.selectedTemplate);
      this.selectedTemplate = "No Template" as any;
      this.searchTemplates = this.settingsCache.searchTemplates;
    } catch (e) { }
  }

  public updateParamsFromTemplate() {
    var template = this.selectedTemplate as SearchTemplate;
    this.genericParams = clone(template.searchParams.genericParams);
    this.lastFmParams = clone(template.searchParams.lastFmParams);
    this.spotifyParams = clone(template.searchParams.spotifyParams);
    this.localParams = clone(template.searchParams.localParams);
    this.deezerParams = clone(template.searchParams.deezerParams);
    this.allParams = this.getOrderedProviderParams();
    if (this.lastFmParams.length > 0) this.selectedLastFmParams = new Params(LastFmServiceProvider.PROVIDER, this.lastFmParams[0], this.lastFmParams[0].lastfmPeriod);
    if (this.spotifyParams.length > 0) this.selectedSpotifyParams = new Params(SpotifyServiceProvider.PROVIDER, this.spotifyParams[0], this.spotifyParams[0].spotifySource);
    if (this.localParams.length > 0) this.selectedLocalParams = new Params(LocalServiceProvider.PROVIDER, this.localParams[0], undefined);
    if (this.deezerParams.length > 0) this.selectedDeezerParams = new Params(DeezerServiceProvider.PROVIDER, this.deezerParams[0], this.deezerParams[0].deezerSource);
  }

  public async openTemplateAlert() {
    return new Promise(resolve => {
      let alert = this.alertCtrl.create({
        header: 'Template',
        inputs: [
          {
            name: 'name',
            placeholder: 'Name'
          }
        ],
        buttons: [
          {
            text: 'Cancel',
            role: 'cancel'
          },
          {
            text: 'OK',
            handler: data => {
              resolve(data.name);
            }
          }
        ]
      }).then(alert => alert.present());
    });
  }

  public createRadioInput(label, value, checked): AlertInput {
    var input = {
      type: 'radio',
      label: label,
      value: value,
      checked: checked
    };
    return input as AlertInput;
  }

  public createNumberInput(label, value): AlertInput {
    var input = {
      type: 'number',
      placeholder: label,
      value: value,
      min: 0
    };
    return input as AlertInput;
  }

  public openTagAlert() {
    return new Promise(resolve => {
      var lastFmParams: LastFmParams = this.selectedLastFmParams.providerParams;
      let alert = this.alertCtrl.create({
        header: 'Tag',
        inputs: [
          {
            name: 'tag',
            placeholder: 'Tag'
          }
        ],
        buttons: [
          {
            text: 'Cancel',
            role: 'cancel',
            handler: data => {
              lastFmParams.lastfmPeriod = this.selectedLastFmParams.oldSource;
              this.changeDetector.detectChanges();
            }
          },
          {
            text: 'OK',
            handler: data => {
              this.selectedLastFmParams.oldSource = lastFmParams.lastfmPeriod;
              resolve(data.tag);
            }
          }
        ]
      }).then(alert => alert.present());
    });
  }

  public presentAlert(title: string, inputs: Array<AlertInput>) {
    return new Promise((resolve, error) => {
      this.alertCtrl.create({
        header: title,
        inputs: inputs,
        buttons: [
          {
            text: 'Cancel',
            role: 'cancel',
            handler: result => {
              error();
            }
          },
          {
            text: 'OK',
            handler: result => {
              resolve(result);
            }
          }
        ]
      }).then(alert => alert.present());
    });
  }

}

class Params {
  constructor(public type: string, public providerParams: any, public oldSource: string) { }
}
