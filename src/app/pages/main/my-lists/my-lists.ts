import { Component } from '@angular/core';
import { NavController, PopoverController } from '@ionic/angular';
import { MyLists } from '../../../models/list/my-lists';
import { MylistsServiceProvider } from '../../../services/other/mylists-service/mylists-service';
import { MyListArtistsPage } from '../../secondary/my-list-artists/my-list-artists';
import { CustomList } from '../../../models/list/custom-list';
import { MyListsPopoverPage } from '../../popover/my-lists-popover/my-lists-popover.page';

/**
 * Generated class for the MyListsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-my-lists',
  templateUrl: 'my-lists.html',
  styleUrls: [
    'my-lists.scss'
  ],
})
export class MyListsPage {

  public myLists: MyLists;

  constructor(public navCtrl: NavController, public popoverCtrl: PopoverController, public myListsService: MylistsServiceProvider) {
    myListsService.getMyLists().then(myLists => this.myLists = myLists);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MyListsPage');
  }

  itemTapped(selectedList: CustomList) {
    this.navCtrl.navigateForward(['my-list-artists-page'], {
      state: {
        customList: selectedList
      }
    });
  }

  presentPopover(popoverEvent) {
    this.popoverCtrl.create({
      component: MyListsPopoverPage,
      event: popoverEvent,
      translucent: true
    }).then(popover => {
      popover.onDidDismiss().then(result => {
        var data = result['data'];
        if (data) {
          if (data.share) {

          }
        }
      });
      popover.present();
    });
  }

}
