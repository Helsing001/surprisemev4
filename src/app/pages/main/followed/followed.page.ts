import { Component, NgZone, ViewChild, ChangeDetectorRef } from '@angular/core';
import { DiscographyServiceProvider } from '../../../services/other/discography-service/discography-service';
import { DiscoArtists } from '../../../models/other/disco-artists';
import { BasicArtist } from '../../../models/artist/basic-artist';
import { BasicDiscoArtist } from '../../../models/artist/basic-disco-artist';
import { orderBy, Many } from 'lodash';
import { AlertController, IonContent, IonInfiniteScroll, PopoverController, NavController, ToastController, LoadingController, ModalController } from '@ionic/angular';
import { SpotifyServiceProvider } from '../../../services/main/spotify-service/spotify-service';
import { ProviderService } from '../../../services/other/provider-service/provider-service';
import { ProviderOperations } from '../../../models/providers/provider-operations';
import { FollowedPopoverPage } from '../../popover/followed-popover/followed-popover.page';
import { LoadingServiceProvider } from '../../../services/other/loading-service/loading-service';
import { RefreshParams } from '../../../models/params/refresh-params';
import { LoadingQueueServiceProvider } from '../../../services/other/loading-queue-service/loading-queue-service';
import { RefreshDiscoAlert } from '../../alerts/refresh-disco-alert';
import { ArtistAlbumsModal } from '../../secondary/artist-albums/artist-albums.page';

@Component({
  selector: 'app-getting-started',
  templateUrl: './followed.page.html',
  styleUrls: ['./followed.page.scss'],
})
export class FollowedPage {

  @ViewChild(IonInfiniteScroll, undefined) infiniteScroll: IonInfiniteScroll;
  @ViewChild(IonContent, undefined) pageTop: IonContent;

  private MAX_SHOWN_STEP = 20;

  public discoArtists: DiscoArtists;
  public filteredArtists: Array<BasicDiscoArtist> = new Array();
  public maxShown = this.MAX_SHOWN_STEP;

  public orderByString: string = "default"
  public orderAscending: boolean = true;
  public unlistened: boolean = false;
  public queryText: string = "";

  constructor(public discographyService: DiscographyServiceProvider, public alertCtrl: AlertController, public ngZone: NgZone,
    public changeDetector: ChangeDetectorRef, public popoverCtrl: PopoverController, public providerService: ProviderService,
    public navCtrl: NavController, public loadingService: LoadingServiceProvider, public loadingQueueProvider: LoadingQueueServiceProvider,
    public toastCtrl: ToastController, public loadingController: LoadingController, public modalCtrl: ModalController) { }

  ionViewWillEnter() {
    this.discographyService.getDiscoArtists().then(discoArtists => {
      this.discoArtists = discoArtists;
      this.filterArtist();
      this.showRefreshInfo();
    });
  }

  filterArtist() {
    this.ngZone.run(() => {
      var orderDirection: Many<boolean | "asc" | "desc"> = this.orderAscending ? "asc" : "desc";
      if (this.orderByString === "default") {
        this.filteredArtists = orderBy(this.discoArtists.artists.filter(artist => artist.latestAlbum != undefined), "latestAlbum", orderDirection)
        this.filteredArtists = this.filteredArtists.concat(orderBy(this.discoArtists.artists.filter(artist => artist.latestAlbum == undefined), "artist.artist", orderDirection))
      }
      else {
        this.filteredArtists = orderBy(this.discoArtists.artists, this.orderByString, orderDirection);
      }
      this.filteredArtists = this.filteredArtists.filter((discoArtist: BasicDiscoArtist) =>
        discoArtist.artist.artist.toLowerCase().indexOf(this.queryText.toLowerCase()) > -1 &&
        (!this.unlistened || discoArtist.albumCount > 0)
      );
      this.resetScroll();
      this.changeDetector.detectChanges();
    })
  }

  showRefreshInfo() {
    if (this.discoArtists.artists.filter(artist => artist.latestAlbum == undefined && artist.albumCount != 0).length > 0) {
      this.presentToast("Have you just updated? It seems artists info is out of sync, please refresh all artist from the menu.");
  }
}

reorderArtists() {
  this.orderAscending = !this.orderAscending;
  this.filterArtist();
}

loadData(event) {
  this.maxShown += this.MAX_SHOWN_STEP;
  event.target.complete();
  if (this.maxShown >= this.filteredArtists.length) {
    event.target.disabled = true;
  }
}

resetScroll() {
  this.pageTop.scrollToTop();
  this.maxShown = this.MAX_SHOWN_STEP;
  this.infiniteScroll.disabled = false;
}

async itemTapped(listArtist: BasicDiscoArtist) {
  if (this.loadingQueueProvider.queueParamsArray.filter(param => param.refreshParams).length == 0) {
    const loading = await this.loadingController.create({ message: 'Getting albums...' })
    const loadingP = await loading.present()
    try {
      const artistAlbums =
        await this.discographyService.refresh(listArtist);
      this.filterArtist();
      this.loadingController.dismiss();
      const albumsModal = await this.modalCtrl.create({
        component: ArtistAlbumsModal,
        componentProps: { albums: artistAlbums, artist: listArtist, followed: true },
        cssClass: 'medium-modal'
      });
      albumsModal.onDidDismiss()
        .then((result) => {
          var data = result['data'];
          if (data) {
            if (data.remove) {
              this.discographyService.unfollowArtist(listArtist.artist)
            }
          }
          this.filterArtist()
        })
      return await albumsModal.present();
    } catch (err) {
      this.loadingController.dismiss();
      if (err instanceof Error && err.message === "Artist not found") {
        this.presentToast("Artist not found on the current discography provider")
      }
      else {
        console.log(err)
        this.presentToast("An error occurred")
      }
    }
  }
  else {
    this.presentToast("Try again after the queued refresh finishes");
  }
}

presentOrderByAlert() {
  this.alertCtrl.create({
    header: "Order By",
    inputs: [
      {
        name: 'default',
        type: 'radio',
        label: 'Default',
        value: 'default',
        checked: this.orderByString === "default"
      },
      {
        name: 'artist',
        type: 'radio',
        label: 'Name',
        value: 'artist.artist',
        checked: this.orderByString === "artist.artist"
      },
      {
        name: 'latestAlbum',
        type: 'radio',
        label: 'Oldest Unlistened',
        value: 'latestAlbum',
        checked: this.orderByString === "latestAlbum"
      },
      {
        name: 'albumCount',
        type: 'radio',
        label: 'Album Count',
        value: 'albumCount',
        checked: this.orderByString === "albumCount"
      }
    ],
    buttons: [
      'Cancel',
      {
        text: 'OK',
        handler: result => {
          this.orderByString = result;
          this.filterArtist();
        }
      }
    ]
  }).then(alert => alert.present());
}

presentInfoAlert() {
  this.alertCtrl.create({
    header: "Info",
    message: "This page is meant to show you when your favorite artists release new albums.<br><br>"
      + "The information on this page can be refreshed by pressing the <b>'Refresh All'</b> button in the page's popover menu.<br><br>"
      + "The date <b>under</b> the artist shows the release date of the oldest album you haven't listened to from that artist.<br><br>"
      + "The number on the <b>right</b> shows how many albums the artist has released since you last listened.<br><br>"
      + "You can press on any artist to open more information about albums",
    buttons: ['OK']
  }).then(alert => alert.present());
}

presentPopover(popoverEvent) {
  this.popoverCtrl.create({
    component: FollowedPopoverPage,
    event: popoverEvent,
    translucent: true
  }).then(popover => {
    popover.onDidDismiss().then(result => {
      var data = result['data'];
      if (data) {
        if (data.refresh) {
          if (this.loadingQueueProvider.queueParamsArray.filter(param => param.refreshParams).length == 0) {
            if (this.loadingQueueProvider.queueParamsArray.length == 0) {
              this.navCtrl.navigateForward('loading-page', { skipLocationChange: true });
            }
            else {
              this.presentToast("Queued");
            }
            this.loadingQueueProvider.queueRefreshDisco(new RefreshParams(this.filteredArtists))
              .then(result => {
                if (!this.loadingService.minimized) {
                  this.navCtrl.pop();
                  RefreshDiscoAlert.showArtistsWithNewAlbums(this.alertCtrl, result)
                }
              })
              .catch(err => {
                this.presentToast(err);
              });
          }
          else {
            this.presentToast("A refresh is already queued");
          }
        }
        else if (data.unlistened) {
          this.unlistened = !this.unlistened;
          this.filterArtist();
        }
        else if (data.share) {
          this.discographyService.share()
            .catch(err => { console.log(err); this.presentToast("An error occurred"); });
        }
        else if (data.import) {
          this.presentImportAlert().then(() => {
            this.discographyService.import()
              .then(discoArtists => { this.discoArtists = discoArtists; this.filterArtist(); this.presentToast("Imported"); })
              .catch(err => { console.log(err); this.presentToast("An error occurred"); });
          })
        }
        else if (data.info) {
          this.presentInfoAlert();
        }
      }
    });
    popover.present();
  });
}

  private presentImportAlert(): Promise < any > {
  return new Promise(resolve => {
    this.alertCtrl.create({
      header: "Info",
      message: "Do you want to replace the current followed artists with the imported ones?",
      buttons: [
        'Cancel',
        {
          text: 'OK',
          handler: result => {
            resolve()
          }
        }
      ]
    }).then(alert => alert.present());
  })
}

  public presentToast(message: string) {
  this.toastCtrl.create({
    message: message,
    duration: 2000,
    position: 'bottom'
  }).then(toast => toast.present());
}

}
