import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';


import { RouterModule } from '@angular/router';
import { ShellModule } from '../../../shell/shell.module';
import { FollowedPage } from './followed.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ShellModule,
    RouterModule.forChild([
      {
        path: '',
        component: FollowedPage
      }
    ])    
  ],
  declarations: [FollowedPage]
})
export class FollowedPageModule {}
