import { Component } from '@angular/core';
import { LoadingServiceProvider } from '../../../services/other/loading-service/loading-service';
import { StorageServiceProvider } from '../../../services/other/storage-service/storage-service';
import { Insomnia } from '@ionic-native/insomnia/ngx';
import { ActivatedRoute, Router } from '@angular/router';
import { NavController, Platform, AlertController } from '@ionic/angular';
import { LoadingQueueServiceProvider } from '../../../services/other/loading-queue-service/loading-queue-service';
import { DatePipe } from '@angular/common';
import { SearchParams } from '../../../models/params/search-params';
import { Market } from '@ionic-native/market/ngx';

/**
 * Generated class for the SupportMePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-loading',
  templateUrl: 'support-me.html',
  styleUrls: [
    'support-me.scss'
  ]
})
export class SupportMePage {

  constructor(public market: Market) {}

  public openAwesomeApp(){
    this.market.open('ro.surprise.premium');
  }

}
