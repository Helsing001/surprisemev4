import { Component, OnInit } from '@angular/core';
import { LoadingQueueServiceProvider } from '../../../services/other/loading-queue-service/loading-queue-service';
import { DatePipe } from '@angular/common';
import { LoadingServiceProvider } from '../../../services/other/loading-service/loading-service';
import { NavController, AlertController, PopoverController, ToastController } from '@ionic/angular';
import { QueuedResultsPopoverPage } from '../../popover/queued-results-popover/queued-results-popover.page';
import { Router } from '@angular/router';
import { GenerateArtistAlert } from '../../alerts/generate-artist-alert';
import { QueuedParams, HistoryResult } from '../../../models/params/queued-params';
import { RefreshDiscoAlert } from '../../alerts/refresh-disco-alert';

@Component({
  selector: 'app-queued-results',
  templateUrl: './queued-results.page.html',
  providers: [DatePipe],
  styleUrls: ['./queued-results.page.scss'],
})
export class QueuedResultsPage {

  public fromMenu: boolean;
  public fromLoading: boolean;

  constructor(public loadingQueueProvider: LoadingQueueServiceProvider, public loadingService: LoadingServiceProvider,
    public navCtrl: NavController, public alertCtrl: AlertController, public popoverCtrl: PopoverController,
    public toastCtrl: ToastController, public router: Router, public datepipe: DatePipe) {
    if (this.router.getCurrentNavigation().extras.state) {
      this.fromMenu = this.router.getCurrentNavigation().extras.state.fromMenu;
      this.fromLoading = this.router.getCurrentNavigation().extras.state.fromLoading;
    }
  }

  public currentSearch(): QueuedParams {
    return this.loadingQueueProvider.getCurrentParams();
  }

  public queuedSearches(): Array<QueuedParams> {
    return this.loadingQueueProvider.getFutureParams().slice().reverse();
  }

  public lastResults(): Array<HistoryResult> {
    return this.loadingQueueProvider.getLastResults().slice().reverse();
  }

  public type(queuedParams: QueuedParams): string {
    return this.typeS(queuedParams.type);
  }

  public typeH(historyResult: HistoryResult): string {
    return this.typeS(historyResult.type);
  }

  public duration(queuedParams: QueuedParams): string {
    var timestamp;
    switch (queuedParams.type) {
      case QueuedParams.TYPE_SEARCH: timestamp = queuedParams.searchParams.genericParams.timestamp; break;
      case QueuedParams.TYPE_GENERATE: timestamp = queuedParams.generateParams.timestamp; break;
      case QueuedParams.TYPE_REFRESH: timestamp = queuedParams.refreshParams.timestamp; break;
      default: timestamp = -1;
    }
    return this.datepipe.transform(timestamp, 'dd MMM yy HH:mm:ss');
  }

  public durationH(historyResult: HistoryResult): string {
    var timestamp;
    switch (historyResult.type) {
      case QueuedParams.TYPE_SEARCH: timestamp = historyResult.queueParams.searchParams.genericParams.timestamp; break;
      case QueuedParams.TYPE_GENERATE: timestamp = historyResult.queueParams.generateParams.timestamp; break;
      case QueuedParams.TYPE_REFRESH: timestamp = historyResult.queueParams.refreshParams.timestamp; break;
      default: timestamp = -1;
    }
    return this.datepipe.transform(timestamp, 'dd MMM yy HH:mm:ss');
  }

  public typeS(type: string) {
    switch (type) {
      case QueuedParams.TYPE_SEARCH: return "Search";
      case QueuedParams.TYPE_GENERATE: return "Generate";
      case QueuedParams.TYPE_REFRESH: return "Refresh";
      default: return "Unknown";
    }
  }

  public currentTapped() {
    this.navCtrl.navigateForward('loading-page', { skipLocationChange: true });
  }

  public historyTapped(historyResult: HistoryResult) {
    if (QueuedParams.TYPE_SEARCH === historyResult.type) {
      this.navCtrl.navigateForward('rec-results-page', {
        state: {
          previousUrl: 'queued-results-page',
          fullArtists: historyResult.searchResults,
          simpleSearch: historyResult.queueParams.searchParams.genericParams.simpleSearch
        }
      });
    }
    else if (QueuedParams.TYPE_GENERATE === historyResult.type) {
      GenerateArtistAlert.showGenerateResult(this.alertCtrl, this.toastCtrl, historyResult.generateResult);
    }
    else if (QueuedParams.TYPE_REFRESH === historyResult.type) {
      RefreshDiscoAlert.showArtistsWithNewAlbums(this.alertCtrl, historyResult.refreshResult);
    }
  }

  public cancelCurrent() {
    this.loadingQueueProvider.cancelCurrent();
  }

  public cancelQueued(queuedParams: QueuedParams) {
    this.loadingQueueProvider.cancelQueued(queuedParams);
  }

  public removeHistory(historyResult: HistoryResult) {
    this.loadingQueueProvider.removeHistory(historyResult);
  }

  public infoCurrent() {
    var params = this.loadingQueueProvider.getCurrentParams();
    this.infoQueue(params);
  }

  public infoHistory(historyResult: HistoryResult) {
    this.infoQueue(historyResult.queueParams);
  }

  public infoQueue(params: QueuedParams) {
    var message = this.loadingQueueProvider.getInfoMessage(params, this.datepipe);
    let alert = this.alertCtrl.create({
      header: "Info",
      message: message,
      buttons: ['OK']
    }).then(alert => alert.present());
  }

  public presentPopover(popoverEvent) {
    this.popoverCtrl.create({
      component: QueuedResultsPopoverPage,
      event: popoverEvent,
      translucent: true,
      cssClass: 'medium-popover'
    }).then(popover => {
      popover.onDidDismiss().then(result => {
        var data = result['data'];
        if (data) {
          if (data.removeAll) {
            this.loadingQueueProvider.removeAll();
          }
          else if (data.removeFinished) {
            this.loadingQueueProvider.removeAllHistory();
          }
          else if (data.removeQueued) {
            this.loadingQueueProvider.removeAllQueued();
          }
        }
      });
      popover.present();
    });
  }

  public back() {
    if (!this.fromLoading) {
      this.navCtrl.pop();
    }
    else {
      this.navCtrl.navigateRoot('surprise-me-page');
    }
  }

}
