import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { QueuedResultsPage } from './queued-results.page';
import { ShellModule } from '../../../shell/shell.module';
import { RouterModule } from '@angular/router';
import { NgCircleProgressModule } from 'ng-circle-progress';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ShellModule,
    NgCircleProgressModule,
    RouterModule.forChild([
      {
        path: '',
        component: QueuedResultsPage
      }
    ])     
  ],
  declarations: [QueuedResultsPage]
})
export class QueuedResultsPageModule {}
