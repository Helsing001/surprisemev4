import { Component } from '@angular/core';
import { LoadingServiceProvider } from '../../../services/other/loading-service/loading-service';
import { StorageServiceProvider } from '../../../services/other/storage-service/storage-service';
import { Insomnia } from '@ionic-native/insomnia/ngx';
import { ActivatedRoute, Router } from '@angular/router';
import { NavController, Platform, AlertController } from '@ionic/angular';
import { LoadingQueueServiceProvider } from '../../../services/other/loading-queue-service/loading-queue-service';
import { DatePipe } from '@angular/common';
import { SearchParams } from '../../../models/params/search-params';

/**
 * Generated class for the LoadingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-loading',
  templateUrl: 'loading.html',
  providers: [DatePipe],
  styleUrls: [
    'loading.scss'
  ]
})
export class LoadingPage {

  public skipMinimized: boolean = false;

  constructor(public navCtrl: NavController, public alertCtrl: AlertController, public loadingService: LoadingServiceProvider,
    public storageService: StorageServiceProvider, public loadingQueueProvider: LoadingQueueServiceProvider, public insomnia: Insomnia,
    public datepipe: DatePipe, public route: ActivatedRoute, public router: Router, public platform: Platform) {
    if (this.router.getCurrentNavigation().extras.state && this.router.getCurrentNavigation().extras.state.skipMinimized) {
      this.skipMinimized = true;
    }
    this.platform.backButton.subscribe(() => { this.loadingService.minimized = true; });
  }

  ionViewWillEnter() {
    this.loadingService.minimized = false;
    this.storageService.getSettingsCache().then(settingsCache => {
      if (settingsCache && settingsCache.keepAwakeLoading) {
        this.insomnia.keepAwake()
          .then(
            () => { },
            () => console.log("Can't start keep awake mode!")
          );
      }
    });
  }

  ionViewWillLeave() {
    this.loadingService.minimized = true;
    this.insomnia.allowSleepAgain()
      .then(
        () => { },
        () => console.log("Can't stop keep awake mode!")
      );
  }

  public close() {
    this.loadingQueueProvider.cancelCurrent();
    this.navCtrl.pop();
  }

  public minimize() {
    this.loadingService.minimized = true;
    this.navCtrl.pop();
  }

  public openQueue() {
    this.loadingService.minimized = true;
    this.navCtrl.navigateForward('queued-results-page', {
      state: {
        fromLoading: true
      }
    });
  }

  public info() {
    var params = this.loadingQueueProvider.getCurrentParams();
    var message = this.loadingQueueProvider.getInfoMessage(params, this.datepipe);
    let alert = this.alertCtrl.create({
      header: "Info",
      message: message,
      buttons: ['OK']
    }).then(alert => alert.present());
  }

}
