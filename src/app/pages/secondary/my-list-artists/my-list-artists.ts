import { Component, ChangeDetectorRef, ViewChild, NgZone } from '@angular/core';
import { NavController, PopoverController, AlertController, ToastController, IonInfiniteScroll, IonContent, ModalController } from '@ionic/angular';
import { BasicArtist } from '../../../models/artist/basic-artist';
import { CustomList } from '../../../models/list/custom-list';
import { MylistsServiceProvider } from '../../../services/other/mylists-service/mylists-service';
import { ArtistInfoPage } from '../artist-info/artist-info';
import { MyListPopoverPage } from '../../popover/my-list-popover/my-list-popover';
import { FullArtist } from '../../../models/artist/full-artist';
import { StorageServiceProvider } from '../../../services/other/storage-service/storage-service';
import { ProviderService } from '../../../services/other/provider-service/provider-service';
import { ActivatedRoute, Router } from '@angular/router';
import { orderBy, Many } from 'lodash';
import { GenerateParams } from '../../../models/params/generate-params';
import { GeneratePlaylistModal } from '../generate-playlist/generate-playlist.page';
import { ProviderOperations } from '../../../models/providers/provider-operations';
import { LoadingServiceProvider } from '../../../services/other/loading-service/loading-service';
import { SearchServiceProvider } from '../../../services/main/search-service/search-service';
import { GenericService } from '../../../models/generic/generic-service';
import { DataServiceProvider } from '../../../services/other/data-service/data-service';

/**
 * Generated class for the MyListArtistsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-my-list-artists',
  templateUrl: 'my-list-artists.html',
  styleUrls: [
    'my-list-artists.scss'
  ],
})
export class MyListArtistsPage {

  @ViewChild(IonInfiniteScroll, undefined) infiniteScroll: IonInfiniteScroll;
  @ViewChild(IonContent, undefined) pageTop: IonContent;

  public MAX_SHOWN_STEP = 20;

  public infoProvider: GenericService;
  public customList: CustomList;
  public selectionMode: boolean = false;
  public selectedArtists: Array<string> = new Array();

  public queryText: string = "";
  public orderByString: string = 'listorder';
  public orderAscending: boolean = true;
  public listFullArtists: Array<FullArtist> = new Array();
  public filteredArtists: Array<FullArtist> = new Array();
  public maxShown = this.MAX_SHOWN_STEP;
  public searchPicturesId;

  constructor(public navCtrl: NavController, public myListsService: MylistsServiceProvider, public loadingService: LoadingServiceProvider,
    public providerService: ProviderService, public popoverCtrl: PopoverController, public modalCtrl: ModalController,
    public alertCtrl: AlertController, public toastCtrl: ToastController, public changeDetector: ChangeDetectorRef,
    public route: ActivatedRoute, public router: Router, public ngZone: NgZone, public searchServiceProvider: SearchServiceProvider,
    public dataService: DataServiceProvider) {
    if (this.router.getCurrentNavigation().extras.state) {
      this.customList = this.router.getCurrentNavigation().extras.state.customList;
      this.listFullArtists = this.customList.listArtists.map(b => FullArtist.fromBasicArtist(b));
      this.filteredArtists = this.listFullArtists;
    }
  }

  ionViewWillEnter() {
    this.searchPicturesId = 0;
    var providerSettings = this.providerService.getActiveProviderFor(ProviderOperations.INFO);
    this.infoProvider = this.providerService.getServiceProvider(providerSettings.name);
    this.picturesSearch(this.filteredArtists.slice(0, this.maxShown), this.searchPicturesId);
  }

  ionViewWillLeave() {
    this.searchPicturesId = -1;
  }

  public picturesSearch(artistsToSearch: Array<FullArtist>, id: number) {
    if (artistsToSearch.length > 0 && this.searchPicturesId == id) {
      var artist = artistsToSearch.shift();
      if (artist.iconSmall) {
        this.picturesSearch(artistsToSearch, id);
      }
      else {
        this.searchServiceProvider.searchArtistInfo(artist).then((fullArtist: FullArtist) => {
          artist.iconSmall = fullArtist.iconSmall;
          setTimeout(() => {
            if (this.searchPicturesId == id) {
              this.picturesSearch(artistsToSearch, id);
            }
          }, this.loadingService.searchTimestamp == -1 ? this.infoProvider.getApiDelay() : 4 * this.infoProvider.getApiDelay());
        })
      }
    }
  }

  itemTapped(item: BasicArtist) {
    if (!this.selectionMode) {
      var localState = {
        fullArtist: item,
        simpleSearch: true
      };
      this.dataService.state = localState;
      this.navCtrl.navigateForward(['artist-info-page']);
    }
    else {
      if (this.selectedArtists.indexOf(item.artist) == -1) {
        this.selectedArtists.push(item.artist);
      }
      else {
        this.selectedArtists = this.selectedArtists.filter(artist => artist !== item.artist);
      }
    }
  }

  filterArtist() {
    this.ngZone.run(() => {
      this.filteredArtists = this.listFullArtists.filter((fullArtist: FullArtist) => fullArtist.artist.toLowerCase().indexOf(this.queryText.toLowerCase()) > -1);
      if (this.orderByString !== 'listorder') {
        var orderDirection: Many<boolean | "asc" | "desc"> = this.orderAscending ? "asc" : "desc";
        this.filteredArtists = orderBy(this.filteredArtists, this.orderByString, orderDirection);
      }
      else if (!this.orderAscending) {
        this.filteredArtists = this.filteredArtists.reverse();
      }
      this.resetScroll();
      this.changeDetector.detectChanges();
      this.searchPicturesId++;
      this.picturesSearch(this.filteredArtists.slice(0, this.maxShown), this.searchPicturesId);
    })
  }

  reorderArtists() {
    this.orderAscending = !this.orderAscending;
    this.filterArtist();
  }

  presentOrderByAlert() {
    var pointsLabel = 'Points';
    if (this.customList.partOfLibrary) { pointsLabel = "Listens" }
    this.alertCtrl.create({
      header: "Order By",
      inputs: [
        {
          name: 'listorder',
          type: 'radio',
          label: 'List Order',
          value: 'listorder',
          checked: this.orderByString === "listorder"
        },
        {
          name: 'artist',
          type: 'radio',
          label: 'Name',
          value: 'artist',
          checked: this.orderByString === "artist"
        },
        {
          name: 'weight',
          type: 'radio',
          label: pointsLabel,
          value: 'weight',
          checked: this.orderByString === "weight"
        }],
      buttons: [
        'Cancel',
        {
          text: 'OK',
          handler: result => {
            this.orderByString = result;
            this.filterArtist();
          }
        }
      ]
    }).then(alert => alert.present());
  }

  setLibrary() {
    this.ngZone.run(() => {
      this.myListsService.addRemovePartOfLibrary(this.customList);
      this.changeDetector.detectChanges();
    });
  }

  presentPopover(popoverEvent) {
    this.popoverCtrl.create({
      component: MyListPopoverPage,
      componentProps: { customList: this.customList },
      event: popoverEvent,
      translucent: true
    }).then(popover => {
      popover.onDidDismiss().then(result => {
        var data = result['data'];
        if (data) {
          if (data.listName) {
            this.customList = this.myListsService.renameList(this.customList, data.listName);
          }
          else if (data.delete) {
            this.myListsService.deleteList(this.customList);
            this.navCtrl.pop();
          }
          else if (data.share) {
            var sharedListAsArray: Array<CustomList> = new Array();
            sharedListAsArray.push(this.customList);
            this.myListsService.share(sharedListAsArray, this.customList.listName)
              .catch(err => { console.log(err); this.presentToast("An error occurred"); });
          }
          else if (data.generate) {
            if (this.listFullArtists.length > GeneratePlaylistModal.MAX_SONGS) {
              this.presentToast("List must have under 10000 songs");
            }
            else {
              if (this.providerService.getAllProvidersFor(ProviderOperations.GENERATE).length > 0) {
                const modal = this.modalCtrl.create({
                  component: GeneratePlaylistModal,
                  componentProps: { customList: this.customList }
                }).then(modal => {
                  modal.onDidDismiss()
                    .then(data => {
                      if (data.data) this.presentToast(data.data);
                    })
                    .catch(error => this.presentToast("An error occurred"));
                  modal.present();
                })
              }
              else {
                this.presentToast("No enabled provider can generate playlists");
              }
            }
          }
        }
      });
      popover.present();
    });
  }

  itemPressed(item: BasicArtist) {
    this.selectionMode = true;
    this.selectedArtists.push(item.artist);
  }

  closeSelection() {
    this.selectionMode = false;
    this.selectedArtists = new Array();
  }

  selectAll() {
    this.listFullArtists.forEach((artist: FullArtist) => {
      if (this.selectedArtists.indexOf(artist.artist) == -1) {
        this.selectedArtists.push(artist.artist);
      }
    });
  }

  remove() {
    this.myListsService.removeArtistsFromList(this.customList, this.selectedArtists);
    this.listFullArtists = this.listFullArtists.filter(a => this.selectedArtists.indexOf(a.artist) == -1);
    this.selectedArtists = new Array();
    this.filterArtist();
  }

  setWeight() {
    this.presentWeightPrompt();
  }

  loadData(event) {
    this.maxShown += this.MAX_SHOWN_STEP;
    event.target.complete();
    if (this.maxShown >= this.filteredArtists.length) {
      event.target.disabled = true;
    }
    this.searchPicturesId++;
    this.picturesSearch(this.filteredArtists.slice(0, this.maxShown), this.searchPicturesId);
  }

  public resetScroll() {
    this.pageTop.scrollToTop();
    this.maxShown = this.MAX_SHOWN_STEP;
    this.infiniteScroll.disabled = false;
  }

  public presentWeightPrompt() {
    this.alertCtrl.create({
      header: 'Enter weight',
      inputs: [
        {
          name: 'weight',
          placeholder: 'Weight',
          type: 'number'
        }
      ],
      buttons: [
        {
          text: 'Cancel'
        },
        {
          text: 'OK',
          handler: data => {
            this.myListsService.setWeightToArtists(this.customList, this.selectedArtists, Number(data.weight));
            this.listFullArtists.filter(a => this.selectedArtists.indexOf(a.artist) !== -1).forEach(a => a.weight = Number(data.weight));
            this.filterArtist();
          }
        }
      ]
    }).then(alert => alert.present());
  }

  public presentToast(message: string) {
    this.toastCtrl.create({
      message: message,
      duration: 2000,
      position: 'bottom'
    }).then(toast => toast.present());
  }

}
