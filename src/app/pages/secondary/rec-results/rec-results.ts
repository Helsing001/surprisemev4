import { Component, ChangeDetectorRef, ViewChild, NgZone } from '@angular/core';
import { NavController, PopoverController, AlertController, IonContent, IonInfiniteScroll, ToastController, ModalController } from '@ionic/angular';
import { FullArtist } from '../../../models/artist/full-artist'
import { RecListPopoverPage } from '../../popover/rec-list-popover/rec-list-popover'
import { ArtistInfoPage } from '../../secondary/artist-info/artist-info';
import { GenreFilterPipe } from '../../../pipes/genre-filter/genre-filter';
import { ActivatedRoute, Router } from '@angular/router';
import { MyLists } from '../../../models/list/my-lists';
import { MylistsServiceProvider } from '../../../services/other/mylists-service/mylists-service';
import { ThousandSuffixesPipePipe } from '../../../pipes/genre-filter/thousand-suffixes-pipe';
import { Many, orderBy, concat as lConcat, countBy, identity, Dictionary, sortBy } from 'lodash';
import { SearchParams } from '../../../models/params/search-params';
import { AddToCustomListAlert } from '../../alerts/add-to-custom-list-alert';
import { CustomList } from '../../../models/list/custom-list';
import { GeneratePlaylistModal } from '../generate-playlist/generate-playlist.page';
import { ProviderService } from '../../../services/other/provider-service/provider-service';
import { ProviderOperations } from '../../../models/providers/provider-operations';
import { DataServiceProvider } from '../../../services/other/data-service/data-service';

@Component({
  selector: 'page-list',
  templateUrl: 'rec-results.html',
  providers: [GenreFilterPipe, ThousandSuffixesPipePipe],
  styleUrls: [
    'rec-results.scss'
  ]
})
export class RecResultsPage {

  @ViewChild(IonInfiniteScroll, undefined) infiniteScroll: IonInfiniteScroll;
  @ViewChild(IonContent, undefined) pageTop: IonContent;

  public MAX_SHOWN_ARTISTS_STEP = 20;

  public myLists: MyLists;

  public selectedItem: any;
  public items: Array<FullArtist>;

  public simpleSearch: boolean = false;
  public showSources: boolean = false;

  public orderByString: string = 'weight';
  public orderAscending: boolean = false;
  public showFilter: boolean = false;
  public filteredArtists: Array<FullArtist> = new Array();
  public maxShownArtists = this.MAX_SHOWN_ARTISTS_STEP;
  public previousUrl: string;

  public tagSign: string = "OR";
  public shownTags: number = 10;
  public maxShownTags: number = 40;
  public topTags: Array<string> = new Array();
  public selectedTags: Array<string> = new Array();

  constructor(public navCtrl: NavController, public popoverCtrl: PopoverController, public genreFilterPipe: GenreFilterPipe,
    public myListsService: MylistsServiceProvider, public alertCtrl: AlertController, public changeDetector: ChangeDetectorRef,
    public toastCtrl: ToastController, public providerService: ProviderService, public route: ActivatedRoute,
    public modalCtrl: ModalController, public router: Router, public ngZone: NgZone, public dataService: DataServiceProvider) {
    if (this.router.getCurrentNavigation().extras.state) {
      this.items = this.router.getCurrentNavigation().extras.state.fullArtists as Array<FullArtist>;
      this.filteredArtists = this.items;
      this.simpleSearch = this.router.getCurrentNavigation().extras.state.simpleSearch;
      this.showSources = this.router.getCurrentNavigation().extras.state.showSources;
      this.previousUrl = this.router.getCurrentNavigation().extras.state.previousUrl;
      if (!this.simpleSearch) {
        this.calculateTopTags();
        this.limitShownTags();
      }
    }
    myListsService.getMyLists().then(myLists => this.myLists = myLists);
  }

  public calculateTopTags() {
    var allTags: Array<string> = new Array();
    this.items.forEach(fullArtist => {
      allTags = lConcat(allTags, fullArtist.genres);
    });
    var topTagsUnordered: Dictionary<number> = countBy(allTags, identity);
    var topTags = orderBy(Object.keys(topTagsUnordered), [function (tag) { return topTagsUnordered[tag]; }], 'desc');
    if (topTags.length == 1 && topTags.includes("undefined")) {
      this.topTags = new Array();
    }
    else {
      this.topTags = topTags;
    }
  }

  public limitShownTags() {
    if (this.shownTags > this.topTags.length) {
      this.shownTags = this.topTags.length;
    }
    else if (this.shownTags > this.maxShownTags) {
      this.shownTags = this.maxShownTags;
    }
  }

  public addRemoveFav(fullArtist: FullArtist) {
    this.myListsService.addRemoveFav(fullArtist);
  }

  public addRemoveBan(fullArtist: FullArtist) {
    this.myListsService.addRemoveBan(fullArtist);
  }

  public checkFav(fullArtist: FullArtist) {
    return this.myLists !== undefined && this.myLists.favList.listArtists.find(artist => artist.artist == fullArtist.artist) !== undefined;
  }

  public checkBan(fullArtist: FullArtist) {
    return this.myLists !== undefined && this.myLists.banList.listArtists.find(artist => artist.artist == fullArtist.artist) !== undefined;
  }

  filterArtist() {
    this.ngZone.run(() => {
      this.filteredArtists = this.items.filter((fullArtist: FullArtist) => this.checkTagFilter(fullArtist));
      var orderDirection: Many<boolean | "asc" | "desc"> = this.orderAscending ? "asc" : "desc";
      this.filteredArtists = orderBy(this.filteredArtists, this.orderByString, orderDirection);
      this.resetScroll().then(() => {
        this.changeDetector.detectChanges();
      });
    });
  }

  checkTagFilter(fullArtist: FullArtist) {
    if (this.selectedTags.length == 0) return true;
    if (!fullArtist.genres) return false;
    if (this.tagSign == 'OR') {
      let result = false;
      this.selectedTags.forEach(selectedTag => {
        if (fullArtist.genres.includes(selectedTag)) result = true;
      });
      return result;
    }
    if (this.tagSign == 'AND') {
      let result = true;
      this.selectedTags.forEach(selectedTag => {
        if (!fullArtist.genres.includes(selectedTag)) result = false;
      });
      return result;
    }
  }

  reorderArtists() {
    this.orderAscending = !this.orderAscending;
    this.filterArtist();
  }

  showGenreFilter() {
    this.showFilter = !this.showFilter;
  }

  tagClicked(tag: string) {
    if (this.selectedTags.indexOf(tag) == -1) {
      this.selectedTags.push(tag);
    }
    else {
      this.selectedTags = this.selectedTags.filter(arrayTag => arrayTag !== tag);
    }
    this.filterArtist();
  }

  changeTagSign() {
    this.tagSign = this.tagSign == 'OR' ? 'AND' : 'OR';
    this.filterArtist();
  }

  loadMoreTags() {
    this.shownTags = this.shownTags + 10;
    this.limitShownTags();
  }

  loadData(event) {
    this.maxShownArtists += this.MAX_SHOWN_ARTISTS_STEP;
    event.target.complete();
    if (this.maxShownArtists >= this.filteredArtists.length) {
      event.target.disabled = true;
    }
  }

  public resetScroll() {
    return new Promise(resolve => {
      this.pageTop.scrollToTop().then(() => {
        this.maxShownArtists = this.MAX_SHOWN_ARTISTS_STEP;
        this.infiniteScroll.disabled = false;
        resolve();
      })
    })
  }

  presentOrderByAlert() {
    this.alertCtrl.create({
      header: "Order By",
      inputs: [
        {
          name: 'listeners',
          type: 'radio',
          label: 'Listeners',
          value: 'listeners',
          checked: this.orderByString === "listeners"
        },
        {
          name: 'playcount',
          type: 'radio',
          label: 'Playcount',
          value: 'playcount',
          checked: this.orderByString === "playcount"
        },
        {
          name: 'weight',
          type: 'radio',
          label: 'Weight',
          value: 'weight',
          checked: this.orderByString === "weight"
        }],
      buttons: [
        'Cancel',
        {
          text: 'OK',
          handler: result => {
            this.orderByString = result;
            this.filterArtist();
          }
        }
      ]
    }).then(alert => alert.present());
  }

  presentPopover(popoverEvent) {
    this.popoverCtrl.create({
      component: RecListPopoverPage,
      event: popoverEvent,
      translucent: true
    }).then(popover => {
      popover.onDidDismiss().then(result => {
        var data = result['data'];
        if (data) {
          if (data.addToCustomList) {
            AddToCustomListAlert.createForList(this.alertCtrl, this.myLists, this.myListsService, this.filteredArtists).then((response: string) => {
              this.presentToast(response);
            });
          }
          else if (data.generate) {
            if (this.filteredArtists.length > GeneratePlaylistModal.MAX_SONGS) {
              this.presentToast("List must have under 10000 songs");
            }
            else {
              var customList = new CustomList("SurpriseMe", CustomList.CUSTOM, false);
              customList.setList(this.filteredArtists);
              if (this.providerService.getAllProvidersFor(ProviderOperations.GENERATE).length > 0) {
                const modal = this.modalCtrl.create({
                  component: GeneratePlaylistModal,
                  componentProps: { customList: customList }
                }).then(modal => {
                  modal.onDidDismiss()
                    .then(data => {
                      console.log(data);
                    })
                    .catch(error => this.presentToast("An error occurred"));
                  modal.present();
                })
              }
              else {
                this.presentToast("No enabled provider can generate playlists");
              }
            }
          }
        }
      });
      popover.present();
    });
  }

  itemTapped(item) {
    this.ngZone.run(() => {
      var localState = {
        fullArtist: item,
        simpleSearch: this.simpleSearch,
        showSources: this.showSources
      };
      this.dataService.state = localState;
      this.navCtrl.navigateForward(['artist-info-page']);
    });
  }

  back() {
    if (this.previousUrl) {
      this.navCtrl.navigateBack(this.previousUrl);
    }
    else {
      this.navCtrl.navigateRoot('surprise-me-page');
    }
  }

  presentToast(message: string) {
    this.toastCtrl.create({
      message: message,
      duration: 2000,
      position: 'bottom'
    }).then(toast => toast.present());
  }

  public formatNumber(n: number, d: number) {
    var x = ('' + n).length;
    var d = Math.pow(10, d);
    x -= x % 3;
    return Math.round(n * d / Math.pow(10, x)) / d + " kMGTPE"[x / 3];
  }

}
