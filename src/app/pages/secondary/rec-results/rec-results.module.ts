import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';

import { RecResultsPage } from './rec-results';
import { PipesModule } from '../../../pipes/pipes.module';
import { ShellModule } from '../../../shell/shell.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ShellModule,
    RouterModule.forChild([
      {
        path: '',
        component: RecResultsPage
      }
    ]),
    PipesModule
  ],
  declarations: [RecResultsPage]
})
export class RecResultsPageModule {}
