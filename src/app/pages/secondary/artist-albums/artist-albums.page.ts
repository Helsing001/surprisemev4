import { Component, OnInit, ViewChild, NgZone, ChangeDetectorRef } from '@angular/core';
import { ModalController, NavParams, AlertController, ToastController } from '@ionic/angular';
import { ArtistAlbum } from '../../../models/album/artist-album';
import { BasicDiscoArtist } from '../../../models/artist/basic-disco-artist';
import { DiscographyServiceProvider } from '../../../services/other/discography-service/discography-service';

@Component({
  selector: 'page-artist-albums',
  templateUrl: './artist-albums.html',
  styleUrls: ['./artist-albums.scss'],
})
export class ArtistAlbumsModal implements OnInit {

  public artist: BasicDiscoArtist;
  public albums: Array<ArtistAlbum>;
  public isFollowed: boolean = false;

  constructor(public discographyService: DiscographyServiceProvider, public navParams: NavParams, public modalCtrl: ModalController,
    public alertCtrl: AlertController, public toastCtrl: ToastController, public ngZone: NgZone, public changeDetector: ChangeDetectorRef) {
    this.artist = navParams.get('artist');
    this.albums = navParams.get('albums');
    this.isFollowed = navParams.get('followed');
  }

  ngOnInit() {
  }

  isListened(album: ArtistAlbum): boolean {
    return this.artist.lastListened != undefined && album.releaseDate.getTime() <= this.artist.lastListened.getTime();
  }

  getName(album: ArtistAlbum): string {
    let name = album.album;
    if (this.isFollowed) {
      if (this.isListened(album)) {
        name = name + " " + String.fromCharCode(0x2705);
      }
      else {
        name = name + " " + String.fromCharCode(0x274C);
      }
    }
    return name;
  }

  checkMarkDisabled() {
    let album = this.albums.slice().reverse().find(album => !this.isListened(album))
    return album == undefined
  }

  checkUnmarkDisabled() {
    let album = this.albums.find(album => this.isListened(album))
    return album == undefined
  }

  markAlbum() {
    let releaseDate = this.albums.slice().reverse().find(album => !this.isListened(album)).releaseDate
    let albumsToBeMarked = this.albums.filter(album => album.releaseDate.getTime() == releaseDate.getTime())
    this.presentMarkAlert(albumsToBeMarked, releaseDate, true);
  }

  unmarkAlbum() {
    let artistLastListenedDate;
    if (this.albums.find(album => album.releaseDate.getTime() == this.artist.lastListened.getTime())) {
      artistLastListenedDate = this.artist.lastListened;
    }
    else {
      artistLastListenedDate = this.albums[0].releaseDate;
    }
    let newLastListenedAlbums = this.albums.find(album => this.isListened(album) && artistLastListenedDate.getTime() != album.releaseDate.getTime())
    let newLastListened = undefined;
    if (newLastListenedAlbums) {
      newLastListened = newLastListenedAlbums.releaseDate
    }
    let albumsToDisplayForUnmark = this.albums.filter(album => album.releaseDate.getTime() == artistLastListenedDate.getTime())
    this.presentMarkAlert(albumsToDisplayForUnmark, newLastListened, false);
  }

  async removeArtist() {
    let confirmed = await this.presentRemoveAlert(this.artist.artist.artist)
    if (confirmed) {
      var result: any = {};
      result.remove = true;
      this.modalCtrl.dismiss(result);
    }
  }

  presentMarkAlert(albumsToDisplay: Array<ArtistAlbum>, newLastListened: Date, mark: boolean) {
    let markString = mark ? "mark" : "unmark"
    let subHeader = albumsToDisplay.length > 1 ? "Multiple albums will be updated because they have the same release date" : ""
    this.alertCtrl.create({
      header: "Info",
      subHeader: subHeader,
      message: "Do you want to " + markString + " '<b>" + albumsToDisplay.map(a => a.album).join(", ") + "</b>' as listened? ",
      buttons: [
        'Cancel',
        {
          text: 'OK',
          handler: result => {
            this.ngZone.run(() => {
              this.discographyService.updateLastListened(this.artist, this.albums, newLastListened)
              this.changeDetector.detectChanges();
            })
          }
        }
      ]
    }).then(alert => alert.present());
  }

  presentRemoveAlert(artist: string): Promise<boolean> {
    return new Promise(resolve => {
      this.alertCtrl.create({
        header: "Info",
        message: "Are you sure you want to unfollow " + artist + "?",
        buttons: [
          'Cancel',
          {
            text: 'OK',
            handler: () => {
              resolve(true)
            }
          }
        ]
      }).then(alert => alert.present());
    })
  }

}
