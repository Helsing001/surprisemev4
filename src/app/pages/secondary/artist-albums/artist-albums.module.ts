import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RouterModule } from '@angular/router';
import { ArtistAlbumsModal } from './artist-albums.page';
import { ShellModule } from '../../../shell/shell.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ShellModule,
    RouterModule.forChild([
      {
        path: '',
        component: ArtistAlbumsModal
      }
    ])  
  ],
  declarations: [ArtistAlbumsModal] 
})
export class ArtistAlbumsPageModule {} 
