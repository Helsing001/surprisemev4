import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';

import { ArtistInfoPage } from './artist-info';
import { PipesModule } from '../../../pipes/pipes.module';
import { ShellModule } from '../../../shell/shell.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ShellModule,
    RouterModule.forChild([
      {
        path: '',
        component: ArtistInfoPage
      }
    ]),
    PipesModule
  ],
  declarations: [ArtistInfoPage]
})
export class ArtistInfoPageModule {}
