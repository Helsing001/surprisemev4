import { Component, ChangeDetectorRef } from '@angular/core';
import { Location } from '@angular/common';
import { NavController, PopoverController, Platform, AlertController, ToastController, LoadingController, ModalController } from '@ionic/angular';
import { FullArtist } from '../../../models/artist/full-artist';
import { MylistsServiceProvider } from '../../../services/other/mylists-service/mylists-service';
import { ArtistInfoPopoverPage } from '../../popover/artist-info-popover/artist-info-popover';
import { MyLists } from '../../../models/list/my-lists';
import { ActivatedRoute, Router } from '@angular/router';
import { ThousandSuffixesPipePipe } from '../../../pipes/genre-filter/thousand-suffixes-pipe';
import { SearchParams } from '../../../models/params/search-params';
import { SearchServiceProvider } from '../../../services/main/search-service/search-service';
import { ArtistExtraInfo } from '../../../models/artist/artist-extra-info';
import { ProviderService } from '../../../services/other/provider-service/provider-service';
import { isNumber } from 'util';
import { ListenedTrack } from '../../../models/other/listened-track';
import { BasicArtist } from '../../../models/artist/basic-artist';
import { ListeningServiceProvider } from '../../../services/other/listening-service/listening-service';
import { StorageServiceProvider } from '../../../services/other/storage-service/storage-service';
import { Insomnia } from '@ionic-native/insomnia/ngx';
import { AlertInput } from '@ionic/core';
import { ProviderOperations } from '../../../models/providers/provider-operations';
import { ProviderSettings } from '../../../models/providers/provider-settings';
import { retryWhen, delay, take, concatMap, tap, map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { DataServiceProvider } from '../../../services/other/data-service/data-service';
import { ArtistAlbum } from '../../../models/album/artist-album';
import { DiscographyServiceProvider } from '../../../services/other/discography-service/discography-service';
import { LoadingQueueServiceProvider } from '../../../services/other/loading-queue-service/loading-queue-service';
import { ArtistAlbumsModal } from '../artist-albums/artist-albums.page';
import { BasicDiscoArtist } from '../../../models/artist/basic-disco-artist';

/**
 * Generated class for the ArtistInfoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-artist-info',
  templateUrl: 'artist-info.html',
  providers: [ThousandSuffixesPipePipe],
  styleUrls: [
    'artist-info.scss'
  ]
})
export class ArtistInfoPage {

  public artistInfoParams: ArtistInfoParams = new ArtistInfoParams();
  public artistInfoParamsHistory: Array<ArtistInfoParams> = new Array();
  public myLists: MyLists;
  public simpleSearch: boolean;

  constructor(public navCtrl: NavController, public changeDetector: ChangeDetectorRef, public popoverCtrl: PopoverController,
    public myListsService: MylistsServiceProvider, public searchProvider: SearchServiceProvider, public providerService: ProviderService,
    public listeningService: ListeningServiceProvider, public storageService: StorageServiceProvider, public dataService: DataServiceProvider,
    public discographyService: DiscographyServiceProvider, public loadingQueueProvider: LoadingQueueServiceProvider, public modalCtrl: ModalController,
    public loadingController: LoadingController, public alertCtrl: AlertController, public insomnia: Insomnia, public route: ActivatedRoute,
    public router: Router, public platform: Platform, public location: Location, public toastCtrl: ToastController) {
    myListsService.getMyLists().then(myLists => this.myLists = myLists);
    this.platform.backButton.subscribe(() => { this.back(); });
  }

  ionViewWillEnter() {
    var localState = this.dataService.state;
    this.artistInfoParams.listening = localState.listening;
    this.createFakeArtistExtraInfo(5);
    if (!this.artistInfoParams.listening) {
      this.artistInfoParams.fullArtist = localState.fullArtist;
      this.simpleSearch = localState.simpleSearch;
      if (this.artistInfoParams.fullArtist.sourceArtists) {
        this.createBasicSimilarArtistsFromSource();
      }
      this.getFullInfo();
    }
    else {
      this.startMonitoringListening();
      this.simpleSearch = true;
      this.keepAwake();
    }
  }

  ionViewWillLeave() {
    this.listeningService.stopMonitoring();
    this.artistInfoParams = new ArtistInfoParams();
    this.allowSleepAgain();
  }

  public startMonitoringListening() {
    this.listeningService.startMonitoring().pipe(retryWhen(err => {
      return err.pipe(
        map(error => {
          if (error.header === 'Temporary Error') {
            this.presentToast(error.subheader);
            return error;
          }
          else {
            throw error;
          }
        }));
    }))
      .subscribe(
        track => {
          if (track && (!this.artistInfoParams.fullArtist || track.artist.artist !== this.artistInfoParams.fullArtist.artist)) {
            this.artistInfoParams = new ArtistInfoParams();
            this.createFakeArtistExtraInfo(5);
            this.artistInfoParams.fullArtist = FullArtist.fromBasicArtist(track.artist);
            this.artistInfoParams.listening = true;
            this.getFullInfo();
          }
          this.artistInfoParams.track = track;
          this.artistInfoParams.listeningError = undefined;
        },
        error => {
          this.artistInfoParams.listeningError = error;
          this.artistInfoParams.track = undefined;
        }
      );
  }

  public keepAwake() {
    this.storageService.getSettingsCache().then(settingsCache => {
      let active = this.router.isActive('artist-info-page', false);
      if (active && settingsCache && settingsCache.keepAwakeListening) {
        this.insomnia.keepAwake()
          .then(
            () => { },
            () => console.log("Can't start keep awake mode!")
          );
      }

    })
  }

  public allowSleepAgain() {
    this.insomnia.allowSleepAgain()
      .then(
        () => { },
        () => console.log("Can't stop keep awake mode!")
      );
  }

  back() {
    if (this.artistInfoParamsHistory.length == 0) {
      this.navCtrl.pop();
    }
    else {
      this.artistInfoParams = this.artistInfoParamsHistory.pop();
      if (this.artistInfoParams.listening) {
        this.startMonitoringListening();
        this.simpleSearch = true;
        this.keepAwake();
      }
    }
  }

  public createFakeArtistExtraInfo(size: number) {
    var fullArtist = new FullArtist(undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined);
    var similarArtists = new Array<FullArtist>();
    for (var i = 0; i < size; i++) { similarArtists.push(fullArtist); }
    var album = new ArtistAlbum(undefined, undefined);
    var topAlbums = new Array<ArtistAlbum>();
    for (var i = 0; i < size; i++) { topAlbums.push(album); }
    var topTracks = new Array<string>();
    for (var i = 0; i < size; i++) { topTracks.push(undefined); }
    this.artistInfoParams.artistExtraInfo = new ArtistExtraInfo(similarArtists, topAlbums, topTracks);
    this.artistInfoParams.track = new ListenedTrack(fullArtist, undefined, undefined, undefined, false, undefined, undefined, false);
  }

  public createBasicSimilarArtistsFromSource() {
    this.artistInfoParams.similarArtistsFromSource = this.artistInfoParams.fullArtist.sourceArtists.map(sa => FullArtist.fromBasicArtist(sa));
  }

  public getFullInfo() {
    if (this.simpleSearch) {
      var currentArtist = this.artistInfoParams.fullArtist.artist;
      this.searchProvider.searchArtistInfo(this.artistInfoParams.fullArtist).then((f: FullArtist) => {
        if (this.artistInfoParams.fullArtist !== undefined && currentArtist === this.artistInfoParams.fullArtist.artist) {
          f.providers = this.artistInfoParams.fullArtist.providers;
          f.sourceArtists = this.artistInfoParams.fullArtist.sourceArtists;
          this.artistInfoParams.fullArtist = f;
          this.getExtraInfo();
          this.getScrobbles();
        }
      });
    }
    else {
      this.getExtraInfo();
      this.getScrobbles();
    }
  }

  public getExtraInfo() {
    var currentArtist = this.artistInfoParams.fullArtist.artist;
    this.searchProvider.searchArtistExtraInfo(this.artistInfoParams.fullArtist).then((extra: ArtistExtraInfo) => {
      if (this.artistInfoParams.fullArtist !== undefined && currentArtist === this.artistInfoParams.fullArtist.artist) {
        if (extra) this.artistInfoParams.artistExtraInfo = extra;
        else this.createFakeArtistExtraInfo(0);

        if (this.artistInfoParams.fullArtist.sourceArtists) {
          this.searchProvider.searchArtistsInfo(this.artistInfoParams.fullArtist.sourceArtists)
            .then(f => {
              if (this.artistInfoParams.fullArtist !== undefined && currentArtist === this.artistInfoParams.fullArtist.artist) {
                this.artistInfoParams.similarArtistsFromSource = f;
              }
            });
        }
      }
    });
  }

  public getScrobbles() {
    var currentArtist = this.artistInfoParams.fullArtist.artist;
    this.searchProvider.searchArtistScrobbles(this.artistInfoParams.fullArtist).then((scrobbles: number) => {
      if (this.artistInfoParams.fullArtist !== undefined && currentArtist === this.artistInfoParams.fullArtist.artist) {
        this.artistInfoParams.scrobbles = String(scrobbles);
      }
    })
  }

  public addRemoveFav() {
    this.myListsService.addRemoveFav(this.artistInfoParams.fullArtist);
    this.changeDetector.detectChanges();
  }

  public addRemoveBan() {
    this.myListsService.addRemoveBan(this.artistInfoParams.fullArtist);
    this.changeDetector.detectChanges();

  }

  public checkFav() {
    if (!this.artistInfoParams.fullArtist) return false;
    return this.myLists !== undefined && this.myLists.favList.listArtists.find(artist => artist.artist == this.artistInfoParams.fullArtist.artist) !== undefined;
  }

  public checkBan() {
    if (!this.artistInfoParams.fullArtist) return false;
    return this.myLists !== undefined && this.myLists.banList.listArtists.find(artist => artist.artist == this.artistInfoParams.fullArtist.artist) !== undefined;
  }

  public beautifyDuration() {
    if (this.artistInfoParams.track && this.artistInfoParams.track.duration) {
      var minutes = Math.floor(this.artistInfoParams.track.duration / 60);
      var seconds = this.artistInfoParams.track.duration - minutes * 60;
      return "(" + this.str_pad_left(minutes, '0', 2) + ":" + this.str_pad_left(seconds, '0', 2) + ")";
    }
  }
  public str_pad_left(string, pad, length) {
    return (new Array(length + 1).join(pad) + string).slice(-length);
  }

  public openArtist(artist: FullArtist) {
    if (artist.artist) {
      this.artistInfoParamsHistory.push(this.artistInfoParams);
      this.listeningService.stopMonitoring();
      this.allowSleepAgain();
      this.simpleSearch = true;
      this.artistInfoParams = new ArtistInfoParams();
      this.artistInfoParams.fullArtist = artist;
      this.createFakeArtistExtraInfo(5);
      this.getFullInfo();
    }
  }

  public async showAlbums(artist: FullArtist) {
    if (artist.artist) {
      if (this.loadingQueueProvider.queueParamsArray.filter(param => param.refreshParams).length == 0) {
        const loading = await this.loadingController.create({ message: 'Getting albums...' })
        const loadingP = await loading.present()
        try {
          let followed = true;
          let discoArtist = this.discographyService.findDiscoArtist(artist);
          if (discoArtist == undefined) {
            followed = false;
            discoArtist = new BasicDiscoArtist(artist, 0, new Date(), undefined);
          }
          const artistAlbums =
            await this.discographyService.refresh(discoArtist);
          this.loadingController.dismiss();
          const albumsModal = await this.modalCtrl.create({
            component: ArtistAlbumsModal,
            componentProps: { albums: artistAlbums, artist: discoArtist, followed: followed },
            cssClass: 'medium-modal'
          });
          return await albumsModal.present();
        } catch (err) {
          this.loadingController.dismiss();
          if (err instanceof Error && err.message === "Artist not found") {
            this.presentToast("Artist not found on the current discography provider")
          }
          else {
            console.log(err)
            this.presentToast("An error occurred")
          }
        }
      }
    }
    else {
      this.presentToast("Try again after the queued refresh finishes");
    }
  }

  presentPopover(popoverEvent) {
    this.popoverCtrl.create({
      component: ArtistInfoPopoverPage,
      componentProps: { fullArtist: this.artistInfoParams.fullArtist },
      event: popoverEvent,
      translucent: true
    }).then(popover => popover.present());
  }

  presentProviders() {
    var inputs: Array<AlertInput> = [];
    var providerSettings: Array<ProviderSettings> = this.providerService.getAllProvidersFor(ProviderOperations.LISTENING);
    var activeProvider: ProviderSettings = this.providerService.getActiveProviderFor(ProviderOperations.LISTENING);
    if (providerSettings.length > 0) {
      if (!this.providerService.getActiveProviderFor(ProviderOperations.LISTENING)) {
        inputs.push(this.createRadioInput("None", undefined, true));
      }
      providerSettings.forEach(setting => {
        inputs.push(this.createRadioInput(setting.name, setting.name, activeProvider && activeProvider.name === setting.name));
      });
    }
    else {
      inputs.push(this.createRadioInput("None", undefined, true));
    }
    this.presentAlert('Providers', inputs)
      .then((selectedProvider: string) => {
        if (selectedProvider && (!activeProvider || selectedProvider !== activeProvider.name)) {
          this.listeningService.stopMonitoring();
          this.artistInfoParams = new ArtistInfoParams();
          this.artistInfoParams.listening = true;
          this.createFakeArtistExtraInfo(5);
          this.providerService.setProviderFor(selectedProvider, ProviderOperations.LISTENING);
          this.startMonitoringListening();
        }
      })
      .catch((error) => { });
  }

  public createRadioInput(label, value, checked): AlertInput {
    var input = {
      type: 'radio',
      label: label,
      value: value,
      checked: checked
    };
    return input as AlertInput;
  }

  public presentAlert(title: string, inputs: Array<AlertInput>) {
    return new Promise((resolve, error) => {
      this.alertCtrl.create({
        header: title,
        inputs: inputs,
        buttons: [
          {
            text: 'Cancel',
            role: 'cancel',
            handler: result => {
              error();
            }
          },
          {
            text: 'OK',
            handler: result => {
              resolve(result);
            }
          }
        ]
      }).then(alert => alert.present());
    })
  }

  public presentToast(message: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 2000,
      position: 'bottom'
    }).then(toast => toast.present());
  }

}

class ArtistInfoParams {
  public fullArtist: FullArtist;
  public artistExtraInfo: ArtistExtraInfo;
  public similarArtistsFromSource: Array<FullArtist> = new Array();
  public scrobbles: string = 'Unknown';
  public track: ListenedTrack;
  public listening: boolean;
  public listeningError: any;
}
