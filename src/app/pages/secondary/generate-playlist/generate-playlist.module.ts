import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { GeneratePlaylistModal } from './generate-playlist.page';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild([
      {
        path: '',
        component: GeneratePlaylistModal
      }
    ])  
  ],
  declarations: [GeneratePlaylistModal] 
})
export class GeneratePlaylistPageModule {} 
