import { Component, OnInit, ViewChild } from '@angular/core';
import { CustomList } from '../../../models/list/custom-list';
import { NavParams, ModalController, IonContent, NavController, ToastController, AlertController, LoadingController } from '@ionic/angular';
import { GenerateParams } from '../../../models/params/generate-params';
import { ProviderService } from '../../../services/other/provider-service/provider-service';
import { ProviderSettings } from '../../../models/providers/provider-settings';
import { AlertOptions } from '@ionic/core';
import { BasicArtist } from '../../../models/artist/basic-artist';
import { GenerateResult } from '../../../models/other/generate-result';
import { LoadingServiceProvider } from '../../../services/other/loading-service/loading-service';
import { ProviderOperations } from '../../../models/providers/provider-operations';
import { LoadingQueueServiceProvider } from '../../../services/other/loading-queue-service/loading-queue-service';
import { GenerateArtistAlert } from '../../alerts/generate-artist-alert';

@Component({
  selector: 'page-generate-playlist',
  templateUrl: './generate-playlist.html',
  styleUrls: ['./generate-playlist.scss'],
})
export class GeneratePlaylistModal implements OnInit {

  public static MAX_SONGS: number = 10000;

  public providerOperations = ProviderOperations;

  public customList: CustomList;
  public generateParams: GenerateParams;
  public maxPerArtist: number;
  public submitHidden: boolean = false; 4

  constructor(public navParams: NavParams, public navCtrl: NavController, public modalCtrl: ModalController,
    public toastCtrl: ToastController, public alertCtrl: AlertController, public loadingController: LoadingController,
    public providerService: ProviderService, public loadingService: LoadingServiceProvider,
    public loadingQueueProvider: LoadingQueueServiceProvider) {
    this.customList = navParams.get('customList');
    var provider: string = undefined;
    if (this.providerService.getAllProvidersFor(ProviderOperations.GENERATE).length > 0) {
      var provider = this.providerService.getAllProvidersFor(ProviderOperations.GENERATE)[0].name;
    }
    this.generateParams = new GenerateParams(this.customList, this.customList.listName, true, "Just Awesome", 1, 10, false, undefined, provider);
    this.checkRanges();
  }

  ngOnInit() {
  }

  public getGenerateButtonText() {
    var mainText = 'GENERATE ~';
    if (this.loadingQueueProvider.queueParamsArray.length > 0) {
      mainText = 'QUEUE ~';
    }
    var songs = " SONGS";
    var songEstimation = this.getSongNumberEstimation();
    if (songEstimation == 1) songs = " SONG";
    return mainText + songEstimation + songs;
  }

  public getSongNumberEstimation() {
    return this.generateParams.songsPerArtist * this.generateParams.customList.listArtists.length;
  }

  public checkValidParams() {
    if (this.generateParams.playlistName.length == 0) return false;
    return true;
  }

  public checkRanges() {
    if (this.generateParams.songsToChooseFrom < this.generateParams.songsPerArtist) {
      this.generateParams.songsPerArtist = this.generateParams.songsToChooseFrom;
    }
    var maxPerArtist = this.generateParams.songsToChooseFrom;
    var maxPerArtist2 = Math.floor(GeneratePlaylistModal.MAX_SONGS / this.customList.listArtists.length);
    this.maxPerArtist = Math.min(maxPerArtist, maxPerArtist2);
  }

  public generate() {
    this.dismissModal();
    const loading = this.loadingController.create({
      message: 'Please wait...',
      duration: 2000
    }).then(l => l.present());
    this.providerService.getServiceProvider(this.generateParams.provider).fetchPrivateLists()
      .then(l => {
        var listsWithTheSameName = l.filter(e => e.name === this.generateParams.playlistName);
        var times = listsWithTheSameName.length;
        this.loadingController.dismiss();
        if (times > 0) {
          this.showListAlreadyExistsAlert(times)
            .then(result => {
              if (result === 'cancel') {
                return;
              }
              else if (result === 'add') {
                this.generateParams.listToAddTo = listsWithTheSameName[0];
              }
              this.callGenerateProvider();
            })
        }
        else {
          this.callGenerateProvider();
        }
      })
      .catch(err => {
        this.loadingController.dismiss();
        this.presentToast(err);
      })
  }

  callGenerateProvider() {
    if (this.getGenerateButtonText().startsWith('GENERATE')) {
      this.navCtrl.navigateForward('loading-page', { skipLocationChange: true });
    }
    else {
      this.presentToast("Queued");
    }
    this.loadingQueueProvider.queueGenerate(this.generateParams)
      .then(generateResult => {
        if (!this.loadingService.minimized) {
          this.navCtrl.pop();
          GenerateArtistAlert.showGenerateResult(this.alertCtrl, this.toastCtrl, generateResult);
        }
      })
      .catch(err => {
        if (!(err instanceof Error) || err.message !== "Search cancelled") {
          this.presentToast(err);
        }
      });
  }

  showListAlreadyExistsAlert(times: number) {
    return new Promise((resolve, error) => {
      var textMessage, addToExisting;
      if (times == 1) {
        textMessage = "There is already a playlist with this name";
        addToExisting = "Add";
      }
      else {
        textMessage = "There are already " + times + " playlists with this name";
        addToExisting = 'Add to last';
      }
      let alert = this.alertCtrl.create({
        header: "Info",
        message: textMessage,
        buttons: [
          {
            text: 'Create New',
            handler: data => { resolve("new"); }
          },
          {
            text: addToExisting,
            handler: data => { resolve("add"); }
          },
          {
            text: 'Cancel',
            handler: data => { resolve("cancel"); }
          }
        ]
      }).then(alert => alert.present());
    });
  }

  public dismissModal(message: string = undefined) {
    this.modalCtrl.dismiss(message);
  }

  public onFocus() {
    this.submitHidden = true;
  }

  public onLostFocus() {
    this.submitHidden = false;
  }

  public presentToast(message: string) {
    this.toastCtrl.create({
      message: message,
      duration: 2000,
      position: 'bottom'
    }).then(toast => toast.present());
  }

}
