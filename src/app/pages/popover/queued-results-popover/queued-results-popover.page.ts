import { Component, OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular';

@Component({
  selector: 'app-queued-results-popover',
  templateUrl: './queued-results-popover.page.html',
  styleUrls: ['./queued-results-popover.page.scss'],
})
export class QueuedResultsPopoverPage {

  constructor(public popoverCtrl: PopoverController) { }

  public removeFinished() {
    var result: any = {};
    result.removeFinished = true;
    this.popoverCtrl.dismiss(result);
  }  

  public removeAll() {
    var result: any = {};
    result.removeAll = true;
    this.popoverCtrl.dismiss(result);
  }   
  
  public removeQueued() {
    var result: any = {};
    result.removeQueued = true;
    this.popoverCtrl.dismiss(result);
  }  

}
