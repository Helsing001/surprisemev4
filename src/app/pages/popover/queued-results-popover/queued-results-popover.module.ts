import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { QueuedResultsPopoverPage } from './queued-results-popover.page';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild([ 
      {
        path: '',
        component: QueuedResultsPopoverPage
      }
    ])
  ],
  declarations: [QueuedResultsPopoverPage]
})
export class QueuedResultsPopoverPageModule {}
