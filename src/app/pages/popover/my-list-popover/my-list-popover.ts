import { Component } from '@angular/core';
import { NavController, AlertController, ToastController, PopoverController, NavParams } from '@ionic/angular';
import { CustomList } from '../../../models/list/custom-list';

/**
 * Generated class for the MyListPopoverPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-my-list-popover',
  templateUrl: 'my-list-popover.html',
  styleUrls: [
    'my-list-popover.scss'
  ],
})
export class MyListPopoverPage {

  public customList: CustomList;

  constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController,
    public toastCtrl: ToastController, public popoverCtrl: PopoverController) {
    this.customList = navParams.get('customList');
  }

  public shareList() {
    var result: any = {};
    result.share = true;
    this.popoverCtrl.dismiss(result);
  }

  public generatePlaylist() {
    var result: any = {};
    result.generate = true;
    this.popoverCtrl.dismiss(result);
  }  

  public renameList() {
    this.alertCtrl.create({
      header: 'Rename',
      inputs: [
        {
          name: 'listName',
          placeholder: 'List Name'
        }
      ],
      subHeader: 'Enter the new list name',
      buttons: [
        {
          text: 'OK',
          handler: result => {
            if (result.listName && result.listName.length <= 15) {
              this.popoverCtrl.dismiss(result);
            }
            else if (!result.listName) {
              this.presentToast("No name set");
            }
            else {
              this.presentToast("Name should be under 15 characters")
            }
          }
        },
        { text: 'Cancel', handler: result => { this.popoverCtrl.dismiss(); } }]
    }).then(alert => alert.present());
  }

  public deleteList() {
    this.alertCtrl.create({
      header: 'Please confirm',
      subHeader: 'The list will be deleted',
      buttons: [
        {
          text: 'OK',
          handler: result => {
            var result: any = {};
            result.delete = true;
            this.popoverCtrl.dismiss(result);
          }
        },
        { text: 'Cancel', handler: result => { this.popoverCtrl.dismiss(); } }]
    }).then(alert => alert.present());
  }

  presentToast(message: string) {
    this.toastCtrl.create({
      message: message,
      duration: 2000,
      position: 'bottom'
    }).then(toast => toast.present());
  }


}
