import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';

import { ArtistInfoPopoverPage } from './artist-info-popover';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild([
      {
        path: '',
        component: ArtistInfoPopoverPage
      }
    ])
  ],
  declarations: [ArtistInfoPopoverPage]
})
export class ArtistInfoPopoverPageModule {}
