import { Component } from '@angular/core';
import { NavController, AlertController, ToastController, PopoverController, NavParams } from '@ionic/angular';
import { MylistsServiceProvider } from '../../../services/other/mylists-service/mylists-service';
import { MyLists } from '../../../models/list/my-lists';
import { CustomList } from '../../../models/list/custom-list';
import { BasicArtist } from '../../../models/artist/basic-artist';
import { FullArtist } from '../../../models/artist/full-artist';
import { SettingsPage } from '../../main/settings/settings';
import { LoadingPage } from '../../secondary/loading/loading';
import { AddToCustomListAlert } from '../../alerts/add-to-custom-list-alert';
import { ActivatedRoute, Router } from '@angular/router';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser/ngx';
import { DiscographyServiceProvider } from '../../../services/other/discography-service/discography-service';

/**
 * Generated class for the ArtistInfoPopoverPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-artist-info-popover',
  templateUrl: 'artist-info-popover.html',
  styleUrls: [
    'artist-info-popover.scss'
  ]
})
export class ArtistInfoPopoverPage {

  public myLists: MyLists;
  public fullArtist: FullArtist;

  constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController,
    public toastCtrl: ToastController, public popoverCtrl: PopoverController, public myListsService: MylistsServiceProvider,
    public discographyService: DiscographyServiceProvider, public iab: InAppBrowser) {
    this.fullArtist = navParams.get('fullArtist');
    myListsService.getMyLists().then(myLists => this.myLists = myLists);
  }

  public addRemoveFollow() {
    if (this.checkFollowed()) {
      this.discographyService.unfollowArtist(this.fullArtist);
    }
    else {
      this.discographyService.followArtist(this.fullArtist);
    }
    this.popoverCtrl.dismiss();
  }

  public checkFollowed() {
    return this.discographyService.isArtistFollowed(this.fullArtist);
  }  

  public addToCustomListPrompt() {
    AddToCustomListAlert.createForArtist(this.alertCtrl, this.myLists, this.myListsService, this.fullArtist).then((response: string) => {
      this.presentToast(response);
      this.popoverCtrl.dismiss();
    });
  }

  public openInBrowser() {
    if (this.fullArtist.url) {
      let options: InAppBrowserOptions = {
        location: 'no',
        hidden: 'no'
      };      
      let target = "_system";      
      const browser = this.iab.create(this.fullArtist.url, target, options);
      this.popoverCtrl.dismiss();
    }
  }  

  presentToast(message: string) {
    this.toastCtrl.create({
      message: message,
      duration: 2000,
      position: 'bottom'
    }).then(toast => toast.present());
  }

}
