import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';

import { RecListPopoverPage } from './rec-list-popover';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild([
      {
        path: '',
        component: RecListPopoverPage
      }
    ])
  ],
  declarations: [RecListPopoverPage]
})
export class RecListPopoverPageModule {}
