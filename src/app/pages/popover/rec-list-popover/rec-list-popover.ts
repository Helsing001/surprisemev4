import { Component } from '@angular/core';
import { NavController, AlertController, ToastController, PopoverController, NavParams, ModalController } from '@ionic/angular';
import { CustomList } from '../../../models/list/custom-list';
import { MylistsServiceProvider } from '../../../services/other/mylists-service/mylists-service';
import { MyLists } from '../../../models/list/my-lists';
import { FullArtist } from '../../../models/artist/full-artist';
import { AddToCustomListAlert } from '../../alerts/add-to-custom-list-alert';
import { Router, ActivatedRoute } from '@angular/router';
import { GeneratePlaylistModal } from '../../secondary/generate-playlist/generate-playlist.page';
import { ProviderService } from '../../../services/other/provider-service/provider-service';

/**
 * Generated class for the RecListPopoverPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-rec-list-popover',
  templateUrl: 'rec-list-popover.html',
  styleUrls: [
    'rec-list-popover.scss'
  ]
})
export class RecListPopoverPage {

  public myLists: MyLists;
  public recArtistList: Array<FullArtist>;

  constructor(public navCtrl: NavController, public navParams: NavParams, public popoverCtrl: PopoverController,
    public alertCtrl: AlertController, public toastCtrl: ToastController, public modalCtrl: ModalController,
    public myListsService: MylistsServiceProvider, public providerService: ProviderService, public route: ActivatedRoute) {}

  ionViewDidLoad() {
  }

  public addToCustomListPrompt() {
    var result: any = {};
    result.addToCustomList = true;
    this.popoverCtrl.dismiss(result);
  }

  public generate() {
    var result: any = {};
    result.generate = true;
    this.popoverCtrl.dismiss(result);
  }

}
