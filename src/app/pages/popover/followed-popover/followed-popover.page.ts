import { Component, OnInit } from '@angular/core';
import { MylistsServiceProvider } from '../../../services/other/mylists-service/mylists-service';
import { PopoverController, AlertController, ToastController } from '@ionic/angular';
import { ExportedLists } from '../../../models/list/exported-lists';
import { MyLists } from '../../../models/list/my-lists';
import { CustomList } from '../../../models/list/custom-list';
import { ɵINTERNAL_BROWSER_PLATFORM_PROVIDERS } from '@angular/platform-browser';
import { AlertButton } from '@ionic/core';
import { skip } from 'rxjs/operators';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-followed-popover',
  templateUrl: './followed-popover.page.html',
  styleUrls: ['./followed-popover.page.scss'],
})
export class FollowedPopoverPage {

  constructor(public popoverCtrl: PopoverController) {
  }

  public refreshAll() {
    var result: any = {};
    result.refresh = true;    
    this.popoverCtrl.dismiss(result);
  }

  public unlistened() {
    var result: any = {};
    result.unlistened = true;    
    this.popoverCtrl.dismiss(result);
  }  

  public shareList() {
    var result: any = {};
    result.share = true;    
    this.popoverCtrl.dismiss(result);    
  }

  public importList() {
    var result: any = {};
    result.import = true;    
    this.popoverCtrl.dismiss(result);    
  }

  public info() {
    var result: any = {};
    result.info = true;    
    this.popoverCtrl.dismiss(result);
  }    

}
