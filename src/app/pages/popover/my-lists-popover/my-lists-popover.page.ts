import { Component, OnInit } from '@angular/core';
import { MylistsServiceProvider } from '../../../services/other/mylists-service/mylists-service';
import { PopoverController, AlertController, ToastController } from '@ionic/angular';
import { ExportedLists } from '../../../models/list/exported-lists';
import { MyLists } from '../../../models/list/my-lists';
import { CustomList } from '../../../models/list/custom-list';
import { ɵINTERNAL_BROWSER_PLATFORM_PROVIDERS } from '@angular/platform-browser';
import { AlertButton } from '@ionic/core';
import { skip } from 'rxjs/operators';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-my-lists-popover',
  templateUrl: './my-lists-popover.page.html',
  providers: [DatePipe],
  styleUrls: ['./my-lists-popover.page.scss'],
})
export class MyListsPopoverPage {

  public OVERWRITE = 1;
  public OVERWRITE_ALL = 2;
  public SKIP = 3;
  public SKIP_ALL = 4;

  public myLists: MyLists;

  constructor(public popoverCtrl: PopoverController, public alertCtrl: AlertController, public datepipe: DatePipe,
    public toastCtrl: ToastController, public myListsServiceProvider: MylistsServiceProvider) {
    myListsServiceProvider.getMyLists().then(myLists => this.myLists = myLists);
  }

  public importList() {
    this.myListsServiceProvider.import()
      .then((exportedLists: ExportedLists) => this.applyImportLogic(exportedLists))
      .catch(err => { console.log(err); this.presentToast("An error occurred"); })
    this.popoverCtrl.dismiss();
  }

  public applyImportLogic(exportedLists: ExportedLists) {
    let conflicts = 0;
    let addOrReplaceLists: Array<CustomList> = new Array();
    exportedLists.customLists.forEach(rawList => {
      if (this.hasConflict(rawList)) conflicts++;
    });
    this.applyImportLogicPerList(exportedLists, addOrReplaceLists, conflicts, false, false);
  }

  public applyImportLogicPerList(exportedLists: ExportedLists, addOrReplaceLists: Array<CustomList>,
    conflictsRemaining: number, overwriteAll: boolean, skipAll: boolean) {
    if (exportedLists.customLists.length > 0) {
      var rawList = exportedLists.customLists.pop();
      var customList = this.myLists.constructFullCustomList(rawList);
      if (this.hasConflict(rawList)) {
        conflictsRemaining--;
        if (!overwriteAll && !skipAll) {
          this.presentConflictAlert(rawList, conflictsRemaining).then((choice: number) => {
            switch (choice) {
              case this.OVERWRITE:
                addOrReplaceLists.push(customList);
                this.applyImportLogicPerList(exportedLists, addOrReplaceLists, conflictsRemaining, false, false);
                break;
              case this.OVERWRITE_ALL:
                addOrReplaceLists.push(customList);
                this.applyImportLogicPerList(exportedLists, addOrReplaceLists, conflictsRemaining, true, false);
                break;
              case this.SKIP:
                this.applyImportLogicPerList(exportedLists, addOrReplaceLists, conflictsRemaining, false, false);
                break;
              case this.SKIP_ALL:
                this.applyImportLogicPerList(exportedLists, addOrReplaceLists, conflictsRemaining, false, true);
                break;
            }
          })
        }
        else if (overwriteAll) {
          addOrReplaceLists.push(customList);
          this.applyImportLogicPerList(exportedLists, addOrReplaceLists, conflictsRemaining, true, false);
        }
        else if (skipAll) {
          this.applyImportLogicPerList(exportedLists, addOrReplaceLists, conflictsRemaining, false, true);
        }
      }
      else {
        addOrReplaceLists.push(customList);
        this.applyImportLogicPerList(exportedLists, addOrReplaceLists, conflictsRemaining, overwriteAll, skipAll);
      }
    }
    else {
      if (addOrReplaceLists.length > 0) {
        this.myListsServiceProvider.addOrReplaceLists(addOrReplaceLists);
        this.presentToast(addOrReplaceLists.length + " lists imported");
      }
    }
  }

  public hasConflict(rawList: CustomList): boolean {
    if (rawList.listType === 'fav' && this.myLists.favList.listArtists.length > 0) return true;
    if (rawList.listType === 'ban' && this.myLists.banList.listArtists.length > 0) return true;
    else {
      if (this.myLists.customLists.find(customList => customList.listName === rawList.listName && customList.listArtists.length > 0)) return true;
    }
    return false;
  }

  public presentConflictAlert(customList: CustomList, conflictsRemaining: number) {
    return new Promise(resolve => {
      var buttons: Array<AlertButton> = [];
      buttons.push({
        text: 'Skip',
        handler: result => {
          resolve(this.SKIP);
        }
      });
      buttons.push({
        text: 'Overwrite',
        handler: result => {
          resolve(this.OVERWRITE);
        }
      });
      if (conflictsRemaining > 0) {
        buttons.push({
          text: 'Skip All (' + conflictsRemaining + " more conflicts)",
          handler: result => {
            resolve(this.SKIP_ALL);
          }
        });
        buttons.push({
          text: 'Overwrite All (' + conflictsRemaining + " more conflicts)",
          handler: result => {
            resolve(this.OVERWRITE_ALL);
          }
        });
      }
      this.alertCtrl.create({
        header: 'List Conflict',
        subHeader: customList.listName + " already exists. What would you like to do?",
        buttons: buttons
      }).then(alert => alert.present());
    });
  }

  public shareList() {
    var timestamp = new Date().getTime();
    var formattedDate = this.datepipe.transform(timestamp, 'yyyyMMddHHmmss');
    this.myListsServiceProvider.share(this.myLists.getAllLists(), "AllMyLists-" + formattedDate)
      .catch(err => { console.log(err); this.presentToast("An error occurred"); });
    this.popoverCtrl.dismiss();
  }

  presentToast(message: string) {
    this.toastCtrl.create({
      message: message,
      duration: 2000,
      position: 'bottom'
    }).then(toast => toast.present());
  }

}
