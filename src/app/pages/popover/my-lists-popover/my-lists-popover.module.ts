import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MyListsPopoverPage } from './my-lists-popover.page';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild([ 
      {
        path: '',
        component: MyListsPopoverPage
      }
    ])
  ],
  declarations: [MyListsPopoverPage]
})
export class MyListsPopoverPageModule {}
