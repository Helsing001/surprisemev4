import { AlertController, Platform, LoadingController } from '@ionic/angular';
import { MusicApp } from '../../models/other/music-app';
import { orderBy } from 'lodash';
import { AlertInput } from '@ionic/core';
import { ProviderService } from '../../services/other/provider-service/provider-service';

export class SelectMusicAppsAlert {

    public static presentMusicApps(alertCtrl: AlertController, platform: Platform, loadingController: LoadingController, navigator: any, providerService: ProviderService): Promise<Array<MusicApp>> {
        return new Promise(resolve => {
            if (platform.is("cordova")) {
                const loading = loadingController.create({
                    message: 'Please wait...'
                }).then(l => l.present());
                navigator.IntentList.getList(
                    appList => {
                        var allApps: Array<MusicApp> = orderBy(appList.map(app => new MusicApp(app.label, app.package)), 'appName');
                        var inputs: Array<AlertInput> = [];

                        allApps.forEach((musicApp) => {
                            inputs.push(this.createCheckboxInput(musicApp.appName, musicApp, providerService.isMusicApp(musicApp), false));
                        })
                        inputs = orderBy(inputs, 'checked', 'desc');

                        loadingController.dismiss();
                        this.presentAlert('Music Apps', inputs, alertCtrl).then((selectedApps: Array<MusicApp>) => {
                            resolve(selectedApps);
                        });
                    },
                    error => {
                        console.log(error);
                    });
            }

        })
    }

    private static createCheckboxInput(label, value, checked, disabled): AlertInput {
        var input = {
            type: 'checkbox',
            label: label,
            value: value,
            checked: checked,
            disabled: disabled
        };
        return input as AlertInput;
    }

    public static presentAlert(title: string, inputs: Array<AlertInput>, alertCtrl: AlertController) {
        return new Promise(resolve => {
            alertCtrl.create({
                header: title,
                inputs: inputs,
                buttons: [
                    'Cancel',
                    {
                        text: 'OK',
                        handler: result => {
                            resolve(result);
                        }
                    }
                ]
            }).then(alert => alert.present());
        });
    }

}