import { AlertController } from '@ionic/angular';
import { BasicDiscoArtist } from '../../models/artist/basic-disco-artist';
import { RefreshResult } from '../../models/other/refresh-result';

export class RefreshDiscoAlert {

  public static showArtistsWithNewAlbums(alertCtrl: AlertController, result: RefreshResult) {
    var message = "";
    if (result.artistsWithNewAlbums.length == 0) {
      message = "<p>No artists have new albums since the last refresh</p>"
    }
    else {
      message = "<p>The following artists have new albums since the last refresh</p>"
      message = message + "<ul>";
      result.artistsWithNewAlbums.forEach(artist => message = message + "<li>" + artist.artist.artist + "</li>");
      message = message + "</ul>"
    }
    if (result.notFoundArtists.length != 0) {
      message = "<p>The following artists were not found on the current discography provider. You can try to unfollow them, set the info provider the same as the discography provider and search & follow them again</p>"
      message = message + "<ul>";
      result.notFoundArtists.forEach(artist => message = message + "<li>" + artist.artist.artist + "</li>");
      message = message + "</ul>"      
    }
    let alert = alertCtrl.create({
      header: "Info",
      message: message,
      buttons: ['OK']
    }).then(alert => alert.present());
  }

}