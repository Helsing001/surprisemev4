import { AlertController } from "@ionic/angular";
import { MyLists } from "../../models/list/my-lists";
import { FullArtist } from "../../models/artist/full-artist";
import { CustomList } from "../../models/list/custom-list";
import { MylistsServiceProvider } from "../../services/other/mylists-service/mylists-service";
import { AlertInput } from '@ionic/core';

export class AddToCustomListAlert {

    public static createForArtist(alertCtrl: AlertController, myLists: MyLists, myListsService: MylistsServiceProvider, fullArtist: FullArtist) {
        return new Promise((resolve) => {
            var inputs: Array<AlertInput> = [];

            myLists.customLists.forEach((customList: CustomList) => {
                var containsArtist = false;
                if (customList.listArtists.find(artist => artist.artist == fullArtist.artist)) {
                    containsArtist = true;
                }
                inputs.push(this.createCheckboxInput(customList.listName, customList.listName, containsArtist));
            })

            this.presentAddToCustomListAlert(alertCtrl, myLists, myListsService, fullArtist, undefined, inputs).then((response: string) => {
                resolve(response);
            });
        });
    }

    public static createForList(alertCtrl: AlertController, myLists: MyLists, myListsService: MylistsServiceProvider, recArtistList: Array<FullArtist>) {
        return new Promise((resolve) => {
            var inputs: Array<AlertInput> = [];

            myLists.customLists.forEach((customList: CustomList) => {
                inputs.push(this.createCheckboxInput(customList.listName, customList.listName, false));
            })

            this.presentAddToCustomListAlert(alertCtrl, myLists, myListsService, undefined, recArtistList, inputs).then((response: string) => {
                resolve(response);
            });
        });
    }

    private static presentAddToCustomListAlert(alertCtrl: AlertController, myLists: MyLists, myListsService: MylistsServiceProvider,
        fullArtist: FullArtist, recArtistList: Array<FullArtist>, inputs: Array<AlertInput>) {
        return new Promise(resolve => {
            alertCtrl.create({
                header: 'Custom Lists',
                inputs: inputs,
                buttons: [
                    {
                        text: 'New',
                        handler: () => {
                            this.presentNewCustomListAlert(alertCtrl).then((data) => {
                                if (data[0].length <= 15) {
                                    if (!myLists.customLists.find(customListB => customListB.listName == data[0])) {
                                        var customList = new CustomList(data[0], CustomList.CUSTOM, data[1]);
                                        if (fullArtist) {
                                            myListsService.addArtistToNewList(customList, fullArtist);
                                        }
                                        else {
                                            myListsService.addArtistsToNewList(customList, recArtistList);
                                        }

                                        resolve("Done");
                                    }
                                    else {
                                        resolve("List exists");
                                    }
                                }
                                else{
                                    resolve("Name is too long")
                                }
                            });
                        }
                    },
                    {
                        text: 'OK',
                        handler: result => {
                            var customLists: Array<CustomList> = new Array();
                            result.forEach((listName) => {
                                var customList = myLists.customLists.find(customList => customList.listName == listName);
                                customLists.push(customList);
                            })
                            if (fullArtist) {
                                myListsService.addRemoveArtistFromLists(customLists, fullArtist);
                            }
                            else {
                                myListsService.addArtistsToLists(customLists, recArtistList);
                            }
                            resolve("Done");
                        }
                    }
                ]
            }).then(alert => alert.present());
        });
    }

    private static presentNewCustomListAlert(alertCtrl: AlertController) {
        return new Promise(resolve => {
            alertCtrl.create({
                header: 'New Custom List',
                inputs: [this.createTextInput("Name", "Name")],
                buttons: [
                    'Cancel',
                    {
                        text: 'OK, part of library',
                        handler: result => {
                            result[1] = true;
                            resolve(result);
                        }
                    },
                    {
                        text: 'OK, noy yet listened',
                        handler: result => {
                            result[1] = false;
                            resolve(result);
                        }
                    }
                ]
            }).then(alert => alert.present());
        });
    }

    private static createCheckboxInput(label, value, checked): AlertInput {
        var input = {
            type: "checkbox",
            label: label,
            value: value,
            checked: checked
        };
        return input as AlertInput;
    }

    private static createTextInput(label, value): AlertInput {
        var input = {
            type: 'text',
            value: value
        };
        return input as AlertInput;
    }

}