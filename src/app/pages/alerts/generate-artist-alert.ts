import { AlertController, ToastController } from '@ionic/angular';
import { GenerateResult } from '../../models/other/generate-result';

export class GenerateArtistAlert {

    public static showGenerateResult(alertCtrl: AlertController, toastCtrl: ToastController, generateResult: GenerateResult) {
        if (generateResult.hasArtists()) {
            this.showArtistsNotFoundAlert(alertCtrl, generateResult);
        }
        else {
            this.presentToast(toastCtrl, "Success");
        }
    }

    private static showArtistsNotFoundAlert(alertCtrl: AlertController, generateResult: GenerateResult) {
      var message = "";
        if (generateResult.notFoundArtists && generateResult.notFoundArtists.length > 0) {
            message = "<p>The following artists weren't found</p>"
            message = message + "<ul>";
            generateResult.notFoundArtists.forEach(artist => message = message + "<li>" + artist.artist + "</li>");
            message = message + "</ul>"
        }
        if (generateResult.noSongsArtists && generateResult.noSongsArtists.length > 0) {
            message = message + "<p>The following artists have no songs available on provider</p>"
            message = message + "<ul>";
            generateResult.noSongsArtists.forEach(artist => message = message + "<li>" + artist.artist + "</li>");
            message = message + "</ul>"
        }
        let alert = alertCtrl.create({
            header: "Info",
            message: message,
            buttons: ['OK']
        }).then(alert => alert.present());
    }

    private static presentToast(toastCtrl: ToastController, message: string) {
        toastCtrl.create({
            message: message,
            duration: 2000,
            position: 'bottom'
        }).then(toast => toast.present());
    }
}