import { Injectable } from '@angular/core';
import { GenericService } from '../../../models/generic/generic-service';
import { GenericParams } from '../../../models/params/search-params';
import { BasicArtist, ProviderId } from '../../../models/artist/basic-artist';
import { SimilarArtist } from '../../../models/artist/similar-artist';
import { FullArtist } from '../../../models/artist/full-artist';
import { ArtistExtraInfo } from '../../../models/artist/artist-extra-info';
import { ListenedTrack } from '../../../models/other/listened-track';
import { ExternalList } from '../../../models/list/external-list';
import { GenerateParams } from '../../../models/params/generate-params';
import { GenerateResult } from '../../../models/other/generate-result';
import { ProviderSettings } from '../../../models/providers/provider-settings';
import { StorageServiceProvider } from '../../other/storage-service/storage-service';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser/ngx';
import { HttpServiceProvider } from '../../other/http-service/http-service';
import { LoadingServiceProvider } from '../../other/loading-service/loading-service';
import { ProviderOperations } from '../../../models/providers/provider-operations';
import { AllServices } from '../../../models/other/all-services';
import { UtilsServiceProvider } from '../../other/utils-service/utils-service';
import { AlertController, ToastController } from '@ionic/angular';
import { ArtistAlbum } from '../../../models/album/artist-album';
import { BasicDiscoArtist } from '../../../models/artist/basic-disco-artist';
import { orderBy } from 'lodash';

@Injectable()
export class DeezerServiceProvider extends GenericService {

  public static PROVIDER = 'Deezer';
  public static DEFAULT_SETTINGS = new ProviderSettings(4, DeezerServiceProvider.PROVIDER, true, true, true, true, false, true, true, false, false, true);

  private SAME_ALBUM_REGEX = [/\(deluxe.*/g, /\(extended\)/g, /\(edited.*/g, /\(instrumental.*/g]

  private apiConfig = {
    appId: "503822",
    redirectUrl: "https://iclutter.synology.me/deezer",
    scopes: ["basic_access", "manage_library", "listening_history"]
  };

  private MAX_COUNT = 450;
  private SEARCH_LIMIT = 20;
  private GENERATE_BATCH_SIZE = 500;

  private apiURL = 'https://api.deezer.com/';
  private getMeURL = 'user/me?access_token={0}';
  private getUserArtistsURL = 'user/me/artists?access_token={0}&limit=1000';
  private getSavedTracksURL = 'user/me/tracks?access_token={0}&limit=1000';
  private getHistoryURL = 'user/me/history?access_token={0}&limit=1000';
  private getFlowURL = 'user/me/flow?access_token={0}';
  private getRecsURL = 'user/me/recommendations/artists?access_token={0}';
  private getGlobalURL = 'chart/0/artists?limit=300';
  private getPlaylistsURL = 'user/me/playlists?access_token={0}';
  private getPlaylistTracksURL = 'playlist/{0}/tracks?access_token={1}&limit=1000';
  private getPlaylistCreateURL = 'user/me/playlists?request_method=POST&access_token={0}&title={1}';
  private getPlaylistUpdateURL = 'playlist/{0}?request_method=POST&access_token={1}&title={2}&public={3}&description={4}';
  private getPlaylistAddTracksURL = 'playlist/{0}/tracks?request_method=POST&access_token={1}&songs={2}';
  private getSimilarArtistsURL = 'artist/{0}/related?limit=20';
  private getSearchArtistURL = 'search/artist?q={0}';
  private getArtistURL = 'artist/{0}';
  private getAlbumsURL = 'artist/{0}/albums';
  private getSongsURL = 'artist/{0}/top';
  private getAlbumTracksURL = 'album/{0}/tracks'

  private authResolve: any;
  private accessToken: string;
  private expireTime: Date;
  private apiDelay = 200;
  private loggedIn = false;
  private userId: string;

  private providerSettings: ProviderSettings;

  constructor(public iab: InAppBrowser, public alertCtrl: AlertController, public storageService: StorageServiceProvider, public toastCtrl: ToastController,
    public httpService: HttpServiceProvider, public loadingService: LoadingServiceProvider, public utilsService: UtilsServiceProvider) {
    super();

    this.storageService.getSettingsCache().then(settingsCache => {
      var deezerSettings: ProviderSettings;
      if (!settingsCache || !settingsCache.deezerProviderSettings) {
        deezerSettings = DeezerServiceProvider.DEFAULT_SETTINGS;
      }
      else {
        this.processVersionDifferences(settingsCache.deezerProviderSettings);
        deezerSettings = settingsCache.deezerProviderSettings;
      }
      this.providerSettings = deezerSettings;
      if (deezerSettings.getUsername()) {
        this.storageService.getSettingsCache().then(settingsCache => {
          if (settingsCache && settingsCache.enabledProviders && settingsCache.enabledProviders.indexOf("Deezer") != -1) {
            this.auth();
          }
        })
      }
    })
  }

  private processVersionDifferences(settings: ProviderSettings) {
    if (!settings.discography) {
      settings.setDiscography(this.storageService, true);
    }
  }

  getOwnName(): string {
    return DeezerServiceProvider.PROVIDER;
  }

  auth(): Promise<boolean> {
    return new Promise(resolve => {
      var expiredAuth = true;
      if (this.expireTime) {
        var expiredAuth = this.expireTime.getTime() <= new Date().getTime() - 1;
      }
      if (this.loggedIn && !expiredAuth) {
        resolve(true);
        return;
      }
      let options: InAppBrowserOptions = {
        location: 'no',
        hidden: 'no'
      };
      this.authResolve = resolve;
      let target = "_system";
      if (!this.providerSettings.getUsername()) {
        let alert = this.alertCtrl.create({
          header: "Info",
          message: "After logging in to Deezer, Android will ask you to choose an application to open, you should choose to always open SurpriseMe",
          buttons: [{
            text: 'OK',
            handler: result => {
              this.openBrowserForAuth(target, options);
            }
          }]
        }).then(alert => alert.present());
      }
      else {
        this.openBrowserForAuth(target, options);
      }
    });
  }

  private openBrowserForAuth(target: string, options: InAppBrowserOptions) {
    const browser = this.iab.create(
      "https://connect.deezer.com/oauth/auth.php?app_id=" +
      encodeURIComponent(this.apiConfig.appId) +
      "&response_type=token&redirect_uri=" +
      encodeURIComponent(this.apiConfig.redirectUrl) +
      "&perms=" +
      encodeURIComponent(this.apiConfig.scopes.join(',')),
      target,
      options
    );
  }

  public handleAuthResponse(accessToken: string, expiresIn: number) {
    console.log("Auth Deezer success");
    this.accessToken = accessToken;
    var tmpTime = new Date();
    tmpTime.setSeconds(tmpTime.getSeconds() + expiresIn);
    this.expireTime = tmpTime;
    this.httpService.httpGet(this.getUrlForUser(accessToken))
      .then(data => {
        if (data.error) {
          console.log(data.error)
          if (data.error.message) {
            if (data.error.message === 'Account permission restricted - free service is closed') {
              this.presentToast('Account permission restricted - free service is closed')
            }
            else {
              this.presentToast('An error occurred')
            }
          }
          else {
            this.presentToast('An error occurred')
          }
          this.forgetAuth();
        }
        this.userId = data.id;
        if (this.providerSettings.getUsername() !== data.name) {
          this.providerSettings.setUsername(this.storageService, data.name);
        }
      })
      .catch(error => {
        console.log(error)
        this.presentToast('An error occurred')
        this.forgetAuth();
      })
      .finally(() => {
        var successLogin = this.accessToken !== undefined
        this.loggedIn = successLogin;
        if (this.authResolve) {
          this.authResolve(successLogin);
          this.authResolve = undefined;
        }
      })
  }

  forgetAuth() {
    this.accessToken = undefined;
    this.expireTime = undefined;
    this.providerSettings.setUsername(this.storageService, undefined);
    this.loggedIn = false;
  }

  isLongAuth(): boolean {
    return false;
  }

  async fetchUserArtists(genericParams: GenericParams): Promise<BasicArtist[]> {
    var authed = await this.auth();
    if (!authed) {
      throw new Error("User not logged in");
    }
    try {
      return await this.fetch(this.getUrlForUserArtists(this.accessToken), genericParams.timestamp, this.mapBasicArtistInRoot, BasicArtist.mergeListenedWithAdd)
    } catch (e) {
      console.log(e);
      throw new Error('An error occurred');
    }
  }

  async fetchSourceArtists(deezerParams: any, genericParams: GenericParams): Promise<BasicArtist[]> {
    var authed = await this.auth();
    if (!authed) {
      throw new Error("User not logged in");
    }
    var sourcePromise: Promise<Array<BasicArtist>>;
    var limit = genericParams.showSources ? genericParams.resultLimit : genericParams.historyLimit;
    switch (deezerParams.deezerSource) {
      case "Followed Artists": sourcePromise = this.fetch(this.getUrlForUserArtists(this.accessToken), genericParams.timestamp, this.mapBasicArtistInRoot, BasicArtist.mergeListenedWithAdd); break;
      case "Saved Tracks": sourcePromise = this.fetch(this.getUrlForSavedTracks(this.accessToken), genericParams.timestamp, this.mapBasicArtistInNode, BasicArtist.mergeListenedWithAdd); break;
      case "History": sourcePromise = this.fetch(this.getUrlForHistory(this.accessToken), genericParams.timestamp, this.mapBasicArtistInNode, BasicArtist.mergeListenedWithAdd, limit); break;
      case "Flow": sourcePromise = this.fetch(this.getUrlForFlow(this.accessToken), genericParams.timestamp, this.mapBasicArtistInNode, BasicArtist.mergeListenedWithAdd); break;
      case "Recommendations": sourcePromise = this.fetch(this.getUrlForRecs(this.accessToken), genericParams.timestamp, this.mapBasicArtistInRoot, BasicArtist.mergeListenedWithAdd); break;
      case "Global": sourcePromise = this.fetch(this.getUrlForGlobal(), genericParams.timestamp, this.mapBasicArtistInRoot, BasicArtist.mergeListenedWithAdd, Number.MAX_SAFE_INTEGER, this.weightFromMaxNumber(300)); break;
      case "Followed List": sourcePromise = this.fetch(this.getUrlForPlaylistTracks(deezerParams.playlist.id, this.accessToken), genericParams.timestamp, this.mapBasicArtistInNode, BasicArtist.mergeListenedWithAdd); break;
      default: throw new Error('Unknown source');
    }
    if (sourcePromise) {
      try {
        return await sourcePromise;
      } catch (e) {
        console.log(e);
        throw new Error('An error occurred');
      }
    }
  }

  async fetchSimilarArtists(userArtists: BasicArtist[], sourceArtists: BasicArtist[], genericParams: GenericParams): Promise<SimilarArtist[]> {
    var historyLimit = genericParams.historyLimit;
    if (!historyLimit || historyLimit > sourceArtists.length) {
      historyLimit = sourceArtists.length;
    }
    this.loadingService.addToReferenceProgress(historyLimit, this.providerSettings.orderId, genericParams.timestamp);

    var recArtists: Array<SimilarArtist> = new Array();
    sourceArtists = sourceArtists.slice(0, historyLimit);
    for (let i = 0; i < historyLimit; i++) {
      let sourceArtist = sourceArtists[i];
      var basicSimilarArtists: Array<BasicArtist> = new Array();
      try {
        if (sourceArtist.getIdForProvider(DeezerServiceProvider.PROVIDER)) {
          basicSimilarArtists = await this.fetch(this.getUrlForSimilarArtists(sourceArtist.getIdForProvider(DeezerServiceProvider.PROVIDER)), genericParams.timestamp,
            this.mapBasicArtistInRoot, BasicArtist.mergeListenedWithAdd, Number.MAX_SAFE_INTEGER, this.weightMultiplier(sourceArtists[i].weight, 20), false);
        }
        else {
          var matchedArtist = await this.matchArtist(sourceArtist, false);
          if (matchedArtist) basicSimilarArtists = await this.fetch(this.getUrlForSimilarArtists(matchedArtist.getIdForProvider(DeezerServiceProvider.PROVIDER)), genericParams.timestamp,
            this.mapBasicArtistInRoot, BasicArtist.mergeListenedWithAdd, Number.MAX_SAFE_INTEGER, this.weightMultiplier(sourceArtist.weight, 20), false);
        }
      } catch (e) {
        throw new Error('An error occurred');
      }
      basicSimilarArtists.forEach(basicArtist => {
        if (!userArtists.find(elem => elem.artist.toLowerCase() == basicArtist.artist.toLowerCase())) {
          var artistIndex = recArtists.findIndex(elem => elem.artist.toLowerCase() == basicArtist.artist.toLowerCase());
          if (artistIndex < 0) {
            var sourceSimilarArtists: Array<BasicArtist> = [new BasicArtist(sourceArtists[i].artist, basicArtist.weight, sourceArtists[i].id)];
            recArtists.push(new SimilarArtist(basicArtist.artist, basicArtist.weight, basicArtist.id, sourceSimilarArtists, basicArtist.id));
          }
          else {
            recArtists[artistIndex].weight = recArtists[artistIndex].weight + basicArtist.weight;
            recArtists[artistIndex].insertSourceArtist(sourceArtists[i], basicArtist.weight);
          }
        }
      })

      this.loadingService.addToCurrentProgress(this.providerSettings.orderId, genericParams.timestamp);

      if (i < historyLimit - 1) {
        await new Promise(resolve => setTimeout(resolve, this.apiDelay));
      }
    }
    return SimilarArtist.sortByWeight(recArtists);
  }

  async fetchSimilarArtistsInfo(recArtists: SimilarArtist[], genericParams: GenericParams): Promise<FullArtist[]> {
    var searchTimestamp: number = undefined; if (genericParams) searchTimestamp = genericParams.timestamp;
    var fullArtists: Array<FullArtist> = new Array();
    this.loadingService.addToReferenceProgress(recArtists.length, this.providerSettings.orderId, searchTimestamp);
    for (let i = 0; i < recArtists.length && this.loadingService.isLoadActive(searchTimestamp); i++) {
      var similarArtist = recArtists[i];
      var fullArtist = await this.fetchArtistInfo(similarArtist);
      fullArtist.mergeWithSimilarArtistInfo(similarArtist);
      fullArtists.push(fullArtist);
      this.loadingService.addToCurrentProgress(this.providerSettings.orderId, searchTimestamp);
      if (i < recArtists.length - 1 && this.loadingService.isLoadActive(searchTimestamp)) {
        await new Promise(resolve => setTimeout(resolve, this.apiDelay));
      }
    }
    return fullArtists;
  }

  async fetchArtistInfo(artist: BasicArtist): Promise<FullArtist> {
    var matchedArtist: FullArtist;
    if (!artist.getIdForProvider(DeezerServiceProvider.PROVIDER)) {
      matchedArtist = await this.matchArtist(artist, false);
      if (matchedArtist) {
        var deezerId = matchedArtist.getIdForProvider(DeezerServiceProvider.PROVIDER);
        matchedArtist.id = artist.id;
        matchedArtist.addProvider(new ProviderId(DeezerServiceProvider.PROVIDER, deezerId));
      }
      else matchedArtist = FullArtist.fromBasicArtist(artist);
    }
    else {
      var json = await this.httpService.httpGet(this.getUrlForArtist(artist.getIdForProvider(DeezerServiceProvider.PROVIDER)));
      matchedArtist = this.mapFullArtistInRoot(json, artist.weight);
    }
    matchedArtist.artist = artist.artist;
    matchedArtist.weight = artist.weight;
    return matchedArtist;
  }

  async searchArtist(artistName: string): Promise<FullArtist[]> {
    var fullArtists: Array<FullArtist> = await this.fetch(this.getUrlForArtistSearch(artistName), undefined, this.mapFullArtistInRoot, this.noOpMerger, this.SEARCH_LIMIT);
    return fullArtists;
  }

  async fetchArtistExtraInfo(basicArtist: BasicArtist): Promise<ArtistExtraInfo> {
    var similarArtists: Array<FullArtist> = await this.fetch(this.getUrlForSimilarArtists(basicArtist.getIdForProvider(DeezerServiceProvider.PROVIDER)), undefined,
      this.mapFullArtistInRoot, this.noOpMerger, 5, this.fixedWeight(this.providerSettings.getSourceWeight()), false);
    var topAlbums: Array<ArtistAlbum> = await this.fetchAlbumsForClarification(basicArtist);
    var topSongs: Array<string> = await this.fetch(this.getUrlForSongs(basicArtist.getIdForProvider(DeezerServiceProvider.PROVIDER)), undefined,
      this.mapSong, this.noOpMerger, 5, this.ignoreWeight(), false);
    return new ArtistExtraInfo(similarArtists, topAlbums, topSongs);
  }

  async fetchAlbumsForClarification(basicArtist: BasicArtist): Promise<ArtistAlbum[]> {
    var albums: Array<ArtistAlbum> = await this.fetch(this.getUrlForAlbums(basicArtist.getIdForProvider(DeezerServiceProvider.PROVIDER)), undefined, this.mapAlbumsInRoot, ArtistAlbum.mergeByName, 5);
    return albums;
  }

  fetchArtistScrobbles(basicArtist: BasicArtist): Promise<number> {
    throw new Error("Method not implemented.");
  }

  fetchTrackScrobbles(basicArtist: BasicArtist, track: string): Promise<number> {
    throw new Error("Method not implemented.");
  }

  fetchListening(): Promise<ListenedTrack> {
    throw new Error("Method not implemented.");
  }

  async fetchPrivateLists(): Promise<ExternalList[]> {
    var playlists: Array<ExternalList> = new Array();
    var url = this.getUrlForPlaylists(this.accessToken);
    var authed = await this.auth();
    if (!authed) {
      throw new Error("User not logged in");
    }
    do {
      try {
        var json = await this.httpService.httpGet(url);
        json.data.forEach(element => {
          var playlist = new ExternalList(element.title, element.id, DeezerServiceProvider.PROVIDER);
          playlists.push(playlist);
        });
        url = json.next;
        if (url) {
          await new Promise(resolve => setTimeout(resolve, this.apiDelay));
        }
      } catch (e) {
        throw new Error('An error occurred');
      }
    } while (url);
    return playlists;
  }

  async generatePlaylist(generateParams: GenerateParams): Promise<GenerateResult> {
    var authed = await this.auth();
    if (!authed) {
      throw new Error("User not logged in");
    }
    if (!generateParams.customList.listArtists || generateParams.customList.listArtists.length == 0) {
      throw new Error("List is empty");
    }

    this.loadingService.searchTimestamp = generateParams.timestamp;
    this.loadingService.setLoadingPhase('Artist Tracks', [this.providerSettings.orderId], 0, 90);
    this.loadingService.addToReferenceProgress(generateParams.customList.listArtists.length, this.providerSettings.orderId, generateParams.timestamp);

    let notFoundArtists: Array<BasicArtist> = new Array();
    let noSongsArtists: Array<BasicArtist> = new Array();
    let chosenTracks: Array<string> = new Array();

    for (let i = 0; i < generateParams.customList.listArtists.length; i++) {
      var basicArtist = generateParams.customList.listArtists[i];
      if (!basicArtist.getIdForProvider(DeezerServiceProvider.PROVIDER)) {
        var matchedArtist = await this.matchArtist(basicArtist, false);
        if (!this.loadingService.isLoadActive(generateParams.timestamp)) {
          console.log("Deezer search cancelled");
          throw new Error("Search cancelled");
        }
        if (matchedArtist) {
          basicArtist = matchedArtist;
        }
        else {
          notFoundArtists.push(matchedArtist);
          continue;
        }
      }
      var topSongIds: Array<string> = await this.fetch(this.getUrlForSongs(basicArtist.getIdForProvider(DeezerServiceProvider.PROVIDER)), generateParams.timestamp,
        this.mapSongId, this.noOpMerger, 10, this.ignoreWeight(), false);
      if (topSongIds.length > 0) {
        var topSongsToChooseFromArtist: Array<string> = topSongIds.slice(0, generateParams.songsToChooseFrom);
        this.utilsService.shuffleArray(topSongsToChooseFromArtist);
        var chosenTracksFromArtist = topSongsToChooseFromArtist.slice(0, generateParams.songsPerArtist);
        chosenTracks.push(...chosenTracksFromArtist);
      }
      else {
        // bug deezer API, top songs intoarce uneori empty list desi artistul are melodii, voi alege random din top albume
        var albums: Array<ArtistAlbum> = await this.fetchAlbumsForClarification(basicArtist);
        if (!this.loadingService.isLoadActive(generateParams.timestamp)) {
          console.log("Deezer search cancelled");
          throw new Error("Search cancelled");
        }
        var topSongsToChooseFromArtist: Array<string> = new Array();
        for (let j = 0; j < albums.length; j++) {
          await new Promise(resolve => setTimeout(resolve, this.apiDelay));
          var album = albums[j];
          var albumTracks: Array<string> = await this.fetch(this.getUrlForAlbumTracks(album.albumId), generateParams.timestamp,
            this.mapSongId, this.noOpMerger, Number.MAX_SAFE_INTEGER, this.ignoreWeight(), false);
          topSongsToChooseFromArtist.push(...albumTracks);
        }
        if (topSongsToChooseFromArtist.length > 0) {
          this.utilsService.shuffleArray(topSongsToChooseFromArtist);
          var chosenTracksFromArtist = topSongsToChooseFromArtist.slice(0, generateParams.songsPerArtist);
          chosenTracks.push(...chosenTracksFromArtist);
        }
        else {
          noSongsArtists.push(basicArtist);
        }
      }
      this.loadingService.addToCurrentProgress(this.providerSettings.orderId, generateParams.timestamp);
      if (i < generateParams.customList.listArtists.length - 1) {
        await new Promise(resolve => setTimeout(resolve, this.apiDelay));
      }
    }

    if (chosenTracks.length > 0) {
      if (generateParams.shuffle) {
        this.utilsService.shuffleArray(chosenTracks);
      }
      chosenTracks = chosenTracks.filter((el, i, a) => i === a.indexOf(el))
      this.loadingService.setLoadingPhase('Generating Playlist', [this.providerSettings.orderId], 90, 10);
      this.loadingService.addToReferenceProgress(Math.floor((chosenTracks.length - 1) / this.GENERATE_BATCH_SIZE + 1), this.providerSettings.orderId, generateParams.timestamp);
      var playlistId;
      if (!generateParams.listToAddTo) {
        var json = await this.httpService.httpGet(this.getUrlForPlaylistCreate(this.accessToken, generateParams.playlistName));
        playlistId = json.id;
        json = await this.httpService.httpGet(this.getUrlForPlaylistUpdate(playlistId, this.accessToken, generateParams.playlistName,
          generateParams.privatePlaylist, generateParams.description));
      }
      else {
        playlistId = generateParams.listToAddTo.id;
      }
      for (let k = 0; k <= (chosenTracks.length - 1) / this.GENERATE_BATCH_SIZE; k++) {
        var answer = await this.httpService.httpGet(
          this.getUrlForPlaylistAddTracks(playlistId, this.accessToken, chosenTracks.slice(k * this.GENERATE_BATCH_SIZE, (k + 1) * this.GENERATE_BATCH_SIZE)));
        this.loadingService.addToCurrentProgress(this.providerSettings.orderId, generateParams.timestamp);
        if (k < generateParams.customList.listArtists.length - 1) {
          await new Promise(resolve => setTimeout(resolve, this.apiDelay));
        }
      }
    }

    return new GenerateResult(notFoundArtists, noSongsArtists);
  }

  getProviderSettings(): ProviderSettings {
    if (this.providerSettings) {
      return this.providerSettings;
    }
    else {
      return DeezerServiceProvider.DEFAULT_SETTINGS;
    }
  }

  getDefaultOperationsWithoutAuth(): string[] {
    var operationsNoAuth: Array<string> = new Array();
    operationsNoAuth.push(ProviderOperations.SIMILARITY);
    operationsNoAuth.push(ProviderOperations.INFO);
    operationsNoAuth.push(ProviderOperations.DISCOGRAPHY);
    return operationsNoAuth;
  }

  getProviderImage(enabled: boolean): string {
    if (enabled) return './assets/images/deezer.png'; else return './assets/images/deezer-greyed.png';
  }

  getApiDelay(): number {
    return this.apiDelay;
  }

  async getAllArtistAlbums(discoArtist: BasicDiscoArtist): Promise<ArtistAlbum[]> {
    var basicArtist = discoArtist.artist;
    if (!basicArtist.getIdForProvider(DeezerServiceProvider.PROVIDER)) {
      var matchedArtist = await this.matchArtist(basicArtist, false);
      if (!matchedArtist) {
        throw new Error("Artist not found");
      }
    }
    var discographyFilters = (await this.storageService.getSettingsCache()).discographyFilters;
    var albums: Array<ArtistAlbum> = await this.fetch(
      this.getUrlForAlbums(discoArtist.artist.getIdForProvider(DeezerServiceProvider.PROVIDER)),
      undefined,
      this.mapAlbumsInRoot,
      ArtistAlbum.mergeByName,
      Number.MAX_SAFE_INTEGER,
      position => this.MAX_COUNT,
      false,
      this.filterNonAlbums
    );
    albums = albums.filter(album => !albums.find(album2 => this.isSameAlbum(album, album2) && album.album !== album2.album)
      && !discographyFilters.find(filter => new RegExp(filter.toLowerCase()).test(album.album.toLowerCase())))
    return orderBy(albums, 'releaseDate', 'desc');
  }

  private isSameAlbum(album: ArtistAlbum, album2: ArtistAlbum): boolean {
    for (let i = 0; i < this.SAME_ALBUM_REGEX.length; i++) {
      var regex = this.SAME_ALBUM_REGEX[i]
      if (album2.album.toLowerCase() === album.album.toLowerCase().replace(regex, "").trim()) {
        return true;
      }
    }
    return false;
  }

  private async matchArtist(sourceArtist: BasicArtist, strict: boolean = true): Promise<FullArtist> {
    var searchResultArtists = await this.searchArtist(sourceArtist.artist);
    var filteredFirst3AndBySameName = searchResultArtists.filter((a, index) => a.artist.toLowerCase() === sourceArtist.artist.toLowerCase() || (index >= 0 && index <= 2));
    var matchedArtist: FullArtist;
    if (filteredFirst3AndBySameName.length == 0) {
      console.log("No artists on Deezer with the name " + sourceArtist.artist + " , will skip");
      return null;
    }
    else if (filteredFirst3AndBySameName.length > 1) {
      var albums: Array<ArtistAlbum> = await AllServices.providerService.getServiceProvider(sourceArtist.id[0].provider).fetchAlbumsForClarification(sourceArtist);
      if (albums.length > 0) {
        matchedArtist = await this.getArtistByMultipleAlbums(filteredFirst3AndBySameName, albums);
        if (!matchedArtist && !strict) matchedArtist = filteredFirst3AndBySameName[0];
      }
      else {
        matchedArtist = filteredFirst3AndBySameName.filter(a => a.artist.toLowerCase() === sourceArtist.artist.toLowerCase())[0];
      }
    }
    else {
      matchedArtist = filteredFirst3AndBySameName[0];
    }
    if (matchedArtist) sourceArtist.addProvider(new ProviderId(DeezerServiceProvider.PROVIDER, matchedArtist.getIdForProvider(DeezerServiceProvider.PROVIDER)));
    return matchedArtist;
  }

  private async getArtistByMultipleAlbums(filteredFirst3AndBySameName: Array<FullArtist>, albums: Array<ArtistAlbum>): Promise<FullArtist> {
    for (let i = 0; i < filteredFirst3AndBySameName.length; i++) {
      var artist = filteredFirst3AndBySameName[i];
      var albumsForFilteredArtist = await this.fetchAlbumsForClarification(artist);
      var commonAlbums = albumsForFilteredArtist.filter(album => albums.find(album2 => album2.album === album.album));
      if (commonAlbums.length > 0) {
        return artist;
      }
    }
    return undefined;
  }

  private async fetch<T>(url: string, searchTimestamp: number, mapper: (element: any, maxCount: number) => T, merger: (artists: T[]) => Array<T>,
    limit: number = Number.MAX_SAFE_INTEGER, weightProvider: (position: number) => number = position => this.MAX_COUNT, addProgress: boolean = true,
    filter: (element: any) => boolean = element => false): Promise<Array<T>> {
    var items: Array<T> = new Array();
    var first = true;
    var i = 1;
    do {
      var json = await this.httpService.httpGet(url);
      if (first) {
        var reference = json.total ? Math.ceil(json.total / json.data.length) : 1;
        if (searchTimestamp && addProgress) this.loadingService.addToReferenceProgress(reference, this.providerSettings.orderId, searchTimestamp);
        first = false;
      }
      if (searchTimestamp && addProgress) this.loadingService.addToCurrentProgress(this.providerSettings.orderId, searchTimestamp);
      json.data.forEach(element => {
        if (limit > 0 && !filter(element)) {
          var item = mapper(element, weightProvider(i));
          items.push(item);
          limit--;
          i++;
        }
      });
      if (limit > 0) {
        url = json.next;
        if (url) {
          await new Promise(resolve => setTimeout(resolve, this.apiDelay));
        }
      }
    } while (limit > 0 && url && this.loadingService.isLoadActive(searchTimestamp));
    if (this.loadingService.isLoadActive(searchTimestamp)) {
      return merger(items);
    }
    else {
      console.log("Deezer search cancelled");
      throw new Error("Search cancelled");
    }
  }

  presentToast(message: string) {
    this.toastCtrl.create({
      message: message,
      duration: 3000,
      position: 'bottom'
    }).then(toast => toast.present());
  }

  private mapBasicArtistInRoot(element: any, maxCount: number): BasicArtist {
    return new BasicArtist(element.name, maxCount, BasicArtist.singleIdToArray(new ProviderId(DeezerServiceProvider.PROVIDER, element.id)));
  }

  private mapBasicArtistInNode(element: any, maxCount: number): BasicArtist {
    return new BasicArtist(element.artist.name, maxCount, BasicArtist.singleIdToArray(new ProviderId(DeezerServiceProvider.PROVIDER, element.artist.id)));;
  }

  private mapFullArtistInRoot(element: any, maxCount: number): FullArtist {
    return new FullArtist(element.name, maxCount, BasicArtist.singleIdToArray(new ProviderId(DeezerServiceProvider.PROVIDER, element.id)),
      element.picture_small, element.picture_big, [], undefined, element.nb_fan, undefined, element.link, undefined, undefined);
  }

  private mapAlbumsInRoot(element: any, maxCount: number): ArtistAlbum {
    return new ArtistAlbum(element.title, element.cover_small, element.id, new Date(Date.parse(element.release_date)));
  }

  private mapSong(element: any, maxCount: number): string {
    return element.title;
  }

  private mapSongId(element: any, maxCount: number): string {
    return element.id;
  }

  private weightFromMaxNumber(maxNumber: number): (position: number) => number {
    return (position: number) => maxNumber - position + 1;
  }

  private weightMultiplier(baseNumer: number, multiplierMax: number): (position: number) => number {
    return (position: number) => baseNumer * (multiplierMax - position + 1);
  }

  private fixedWeight(weight: number): (position: number) => number {
    return (position: number) => weight;
  }

  private ignoreWeight(): (position: number) => number {
    return (position: number) => 0;
  }

  private noOpMerger<T>(artists: Array<T>): Array<T> {
    return artists;
  }

  private filterNonAlbums(element: any): boolean {
    return element.record_type !== 'album' && element.record_type !== 'ep'
  }

  private getUrlForUser(accessToken: string) {
    return this.apiURL + this.getMeURL.replace("{0}", accessToken);
  }

  private getUrlForUserArtists(accessToken: string) {
    return this.apiURL + this.getUserArtistsURL.replace("{0}", accessToken);
  }

  private getUrlForSavedTracks(accessToken: string) {
    return this.apiURL + this.getSavedTracksURL.replace("{0}", accessToken);
  }

  private getUrlForHistory(accessToken: string) {
    return this.apiURL + this.getHistoryURL.replace("{0}", accessToken);
  }

  private getUrlForFlow(accessToken: string) {
    return this.apiURL + this.getFlowURL.replace("{0}", accessToken);
  }

  private getUrlForRecs(accessToken: string) {
    return this.apiURL + this.getRecsURL.replace("{0}", accessToken);
  }

  private getUrlForGlobal() {
    return this.apiURL + this.getGlobalURL;
  }

  private getUrlForPlaylists(accessToken: string) {
    return this.apiURL + this.getPlaylistsURL.replace("{0}", accessToken);
  }

  private getUrlForPlaylistTracks(playlistId: string, accessToken: string) {
    return this.apiURL + this.getPlaylistTracksURL.replace("{0}", playlistId).replace("{1}", accessToken);
  }

  private getUrlForPlaylistCreate(accessToken: string, title: string) {
    return this.apiURL + this.getPlaylistCreateURL.replace("{0}", accessToken).replace("{1}", title);
  }

  private getUrlForPlaylistUpdate(playlistId: string, accessToken: string, title: string, privatePlaylist: boolean, description: string) {
    return this.apiURL + this.getPlaylistUpdateURL.replace("{0}", playlistId).replace("{1}", accessToken)
      .replace("{2}", title).replace("{3}", String(!privatePlaylist)).replace("{4}", description);
  }

  private getUrlForPlaylistAddTracks(playlistId: string, accessToken: string, songs: Array<string>) {
    return this.apiURL + this.getPlaylistAddTracksURL.replace("{0}", playlistId).replace("{1}", accessToken).replace("{2}", songs.join(','));
  }

  private getUrlForSimilarArtists(artistId: string) {
    return this.apiURL + this.getSimilarArtistsURL.replace("{0}", artistId);
  }

  private getUrlForArtistSearch(artistName: string) {
    return this.apiURL + this.getSearchArtistURL.replace("{0}", artistName);
  }

  private getUrlForArtist(artistId: string) {
    return this.apiURL + this.getArtistURL.replace("{0}", artistId);
  }

  private getUrlForAlbums(artistId: string) {
    return this.apiURL + this.getAlbumsURL.replace("{0}", artistId);
  }

  private getUrlForSongs(artistId: string) {
    return this.apiURL + this.getSongsURL.replace("{0}", artistId);
  }

  private getUrlForAlbumTracks(albumId: string) {
    return this.apiURL + this.getAlbumTracksURL.replace("{0}", albumId);
  }

}