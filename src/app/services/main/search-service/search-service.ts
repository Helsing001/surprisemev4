import { Injectable } from '@angular/core';
import { SpotifyServiceProvider } from '../spotify-service/spotify-service';
import { LastFmServiceProvider } from '../last-fm-service/last-fm-service';
import { BasicArtist, ProviderId } from '../../../models/artist/basic-artist';
import { FullArtist } from '../../../models/artist/full-artist';
import { concat as lConcat, reduce, values, orderBy, groupBy, cloneDeep, unionWith } from 'lodash';
import { SearchParams } from '../../../models/params/search-params';
import { ProviderService } from '../../other/provider-service/provider-service';
import { concat } from 'rxjs';
import { LoadingServiceProvider } from '../../other/loading-service/loading-service';
import { ProviderSettings } from '../../../models/providers/provider-settings';
import { ArtistExtraInfo } from '../../../models/artist/artist-extra-info';
import { SimilarArtist } from '../../../models/artist/similar-artist';
import { ProviderOperations } from '../../../models/providers/provider-operations';

/*
  Generated class for the SearchServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers 
  and Angular DI.
*/
@Injectable()
export class SearchServiceProvider {

  private currentState: string;
  private currentEvents: number;
  private currentTotal: number;
  private basePercent: number = 0;

  constructor(public providerService: ProviderService, public loadingService: LoadingServiceProvider) { }

  searchForRec(searchParams: SearchParams) {
    return new Promise((resolve, error) => {
      console.log("Started searching");
      this.loadingService.searchTimestamp = searchParams.genericParams.timestamp;
      if (this.loadingService.isLoadActive(searchParams.genericParams.timestamp)) {
        this.fetchUserArtists(searchParams).then((userArtists: Array<BasicArtist>) => {
          console.log("UserArtists");
          console.log(userArtists);
          if (this.loadingService.isLoadActive(searchParams.genericParams.timestamp)) {
            this.fetchSourceArtists(userArtists, searchParams).then((sourceArtists: Array<BasicArtist>) => {
              console.log("SourceArtists");
              console.log(sourceArtists);
              if (this.loadingService.isLoadActive(searchParams.genericParams.timestamp)) {
                this.mergeUserWithSourceArtists(userArtists, sourceArtists, searchParams).then((userArtists: Array<BasicArtist>) => {
                  console.log("MergedUserSourceArtists");
                  console.log(userArtists);
                  if (this.loadingService.isLoadActive(searchParams.genericParams.timestamp)) {
                    this.fetchSimilarArtists(userArtists, sourceArtists, searchParams).then((recArtists: Array<SimilarArtist>) => {
                      console.log("SimilarArtists");
                      console.log(recArtists);
                      if (this.loadingService.isLoadActive(searchParams.genericParams.timestamp)) {
                        this.fetchSimilarArtistsInfo(recArtists, searchParams).then((recArtists: Array<FullArtist>) => {
                          console.log("ArtistsWithInfo");
                          console.log(recArtists);
                          if (this.loadingService.isLoadActive(searchParams.genericParams.timestamp)) {
                            resolve(recArtists);
                          } else { console.log("Search cancelled"); }
                        }).catch(err => { error(err); })
                      } else { console.log("Search cancelled"); }
                    }).catch(err => { error(err); })
                  } else { console.log("Search cancelled"); }
                }).catch(err => { error(err); })
              } else { console.log("Search cancelled"); }
            }).catch(err => { error(err); })
          } else { console.log("Search cancelled"); }
        }).catch(err => { error(err); })
      } else { console.log("Search cancelled"); }
    });
  }

  searchForName(artistName: string) {
    return new Promise((resolve, error) => {
      var providerSettings = this.providerService.getActiveProviderFor(ProviderOperations.INFO);
      var infoExecutor = this.providerService.getServiceProvider(providerSettings.name).searchArtist(artistName);
      infoExecutor
        .then((recArtists: Array<FullArtist>) => { resolve(recArtists); })
        .catch(err => { error(err); })
    });
  }

  searchArtistInfo(artist: BasicArtist) {
    return new Promise((resolve, error) => {
      var providerSettings = this.providerService.getActiveProviderFor(ProviderOperations.INFO);
      this.providerService.getServiceProvider(providerSettings.name).fetchArtistInfo(artist)
        .then(a => resolve(a))
        .catch(err => { error(err); })
    });
  }

  searchArtistsInfo(artists: Array<BasicArtist>): Promise<Array<FullArtist>> {
    return new Promise((resolve, error) => {
      var providerSettings = this.providerService.getActiveProviderFor(ProviderOperations.INFO);
      var sArtists = artists.map(a => SimilarArtist.fromBasicArtist(a));
      this.providerService.getServiceProvider(providerSettings.name).fetchSimilarArtistsInfo(sArtists, undefined)
        .then(f => resolve(f))
        .catch(err => { error(err); })
    });
  }

  searchArtistExtraInfo(artist: BasicArtist) {
    return new Promise((resolve, error) => {
      var providerSettings = this.providerService.getActiveProviderFor(ProviderOperations.INFO);
      this.providerService.getServiceProvider(providerSettings.name).fetchArtistExtraInfo(artist)
        .then((artistExtraInfo: ArtistExtraInfo) => { resolve(artistExtraInfo); })
        .catch(err => { error(err); })
    });
  }

  searchArtistScrobbles(artist: BasicArtist) {
    return new Promise((resolve, error) => {
      var providerSettings = this.providerService.getActiveProviderFor(ProviderOperations.SCROBBLE);
      this.providerService.getServiceProvider(providerSettings.name).fetchArtistScrobbles(artist)
        .then(scrobbles => { resolve(scrobbles); })
        .catch(err => { error(err); })
    });
  }

  private fetchUserArtists(searchParams: SearchParams) {
    return new Promise((resolve, error) => {
      var userArtists: Array<BasicArtist>;
      var showSources = searchParams.genericParams.showSources;
      var ignoreLibrary = searchParams.genericParams.ignoreLibrary;
      var excludeLibrary = searchParams.genericParams.excludeLibrary;

      var providers = this.providerService.getActiveProvidersFor(ProviderOperations.LIBRARY);
      this.setLoadingPhase('User Library', providers, 0, 25);

      if ((showSources && !excludeLibrary) || (!showSources && ignoreLibrary)) {
        userArtists = new Array();
        resolve(userArtists);
        return;
      }

      const providersCalls = [];
      const providerResults: Array<Array<BasicArtist>> = [];

      providers.forEach(provider => {
        var serviceParams = this.providerService.getServiceParams(searchParams, provider.name);
        providersCalls.push(this.providerService.getServiceProvider(provider.name).fetchUserArtists(searchParams.genericParams));
      });
      concat(...providersCalls).subscribe(
        res => {
          providerResults.push(res as Array<BasicArtist>);
        },
        err => {
          console.log("error receiving provider call: " + err);
          error(err);
        },
        () => {
          providerResults.forEach(providerArtists => {
            if (!userArtists) {
              userArtists = providerArtists;
            }
            else {
              userArtists = BasicArtist.mergeListenedWithAdd2(userArtists, providerArtists);
            }
          });
          resolve(userArtists);
        });
    });
  }

  private fetchSourceArtists(userArtists: Array<BasicArtist>, searchParams: SearchParams) {
    return new Promise((resolve, error) => {
      var sourceArtists: Array<BasicArtist>;

      const providersCalls: Array<Promise<CallResult[]>> = [];
      const providerResults: Array<Array<BasicArtist>> = [];
      const providerIds: Array<number> = [];

      var providers = this.providerService.getAllProvidersFor(ProviderOperations.SOURCE);
      providers.forEach(provider => {
        const currentProviderCalls: Array<Call> = new Array();
        var serviceParamsArray: Array<any> = this.providerService.getServiceParams(searchParams, provider.name);
        serviceParamsArray.forEach(serviceParams => {
          currentProviderCalls.push(new Call(serviceParams, this.providerService.getServiceProvider(provider.name).fetchSourceArtists(serviceParams, searchParams.genericParams)));
          providerIds.push(serviceParams.id);
        });
        if (currentProviderCalls.length > 0) {
          providersCalls.push(this.executeInSequence(0, currentProviderCalls, new Array()));
        }
      });

      this.setLoadingPhaseByIds('Source Artists', providerIds, 25, 25);
      concat(...providersCalls).subscribe(
        res => {
          var callResults = res as Array<CallResult>;
          callResults.forEach(result => {
            if (!searchParams.genericParams.showSources) this.normalizeListened(result.basicArtists, result.searchParams.weight);
            providerResults.push(result.basicArtists);
          })
        },
        err => {
          console.log("error receiving provider call: " + err);
          error(err);
        },
        () => {
          providerResults.forEach(providerArtists => {
            if (!sourceArtists) {
              sourceArtists = providerArtists;
            }
            else {
              sourceArtists = BasicArtist.mergeListenedWithAdd2(sourceArtists, providerArtists);
            }
          });

          if (searchParams.genericParams.showSources && searchParams.genericParams.excludeLibrary) {
            sourceArtists = sourceArtists.filter(sourceArtist => userArtists.findIndex(userArtist => userArtist.artist.toLowerCase() === sourceArtist.artist.toLowerCase()) == -1);
          }

          if (sourceArtists.length > 0) {
            resolve(sourceArtists);
          }
          else {
            error("Source is empty");
          }

        });
    });
  }

  private mergeUserWithSourceArtists(userArtists: Array<BasicArtist>, sourceArtists: Array<BasicArtist>, searchParams: SearchParams) {
    return new Promise((resolve, error) => {
      if (!searchParams.genericParams.showSources) {
        var u = unionWith(userArtists, sourceArtists, (a, b) => a.artist.toLowerCase() === b.artist.toLowerCase());
        resolve(u);
      }
      else {
        resolve(userArtists);
      }
    });
  }

  private fetchSimilarArtists(userArtists: Array<BasicArtist>, sourceArtists: Array<BasicArtist>, searchParams: SearchParams) {
    return new Promise((resolve, error) => {
      var recArtists: Array<SimilarArtist>;
      var resultLimit = searchParams.genericParams.resultLimit;
      var showSources = searchParams.genericParams.showSources;
      var excludeLibrary = searchParams.genericParams.excludeLibrary;

      if (showSources) {
        recArtists = sourceArtists.map(s => new SimilarArtist(s.artist, s.weight, s.id, undefined, undefined));
        if (!resultLimit || resultLimit > recArtists.length) {
          resultLimit = recArtists.length;
        }
        recArtists = recArtists.slice(0, resultLimit);
        resolve(recArtists);
        return;
      }

      var providers = this.providerService.getActiveProvidersFor(ProviderOperations.SIMILARITY);

      this.setLoadingPhase('Similar Artists', providers, 50, 25);

      const providersCalls = [];
      const providersNames = [];
      const providerResults: Array<Array<SimilarArtist>> = [];

      providers.forEach(provider => {
        providersCalls.push(this.providerService.getServiceProvider(provider.name).fetchSimilarArtists(userArtists, sourceArtists, searchParams.genericParams));
        providersNames.push(provider.name);
      });

      concat(...providersCalls).subscribe(
        res => {
          var recArtists = res as Array<SimilarArtist>;
          this.normalizeRec(recArtists, providersNames.shift());
          providerResults.push(recArtists);
        },
        err => {
          console.log("error receiving provider call: " + err);
          error(err);
        },
        () => {
          providerResults.forEach(providerArtists => {
            if (!recArtists) {
              recArtists = providerArtists;
            }
            else {
              recArtists = SimilarArtist.mergeSimilarWithAdd(recArtists, providerArtists);
            }
          });

          if (!resultLimit || resultLimit > recArtists.length) {
            resultLimit = recArtists.length;
          }

          recArtists = recArtists.slice(0, resultLimit);

          resolve(recArtists);

          if (recArtists.length > 0) {
            resolve(recArtists);
          }
          else {
            error("No artist to recommend");
          }

        });
    });
  }

  private fetchSimilarArtistsInfo(recArtists: Array<SimilarArtist>, searchParams: SearchParams) {
    return new Promise((resolve, error) => {
      var simpleSearch = searchParams.genericParams.simpleSearch;

      if (simpleSearch) {
        var fullArtists: Array<FullArtist> = new Array();
        recArtists.forEach(similarArtist => fullArtists.push(FullArtist.fromSimilarArtist(similarArtist)));
        resolve(fullArtists);
        return;
      }

      var provider = this.providerService.getActiveProviderFor(ProviderOperations.INFO);
      this.setLoadingPhase('Artists Info', [provider], 75, 25);
      var infoExecutor = this.providerService.getServiceProvider(provider.name).fetchSimilarArtistsInfo(recArtists, searchParams.genericParams);
      infoExecutor
        .then((recArtists: Array<FullArtist>) => {
          resolve(recArtists);
        })
        .catch(err => { error(err); });
    });
  }

  private executeInSequence(i: number, calls: Array<Call>, callResults: Array<CallResult>): Promise<Array<CallResult>> {
    return new Promise((resolve, error) => {
      calls[i].call
        .then(basicArtists => {
          callResults.push(new CallResult(calls[i].searchParams, basicArtists));
          if (i + 1 < calls.length) {
            this.executeInSequence(i + 1, calls, callResults).then((r) => resolve(r), e => error(e));
          }
          else {
            resolve(callResults);
          }
        })
        .catch(e => error(e));
    })
  }

  private normalizeListened(providerResults: Array<BasicArtist>, maxWeight: number) {
    if (providerResults.length > 0) {
      var maxCount = reduce(providerResults, (a, b) => Number(a.weight) >= Number(b.weight) ? a : b).weight;
      providerResults.forEach(listenedArtist => {
        listenedArtist.weight = listenedArtist.weight * maxWeight / maxCount;
      })
    }
  }

  private normalizeRec(providerResults: Array<SimilarArtist>, currentProvider: string) {
    if (providerResults.length > 0) {
      var maxCount = reduce(providerResults, (a, b) => Number(a.weight) >= Number(b.weight) ? a : b).weight;
      var maxWeightSim = this.providerService.getProviderSettings(currentProvider).getSimilarityWeight();
      var maxWeightSource = this.providerService.getProviderSettings(currentProvider).getSourceWeight();
      providerResults.forEach(recArtist => {
        recArtist.weight = recArtist.weight * maxWeightSim / maxCount;
        this.normalizeListened(recArtist.sourceArtists, maxWeightSource);
      })
    }
  }

  private setLoadingPhase(phase: string, providers: ProviderSettings[], basePercent: number, percentRange: number) {
    var providersString: Array<number> = new Array();
    providers.forEach(provider => providersString.push(provider.orderId));
    this.loadingService.setLoadingPhase(phase, providersString, basePercent, percentRange);
  }

  private setLoadingPhaseByIds(phase: string, providerIds: Array<number>, basePercent: number, percentRange: number) {
    this.loadingService.setLoadingPhase(phase, providerIds, basePercent, percentRange);
  }

}

class Call {
  constructor(public searchParams, public call: Promise<BasicArtist[]>) { }
}

class CallResult {
  constructor(public searchParams, public basicArtists: Array<BasicArtist>) { }
}
