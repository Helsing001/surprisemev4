import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { AlertController } from '@ionic/angular';
import { BasicArtist, ProviderId } from '../../../models/artist/basic-artist';
import { FullArtist } from '../../../models/artist/full-artist'
import { StorageServiceProvider } from '../../../services/other/storage-service/storage-service';
import { LastFmParams, GenericParams } from '../../../models/params/search-params';
import { GenericService } from '../../../models/generic/generic-service';
import { ProviderSettings } from '../../../models/providers/provider-settings';
import { concat, EMPTY, onErrorResumeNext } from 'rxjs';
import { map, last } from 'rxjs/operators';
import { LoadingServiceProvider } from '../../other/loading-service/loading-service';
import { concat as lConcat, reduce, values, orderBy, groupBy, unionBy } from 'lodash';
import { ArtistExtraInfo } from '../../../models/artist/artist-extra-info';
import { SimilarArtist } from '../../../models/artist/similar-artist';
import { TheAudioDbServiceProvider } from '../../other/theaudiodb-service/theuadiodb-service';
import { GenerateParams } from '../../../models/params/generate-params';
import { GenerateResult } from '../../../models/other/generate-result';
import { ProviderOperations } from '../../../models/providers/provider-operations';
import { ListenedTrack } from '../../../models/other/listened-track';
import { ExternalList } from '../../../models/list/external-list';
import { ArtistAlbum } from '../../../models/album/artist-album';
import { BasicDiscoArtist } from '../../../models/artist/basic-disco-artist';


/*
  Generated class for the LastFmServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class LastFmServiceProvider extends GenericService {

  public static PROVIDER = 'Last.fm';
  public static DEFAULT_SETTINGS = new ProviderSettings(2, LastFmServiceProvider.PROVIDER, true, true, true, false, true, true, true, true, true, true);

  private apiKeyURL = '&api_key=4a52587e005bc11343fffa806529a7da';
  private jsonResultURL = '&format=json';
  private autocorrectURL = "&autocorrect=1"
  private periodURLURL = '&period=';
  private artistURL = '&artist=';
  private albumURL = '&album=';
  private trackURL = '&track=';
  private pageURL = "&page=";
  private fromURL = "&from=";
  private toURL = "&to=";
  private tagURL = "&tag=";

  private apiURL = 'http://ws.audioscrobbler.com/2.0/';
  private getTopArtistsURL = '?method=user.gettopartists&limit=1000';
  private getSimilarArtistsURL = '?method=artist.getsimilar'
  private getArtistInfoRUL = "?method=artist.getinfo";
  private getTopAlbumsURL = "?method=artist.gettopalbums";
  private getTopTracksURL = "?method=artist.gettoptracks";
  private getArtistSearchRUL = "?method=artist.search";
  private getArtistCorrectionURL = "?method=artist.getcorrection";
  private getArtistChartURL = "?method=user.getweeklyartistchart";
  private getLovedTracksURL = '?method=user.getlovedtracks&limit=1000';
  private getRecentTracksURL = '?method=user.getrecenttracks';
  private getGlobalChartURL = '?method=chart.gettopartists&limit=1000';
  private getTagChartURL = '?method=tag.gettopartists&limit=1000';
  private getAlbumURL = '?method=album.getinfo';
  private getTrackURL = '?method=track.getinfo';

  private apiDelay = 250;
  private maxDiscoAlbums = 15
  private loggedIn = false;

  private providerSettings: ProviderSettings;
  private activeTimers: Array<number> = new Array();
  private inUse: boolean = false;

  constructor(public http: Http, public loadingService: LoadingServiceProvider, public storageService: StorageServiceProvider,
    public alertCtrl: AlertController, public theAudioDbService: TheAudioDbServiceProvider) {
    super();
    this.storageService.getSettingsCache().then(settingsCache => {
      var lastSettings: ProviderSettings;
      if (!settingsCache || !settingsCache.lastFmProviderSettings) {
        lastSettings = LastFmServiceProvider.DEFAULT_SETTINGS;
      }
      else {
        this.processVersionDifferences(settingsCache.lastFmProviderSettings);
        lastSettings = settingsCache.lastFmProviderSettings;
      }
      if (lastSettings.getUsername()) {
        this.loggedIn = true;
      }
      this.providerSettings = lastSettings;
    })
  }

  private processVersionDifferences(settings: ProviderSettings) {
    if (!settings.shouldLogIn) {
      settings.setShouldLogIn(this.storageService, true);
    }
    if (!settings.source) {
      settings.setSource(this.storageService, true);
    }
    if (!settings.scrobble) {
      settings.setScrobble(this.storageService, true);
    }
    if (!settings.listening) {
      settings.setListening(this.storageService, true);
    }
    if (!settings.discography) {
      settings.setDiscography(this.storageService, true);
    }
  }

  public getOwnName() {
    return LastFmServiceProvider.PROVIDER;
  }

  public auth(): Promise<boolean> {
    return new Promise(resolve => {
      if (this.loggedIn) {
        resolve(true);
        return;
      }
      this.presentUsernamePopup().then((user: string) => {
        if (user) {
          this.loggedIn = true;
          this.providerSettings.setUsername(this.storageService, user);
          resolve(true);
        }
        else {
          resolve(false);
        }
      });
    });
  }

  public forgetAuth() {
    this.providerSettings.setUsername(this.storageService, undefined);
    this.loggedIn = false;
  }

  public isLongAuth(): boolean {
    return false;
  }

  private presentUsernamePopup() {
    return new Promise(resolve => {
      let alert = this.alertCtrl.create({
        header: 'Last.fm username',
        inputs: [
          {
            name: 'username',
            placeholder: 'Username'
          }
        ],
        buttons: [
          {
            text: 'Cancel',
            role: 'cancel',
            handler: data => {
              resolve(undefined);
            }
          },
          {
            text: 'OK',
            handler: data => {
              resolve(data.username);
            }
          }
        ]
      }).then(alert => alert.present());
    });
  }

  fetchUserArtists(genericParams: GenericParams): Promise<Array<BasicArtist>> {
    return new Promise((resolve, error) => {
      var userArtists: Array<BasicArtist> = [];
      this.auth()
        .then((success: boolean) => {
          if (!success) {
            error("User not logged in");
            return;
          }
          this.getTopArtistsPage(userArtists, 1, 'Overall', genericParams.timestamp, this.providerSettings.orderId).then(u => resolve(u)).catch(e => error('An error occurred'));
        })
    });
  }

  fetchSourceArtists(lastFmParams: LastFmParams, genericParams: GenericParams): Promise<Array<BasicArtist>> {
    return new Promise((resolve, error) => {
      var period = lastFmParams.lastfmPeriod;
      var historyLimit = genericParams.historyLimit;
      var resultLimit = genericParams.resultLimit;
      var showSources = genericParams.showSources;
      var sourceArtists: Array<BasicArtist> = [];
      this.auth().then((success) => {
        if (!success) {
          error("User not logged in");
          return;
        }
        this.inUse = true;
        if (period === 'Custom') {
          this.getUserChartArtists(lastFmParams.from, lastFmParams.to, genericParams.timestamp, lastFmParams.id).then(u => resolve(u)).catch(e => error('An error occurred')).finally(() => this.inUse = false);
        }
        else if (period === 'Loved') {
          this.getLovedTracksArtists(sourceArtists, 1, genericParams.timestamp, lastFmParams.id).then(u => resolve(u)).catch(e => error('An error occurred')).finally(() => this.inUse = false);
        }
        else if (period === 'Global') {
          this.getGlobalChartArtists(genericParams.timestamp, lastFmParams.id).then(u => resolve(u)).catch(e => error('An error occurred')).finally(() => this.inUse = false);
        }
        else if (period === 'Tag') {
          var limit = showSources ? resultLimit : historyLimit;
          this.getTagChartArtists(sourceArtists, lastFmParams.tag, limit, 1, genericParams.timestamp, lastFmParams.id).then(u => resolve(u)).catch(e => error('An error occurred')).finally(() => this.inUse = false);
        }
        else {
          this.getTopArtistsPage(sourceArtists, 1, period, genericParams.timestamp, lastFmParams.id).then(u => resolve(u)).catch(e => error('An error occurred')).finally(() => this.inUse = false);
        }
      });
    });
  }

  private getUserChartArtists(from: number, to: number, searchTimestamp: number, loadingId: number): Promise<Array<BasicArtist>> {
    return new Promise((resolve, error) => {
      var userArtists: Array<BasicArtist> = new Array();
      this.loadingService.addToReferenceProgress(1, loadingId, searchTimestamp);
      this.http.get(this.getUrlForTracksWithinPeriod(from, to))
        .pipe(map(res => res.json())).toPromise()
        .then(data => {
          for (var i = 0, len = data.weeklyartistchart.artist.length; i < len; i++) {
            userArtists.push(new BasicArtist(data.weeklyartistchart.artist[i].name, Number(data.weeklyartistchart.artist[i].playcount), BasicArtist.singleIdToArray(new ProviderId(LastFmServiceProvider.PROVIDER, data.weeklyartistchart.artist[i].mbid))));
          }
          this.loadingService.addToCurrentProgress(loadingId, searchTimestamp);
          resolve(userArtists);
        })
        .catch(e => error(e));
    })
  }

  private getLovedTracksArtists(userArtists: Array<BasicArtist>, page: number, searchTimestamp: number, loadingId: number): Promise<Array<BasicArtist>> {
    return new Promise((resolve, error) => {
      this.http.get(this.getUrlForLovedTracks(page))
        .pipe(map(res => res.json())).toPromise()
        .then(data => {
          var currentPage = Number(data.lovedtracks['@attr'].page);
          var totalPages = Number(data.lovedtracks['@attr'].totalPages);
          if (currentPage == 1) {
            this.loadingService.addToReferenceProgress(totalPages, loadingId, searchTimestamp);
          }
          for (var i = 0, len = data.lovedtracks.track.length; i < len; i++) {
            userArtists.push(new BasicArtist(data.lovedtracks.track[i].artist.name, Number(1), BasicArtist.singleIdToArray(new ProviderId(LastFmServiceProvider.PROVIDER, data.lovedtracks.track[i].artist.mbid))));
          }
          this.loadingService.addToCurrentProgress(loadingId, searchTimestamp);
          if (currentPage == totalPages || totalPages == 0) {
            resolve(BasicArtist.mergeListenedWithAdd(userArtists));
          }
          else {
            setTimeout(() => {
              if (this.loadingService.isLoadActive(searchTimestamp)) {
                this.getLovedTracksArtists(userArtists, page + 1, searchTimestamp, loadingId).then(u => resolve(u));
              }
              else {
                console.log("Last.fm search cancelled");
              }
            }, this.apiDelay);
          }
        })
        .catch(e => error(e));
    })
  }

  private getGlobalChartArtists(searchTimestamp: number, loadingId: number): Promise<Array<BasicArtist>> {
    return new Promise((resolve, error) => {
      var userArtists: Array<BasicArtist> = new Array();
      this.loadingService.addToReferenceProgress(1, loadingId, searchTimestamp);
      this.http.get(this.getUrlForGlobalChart())
        .pipe(map(res => res.json())).toPromise()
        .then(data => {
          for (var i = 0, len = data.artists.artist.length; i < len; i++) {
            userArtists.push(new BasicArtist(data.artists.artist[i].name, Number(data.artists.artist.length - i), BasicArtist.singleIdToArray(new ProviderId(LastFmServiceProvider.PROVIDER, data.artists.artist[i].mbid))));
          }
          this.loadingService.addToCurrentProgress(loadingId, searchTimestamp);
          resolve(BasicArtist.mergeListenedWithAdd(userArtists));
        })
        .catch(e => error(e));
    })
  }

  private getTagChartArtists(userArtists: Array<BasicArtist>, tag: string, limit: number, page: number, searchTimestamp: number, loadingId: number): Promise<Array<BasicArtist>> {
    return new Promise((resolve, error) => {
      this.http.get(this.getUrlForTagChart(tag, page))
        .pipe(map(res => res.json())).toPromise()
        .then(data => {
          var currentPage = Number(data.topartists['@attr'].page);
          var totalPages = Number(data.topartists['@attr'].totalPages);
          if (currentPage == 1) {
            if (limit > 0 && data.topartists.artist.length > 0) {
              var reference = Math.ceil(limit / data.topartists.artist.length);
              if (reference > totalPages) reference = totalPages;
              this.loadingService.addToReferenceProgress(reference, loadingId, searchTimestamp);
            }
            else {
              this.loadingService.addToReferenceProgress(1, loadingId, searchTimestamp);
            }
          }
          for (var i = 0, len = data.topartists.artist.length; i < len && i < limit; i++) {
            userArtists.push(new BasicArtist(data.topartists.artist[i].name, Number(data.topartists.artist.length - i), BasicArtist.singleIdToArray(new ProviderId(LastFmServiceProvider.PROVIDER, data.topartists.artist[i].mbid))));
          }
          limit = limit - data.topartists.artist.length;
          this.loadingService.addToCurrentProgress(loadingId, searchTimestamp);
          if (currentPage == totalPages || totalPages == 0 || limit <= 0) {
            resolve(BasicArtist.mergeListenedWithAdd(userArtists));
          }
          else {
            setTimeout(() => {
              if (this.loadingService.isLoadActive(searchTimestamp)) {
                this.getTagChartArtists(userArtists, tag, limit, page + 1, searchTimestamp, loadingId).then(u => resolve(u));
              }
              else {
                console.log("Last.fm search cancelled");
              }
            }, this.apiDelay);
          }
        })
        .catch(e => error(e));
    })
  }

  private getTopArtistsPage(userArtists: Array<BasicArtist>, page: number, period: string, searchTimestamp: number, loadingId: number): Promise<Array<BasicArtist>> {
    return new Promise((resolve, error) => {
      this.http.get(this.getUrlForTopArtistsWithPeriod(period, page))
        .pipe(map(res => res.json())).toPromise()
        .then(data => {
          var currentPage = Number(data.topartists['@attr'].page);
          var totalPages = Number(data.topartists['@attr'].totalPages);
          if (currentPage == 1) {
            this.loadingService.addToReferenceProgress(totalPages, loadingId, searchTimestamp);
          }
          for (var i = 0, len = data.topartists.artist.length; i < len; i++) {
            userArtists.push(new BasicArtist(data.topartists.artist[i].name, Number(data.topartists.artist[i].playcount), BasicArtist.singleIdToArray(new ProviderId(LastFmServiceProvider.PROVIDER, data.topartists.artist[i].mbid))));
          }
          this.loadingService.addToCurrentProgress(loadingId, searchTimestamp);
          if (currentPage == totalPages || totalPages == 0) {
            resolve(userArtists);
          }
          else {
            setTimeout(() => {
              if (this.loadingService.isLoadActive(searchTimestamp)) {
                this.getTopArtistsPage(userArtists, page + 1, period, searchTimestamp, loadingId).then(u => resolve(u));
              }
              else {
                console.log("Last.fm search cancelled");
              }
            }, this.apiDelay);
          }
        })
        .catch(e => error(e));
    })
  }

  private getSimilarArtistsWithDelay(delay, artist, searchTimestamp: number) {
    return new Promise((resolve, error) => {
      var timer = setTimeout(() => {
        if (this.loadingService.isLoadActive(searchTimestamp)) {
          this.http.get(this.getUrlForSimilarArtist(artist)).pipe(map(res => res.json())).toPromise()
            .then(data => {
              resolve(data);
            })
            .catch(err => error(err));
        }
        else {
          console.log("Last.fm search cancelled");
          this.activeTimers.forEach(timer => {
            clearTimeout(timer);
          });
          this.activeTimers = new Array();
        }
      },
        delay);
      this.activeTimers.push(timer);
    });
  }

  fetchSimilarArtists(userArtists: Array<BasicArtist>, sourceArtists: Array<BasicArtist>, genericParams: GenericParams): Promise<Array<SimilarArtist>> {
    return new Promise(resolve => {
      const httpCalls = [];
      const httpResults = [];

      this.inUse = true;
      var historyLimit = genericParams.historyLimit;
      var recArtists: Array<SimilarArtist> = [];
      let i = 0;

      if (!historyLimit || historyLimit > sourceArtists.length) {
        historyLimit = sourceArtists.length;
      }
      this.loadingService.addToReferenceProgress(historyLimit, this.providerSettings.orderId, genericParams.timestamp);

      sourceArtists.slice(0, historyLimit).forEach(recentArtist => {
        httpCalls.push(this.getSimilarArtistsWithDelay(i * this.apiDelay, recentArtist.artist, genericParams.timestamp));
        i++;
      });

      const httpCallsWithCatch = httpCalls.map(s => s.catch(e => { console.log("Error: " + e); return null; }));
      concat(...httpCallsWithCatch).subscribe(
        res => {
          httpResults.push(res);
          this.loadingService.addToCurrentProgress(this.providerSettings.orderId, genericParams.timestamp);
        },
        error => {
          console.log("Error that should never happen: " + error);
        },
        () => {
          this.activeTimers = new Array();
          for (let i = 0; i < httpResults.length; i++) {
            if (httpResults[i] == null || !httpResults[i].hasOwnProperty("similarartists")) { console.log(httpResults[i]); continue; }
            for (let j = 0, len2 = httpResults[i].similarartists.artist.length; j < len2; j++) {
              var currentArtist = httpResults[i].similarartists.artist[j];
              var artistIndex = userArtists.findIndex(elem => elem.artist.toLowerCase() == currentArtist.name.toLowerCase());
              if (artistIndex < 0) {
                var score = sourceArtists[i].weight * currentArtist.match;
                artistIndex = recArtists.findIndex(elem => elem.artist.toLowerCase() == currentArtist.name.toLowerCase());
                if (artistIndex < 0) {
                  var basicArtist = new BasicArtist(currentArtist.name, score, BasicArtist.singleIdToArray(new ProviderId(LastFmServiceProvider.PROVIDER, currentArtist.mbid)));
                  var sourceSimilarArtists: Array<BasicArtist> = [new BasicArtist(sourceArtists[i].artist, score, sourceArtists[i].id)];
                  recArtists.push(new SimilarArtist(basicArtist.artist, basicArtist.weight, basicArtist.id, sourceSimilarArtists, basicArtist.id));
                }
                else {
                  recArtists[artistIndex].weight = recArtists[artistIndex].weight + score;
                  recArtists[artistIndex].insertSourceArtist(sourceArtists[i], score);
                }
              }
            }
          };

          this.inUse = false;
          resolve(SimilarArtist.sortByWeight(recArtists));
        });
    });
  }

  private getArtistInfoWithDelay(delay: number, artist: SimilarArtist, searchTimestamp: number) {
    return new Promise((resolve, error) => {
      var timer = setTimeout(() => {
        if (this.loadingService.isLoadActive(searchTimestamp)) {
          this.http.get(this.getUrlForArtistInfo(artist.artist)).pipe(map(res => res.json())).toPromise()
            .then(res => {
              if (!res.artist) {
                resolve(FullArtist.fromBasicArtist(artist));
              }
              else {
                var description: string = res.artist.bio ? res.artist.bio.summary : undefined;
                if (description) description = description.substring(0, description.indexOf("<a href"));
                var genres = [];
                if (res.artist.tags && res.artist.tags.tag) {
                  if (Array.isArray(res.artist.tags.tag)) {
                    res.artist.tags.tag.forEach(genre => {
                      genres.push(genre.name);
                    });
                  }
                  else {
                    genres.push(res.artist.tags.tag.name);
                  }
                }
                var listeners = res.artist.stats.listeners;
                var playcount = res.artist.stats.playcount;
                var url = res.artist.url;
                var fullArtist = new FullArtist(artist.artist, artist.weight, artist.id,
                  res.artist.image[0]["#text"], res.artist.image[2]["#text"],
                  genres, description, Number(listeners), Number(playcount), url, artist.sourceArtists, artist.providers);
                this.theAudioDbService.replaceImage(fullArtist, fullArtist.getIdForProvider(LastFmServiceProvider.PROVIDER))
                  .then(() => {
                    this.loadingService.addToCurrentProgress(this.providerSettings.orderId, searchTimestamp);
                    resolve(fullArtist);
                  })
                  .catch(err => {
                    this.loadingService.addToCurrentProgress(this.providerSettings.orderId, searchTimestamp);
                    resolve(fullArtist);
                  });
              }
            })
            .catch(err => error(err))
        }
        else {
          console.log("Last.fm search cancelled");
          this.activeTimers.forEach(timer => {
            clearTimeout(timer);
          });
          this.activeTimers = new Array();
        }
      },
        delay);
      this.activeTimers.push(Number(timer));
    });
  }

  public fetchArtistInfo(artist: BasicArtist): Promise<FullArtist> {
    return new Promise(resolve => {
      var similarArtist: SimilarArtist = new SimilarArtist(artist.artist, artist.weight, artist.id, undefined, undefined);
      var recArtists: Array<SimilarArtist> = [similarArtist];
      this.fetchSimilarArtistsInfo(recArtists, undefined).then(f => resolve(f.pop()));
    });
  }

  public fetchSimilarArtistsInfo(recArtists: Array<SimilarArtist>, genericParams: GenericParams): Promise<Array<FullArtist>> {
    return new Promise(resolve => {
      this.inUse = true;
      var searchTimestamp: number = undefined; if (genericParams) searchTimestamp = genericParams.timestamp;
      var artistsWithInfo: Array<FullArtist> = new Array();
      const httpCallsForArtistInfo = [];
      let i = 0;

      this.loadingService.addToReferenceProgress(recArtists.length, this.providerSettings.orderId, searchTimestamp);
      recArtists.forEach(recArtist => {
        httpCallsForArtistInfo.push(this.getArtistInfoWithDelay(i * this.apiDelay, recArtist, searchTimestamp));
        i++;
      });

      const httpCallsWithCatch = httpCallsForArtistInfo.map(s => s.catch(e => {
        console.log("Error: " + e);
        return FullArtist.fromBasicArtist(recArtists[artistsWithInfo.length]);
      }));
      concat(...httpCallsWithCatch).subscribe(
        (fullArtist: FullArtist) => {
          artistsWithInfo.push(fullArtist);
        },
        error => {
          console.log("Error that should never happen: " + error);
        },
        () => {
          this.activeTimers = new Array();
          this.inUse = false;
          resolve(artistsWithInfo);
        });
    });
  }

  public searchArtist(artistName: string): Promise<FullArtist[]> {
    return new Promise(resolve => {
      var searchResults: Array<FullArtist> = new Array();
      this.http.get(this.getUrlForArtistCorrection(artistName)).pipe(map(res => res.json())).toPromise()
        .then(data => {
          if (data.corrections.correction && data.corrections.correction.artist && data.corrections.correction.artist.name) {
            var correctName = data.corrections.correction.artist.name;
            if (artistName.toLowerCase() !== correctName.toLowerCase()) {
              var correctBasicArtist = new BasicArtist(correctName, this.providerSettings.getSourceWeight(), BasicArtist.singleIdToArray(new ProviderId(LastFmServiceProvider.PROVIDER, data.corrections.correction.artist.mbid)));
              this.fetchArtistInfo(correctBasicArtist)
                .then(correctFullArtist => searchResults.push(correctFullArtist))
                .finally(() => this.getSearchResults(artistName, searchResults).then(s => { resolve(s); console.log(s); }));
            }
            else {
              this.getSearchResults(artistName, searchResults).then(s => resolve(s));
            }
          }
          else {
            this.getSearchResults(artistName, searchResults).then(s => resolve(s));
          }
        });
    });
  }

  private getSearchResults(artistName: string, searchResults: Array<FullArtist>): Promise<FullArtist[]> {
    return new Promise((resolve, error) => {
      this.http.get(this.getUrlForArtistSearch(artistName)).pipe(map(res => res.json())).toPromise()
        .then(data => {
          if (data && data.results && data.results.artistmatches && data.results.artistmatches.artist) {
            data.results.artistmatches.artist.forEach((artist, index) => {
              if (index <= 10 && searchResults.find(a => a.artist === artist.name) === undefined) {
                var searchResult = new FullArtist(artist.name, this.providerSettings.getSourceWeight(), BasicArtist.singleIdToArray(new ProviderId(LastFmServiceProvider.PROVIDER, artist.mbid)),
                  artist.image[0]["#text"], artist.image[2]["#text"], [], undefined, Number(artist.listeners), undefined, artist.url, undefined, undefined);
                this.theAudioDbService.replaceImage(searchResult, artist.mbid, index).catch(err => console.log(err));
                searchResults.push(searchResult);
              }
            })
          }
          resolve(searchResults);
        })
        .catch(error => {
          console.log(error);
          resolve(searchResults);
        });
    })
  }

  fetchArtistExtraInfo(basicArtist: BasicArtist): Promise<ArtistExtraInfo> {
    return new Promise((resolve, error) => {
      var similarArtists: Array<FullArtist> = new Array();
      var topAlbums: Array<ArtistAlbum> = new Array();
      var topSongs: Array<string> = new Array();
      this.http.get(this.getUrlForSimilarArtist(basicArtist.artist)).pipe(map(res => res.json())).toPromise()
        .then(data => {
          for (let j = 0, len2 = data.similarartists.artist.length; j < len2 && j < 5; j++) {
            var currentArtist = data.similarartists.artist[j];
            var similarArtist = new FullArtist(currentArtist.name, this.providerSettings.getSourceWeight(), BasicArtist.singleIdToArray(new ProviderId(LastFmServiceProvider.PROVIDER, currentArtist.mbid)),
              currentArtist.image[0]["#text"], currentArtist.image[2]["#text"], undefined, undefined, undefined, undefined, undefined, undefined, undefined);
            similarArtists.push(similarArtist);
            this.theAudioDbService.replaceImage(similarArtist, currentArtist.mbid, j).catch(err => console.log(err));
          }
          this.http.get(this.getUrlForTopAlbums(basicArtist.artist)).pipe(map(res => res.json())).toPromise()
            .then(data => {
              for (let j = 0, len2 = data.topalbums.album.length; j < len2 && j < 5; j++) {
                var currentAlbum = data.topalbums.album[j];
                if (currentAlbum.name === "(null)") continue;
                topAlbums.push(new ArtistAlbum(currentAlbum.name, currentAlbum.image[2]["#text"]));
              }
              this.http.get(this.getUrlForTopTracks(basicArtist.artist)).pipe(map(res => res.json())).toPromise()
                .then(data => {
                  for (let j = 0, len2 = data.toptracks.track.length; j < len2 && j < 5; j++) {
                    var currentTrack = data.toptracks.track[j];
                    topSongs.push(currentTrack.name);
                  }
                  var artistExtraInfo: ArtistExtraInfo = new ArtistExtraInfo(similarArtists, topAlbums, topSongs);
                  resolve(artistExtraInfo);
                })
                .catch(err => { console.log(err); error(err); });
            })
            .catch(err => { console.log(err); error(err); });

        })
        .catch(err => { console.log(err); error(err); });
    });
  }

  fetchAlbumsForClarification(basicArtist: BasicArtist): Promise<ArtistAlbum[]> {
    return new Promise((resolve, error) => {
      var revertInUse = false;
      if (!this.inUse) {
        this.inUse = revertInUse = true;
      }
      var topAlbums: Array<ArtistAlbum> = new Array();
      this.http.get(this.getUrlForTopAlbums(basicArtist.artist)).pipe(map(res => res.json())).toPromise()
        .then(data => {
          let maxIt = 5;
          for (let j = 0, len2 = data.topalbums.album.length; j < len2 && j < maxIt; j++) {
            var currentAlbum = data.topalbums.album[j];
            if (currentAlbum.name === "(null)") {
              maxIt++;
              continue;
            }
            topAlbums.push(new ArtistAlbum(currentAlbum.name, currentAlbum.image[2]["#text"]));
          }

          if (!this.loggedIn) {
            resolve(topAlbums);
            return;
          }

          const httpCalls = [];
          const httpResults = [];
          var finalAlbums: Array<ArtistAlbum> = new Array();
          var delay = this.inUse && !revertInUse ? this.apiDelay * 4 : this.apiDelay;
          topAlbums.forEach((album, index) => {
            httpCalls.push(this.getAlbumListens(basicArtist, album, (index + 1) * delay));
          });

          const httpCallsWithCatch = httpCalls.map(s => s.catch(e => { console.log(e); return "0"; }));
          concat(...httpCallsWithCatch).subscribe(
            res => {
              httpResults.push(res);
            },
            error => {
              console.log("Error that should never happen: " + error);
            },
            () => {
              for (let i = 0; i < httpResults.length; i++) {
                if (httpResults[i] > 0) finalAlbums.push(topAlbums[i]);
              }
              if (finalAlbums.length == 0) { finalAlbums = topAlbums; }
              if (revertInUse) this.inUse = false;
              resolve(finalAlbums);
            });

        })
        .catch(e => error(e));
    })
  }

  private getAlbumListens(basicArtist: BasicArtist, album: ArtistAlbum, timeout: number) {
    return new Promise((resolve, error) => {
      setTimeout(() => {
        this.http.get(this.getUrlForAlbumInfo(basicArtist.artist, album.album, true)).pipe(map(res => res.json())).toPromise()
          .then(data => {
            if (data.album && data.album.userplaycount) {
              resolve(data.album.userplaycount as number);
            }
            else {
              resolve(0);
            }
          })
          .catch(e => error(e));
      }
        , timeout);
    })
  }

  fetchArtistScrobbles(basicArtist: BasicArtist): Promise<number> {
    return new Promise((resolve, error) => {
      this.auth()
        .then((success: boolean) => {
          if (!success) {
            error("User not logged in, scrobbles unavailable");
            return;
          }
          this.http.get(this.getUrlForArtistInfo(basicArtist.artist, true))
            .pipe(map(res => res.json())).toPromise()
            .then(data => {
              if (data.artist && data.artist.stats && data.artist.stats.userplaycount) {
                resolve(data.artist.stats.userplaycount as number);
              }
              else {
                resolve(0);
              }
            })
            .catch(e => {
              console.log(e);
              error(e);
            });
        });
    })
  }

  fetchTrackScrobbles(basicArtist: BasicArtist, track: string): Promise<number> {
    return new Promise((resolve, error) => {
      this.http.get(this.getUrlForTrackInfo(basicArtist.artist, track))
        .pipe(map(res => res.json())).toPromise()
        .then(data => {
          var playcount: number = 0;
          if (Number(data.track.userplaycount)) playcount = Number(data.track.userplaycount);
          resolve(playcount);
        })
        .catch(e => {
          console.log(e);
          error(e);
        });
    });
  }

  fetchListening(): Promise<ListenedTrack> {
    return new Promise((resolve, error) => {
      this.auth()
        .then((success: boolean) => {
          if (!success) {
            error("User not logged in, scrobbles unavailable");
            return;
          }
        })
      this.http.get(this.getUrlForRecentTracks())
        .pipe(map(res => res.json())).toPromise()
        .then(data => {
          if (data.recenttracks && data.recenttracks.track && data.recenttracks.track[0]) {
            var track = data.recenttracks.track[0];
            var nowPlaying = false;
            if (track['@attr'] && track['@attr'].nowplaying) {
              nowPlaying = true;
            }
            var basicArtist = new BasicArtist(track.artist['#text'], this.providerSettings.getSourceWeight(), BasicArtist.singleIdToArray(new ProviderId(LastFmServiceProvider.PROVIDER, track.artist.mbid)));
            setTimeout(() => {
              this.http.get(this.getUrlForTrackInfo(basicArtist.artist, track.name))
                .pipe(map(res => res.json())).toPromise()
                .then(data => {
                  var playcount: number = 0;
                  var image = undefined;
                  var duration = undefined;
                  var loved = false;
                  if (Number(data.track.userplaycount)) playcount = Number(data.track.userplaycount);
                  if (data.track.album && data.track.album.image[2]) image = data.track.album.image[2]["#text"];
                  if (Number(data.track.duration)) duration = Number(data.track.duration) / 1000;
                  if(data.track.userloved == 1) loved = true;
                  var listenedTrack = new ListenedTrack(basicArtist, track.album['#text'], track.name, duration, nowPlaying, playcount, image, loved);
                  resolve(listenedTrack);
                });
            }, this.apiDelay);
          }
          else {
            resolve(undefined);
          }
        })
        .catch(e => {
          console.log(e);
          error(e);
        });
    });
  }

  getProviderImage(enabled: boolean): string {
    if (enabled) return './assets/images/last-fm.png'; else return './assets/images/last-fm-greyed.png';
  }

  getProviderSettings(): ProviderSettings {
    if (this.providerSettings) {
      return this.providerSettings;
    }
    else {
      return LastFmServiceProvider.DEFAULT_SETTINGS;
    }
  }

  getDefaultOperationsWithoutAuth(): string[] {
    var operationsNoAuth: Array<string> = new Array();
    operationsNoAuth.push(ProviderOperations.SIMILARITY);
    operationsNoAuth.push(ProviderOperations.INFO);
    operationsNoAuth.push(ProviderOperations.DISCOGRAPHY);
    return operationsNoAuth;
  }

  getApiDelay(): number {
    return this.apiDelay;
  }

  fetchPrivateLists(): Promise<ExternalList[]> {
    throw new Error("Method not implemented.");
  }

  generatePlaylist(generateParams: GenerateParams): Promise<GenerateResult> {
    throw new Error("Method not implemented.");
  }

  getAllArtistAlbums(discoArtist: BasicDiscoArtist): Promise<ArtistAlbum[]> {
    return new Promise((resolve, error) => {
      var albumNames: Array<string> = new Array()
      this.http.get(this.getUrlForTopAlbums(discoArtist.artist.artist)).pipe(map(res => res.json())).toPromise()
        .then(data => {
          for (let j = 0, len2 = data.topalbums.album.length; j < len2 && j < this.maxDiscoAlbums; j++) {
            var currentAlbum = data.topalbums.album[j];
            if (currentAlbum.name === "(null)") continue;
            albumNames.push(currentAlbum.name)
          }
          albumNames = albumNames.filter(album =>
            !albumNames.find(album2 => album2.toLowerCase() === album.toLowerCase().replace("(deluxe)", "").trim() && album !== album2))
          this.storageService.getSettingsCache().then(settingsCache => {
            albumNames = albumNames.filter(album => !settingsCache.discographyFilters.find(filter => new RegExp(filter.toLowerCase()).test(album.toLowerCase())))
            this.getAlbumsInfo(discoArtist.artist.artist, albumNames)
              .then(r => resolve(r))
              .catch(e => {
                console.log(e)
                error(e)
              })
          })
        })
    })
  }

  private async getAlbumsInfo(artist: string, albums: Array<string>): Promise<ArtistAlbum[]> {
    let artistAlbums: Array<ArtistAlbum> = new Array()
    for (let i = 0; i < albums.length; i++) {
      var albumInfo = await this.http.get(this.getUrlForAlbumInfo(artist, albums[i], false)).pipe(map(res => res.json())).toPromise()
      var releaseDate = albumInfo.album.wiki ? new Date(Date.parse(albumInfo.album.wiki.published)) : undefined
      if (releaseDate) artistAlbums.push(new ArtistAlbum(albums[i], albumInfo.album.image[2]["#text"], albums[i], releaseDate))
      if (i < albums.length - 1) {
        await new Promise(resolve => setTimeout(resolve, this.apiDelay));
      }
    }
    return orderBy(artistAlbums, 'releaseDate', 'desc');
  }

  private getUrlForUser() {
    return '&user=' + this.providerSettings.getUsername();
  }

  private getUrlForTopArtistsWithPeriod(period: String, page: number) {
    var lastPeriod: string;
    switch (period) {
      case 'Overall': lastPeriod = 'overall'; break;
      case '7 days': lastPeriod = '7day'; break;
      case '1 month': lastPeriod = '1month'; break;
      case '3 months': lastPeriod = '3month'; break;
      case '6 months': lastPeriod = '6month'; break;
      case '1 year': lastPeriod = '12month'; break;
    }
    return this.apiURL + this.getTopArtistsURL + this.getUrlForUser() + this.apiKeyURL + this.jsonResultURL + this.periodURLURL + lastPeriod + this.pageURL + page;
  }

  private getUrlForSimilarArtist(artist: string) {
    artist = encodeURIComponent(artist);
    return this.apiURL + this.getSimilarArtistsURL + this.artistURL + artist + this.apiKeyURL + this.jsonResultURL;
  }

  private getUrlForArtistInfo(artist: string, scrobbles: boolean = false) {
    artist = encodeURIComponent(artist);
    var url = this.apiURL + this.getArtistInfoRUL + this.artistURL + artist + this.apiKeyURL + this.jsonResultURL + this.autocorrectURL;
    if (scrobbles) {
      url = url + this.getUrlForUser();
    }
    return url;
  }

  private getUrlForArtistCorrection(artist: string) {
    artist = encodeURIComponent(artist);
    return this.apiURL + this.getArtistCorrectionURL + this.artistURL + artist + this.apiKeyURL + this.jsonResultURL;
  }

  private getUrlForTopAlbums(artist: string) {
    artist = encodeURIComponent(artist);
    return this.apiURL + this.getTopAlbumsURL + this.artistURL + artist + this.apiKeyURL + this.jsonResultURL;
  }

  private getUrlForTopTracks(artist: string) {
    artist = encodeURIComponent(artist);
    return this.apiURL + this.getTopTracksURL + this.artistURL + artist + this.apiKeyURL + this.jsonResultURL;
  }

  private getUrlForArtistSearch(artist: string) {
    artist = encodeURIComponent(artist);
    return this.apiURL + this.getArtistSearchRUL + this.artistURL + artist + this.apiKeyURL + this.jsonResultURL;
  }

  private getUrlForTracksWithinPeriod(from: number, to: number) {
    return this.apiURL + this.getArtistChartURL + this.getUrlForUser() + this.apiKeyURL + this.jsonResultURL + this.fromURL + from + this.toURL + to;
  }

  private getUrlForLovedTracks(page: number) {
    return this.apiURL + this.getLovedTracksURL + this.getUrlForUser() + this.apiKeyURL + this.jsonResultURL + this.pageURL + page;
  }

  private getUrlForRecentTracks() {
    return this.apiURL + this.getRecentTracksURL + this.getUrlForUser() + this.apiKeyURL + this.jsonResultURL;
  }

  private getUrlForGlobalChart() {
    return this.apiURL + this.getGlobalChartURL + this.getUrlForUser() + this.apiKeyURL + this.jsonResultURL;
  }

  private getUrlForTagChart(tag: string, page: number) {
    tag = encodeURIComponent(tag);
    return this.apiURL + this.getTagChartURL + this.getUrlForUser() + this.apiKeyURL + this.jsonResultURL + this.tagURL + tag + this.pageURL + page;
  }

  private getUrlForAlbumInfo(artist: string, album: string, getPlaycount: boolean) {
    artist = encodeURIComponent(artist);
    album = encodeURIComponent(album);
    if (getPlaycount) return this.apiURL + this.getAlbumURL + this.getUrlForUser() + this.artistURL + artist + this.albumURL + album + this.apiKeyURL + this.jsonResultURL;
    else return this.apiURL + this.getAlbumURL + this.artistURL + artist + this.albumURL + album + this.apiKeyURL + this.jsonResultURL;
  }

  private getUrlForTrackInfo(artist: string, track: string) {
    artist = encodeURIComponent(artist);
    track = encodeURIComponent(track);
    return this.apiURL + this.getTrackURL + this.getUrlForUser() + this.artistURL + artist + this.trackURL + track + this.apiKeyURL + this.jsonResultURL;
  }

}
