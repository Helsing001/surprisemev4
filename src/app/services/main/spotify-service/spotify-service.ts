import { Injectable } from '@angular/core';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser/ngx';
import { StorageServiceProvider, SettingsCache } from '../../other/storage-service/storage-service';
import { BasicArtist, ProviderId } from '../../../models/artist/basic-artist';
import { FullArtist } from '../../../models/artist/full-artist';
import SpotifyWebApi from 'spotify-web-api-js';
import { concat as lConcat, reduce, values, orderBy, groupBy, unionBy } from 'lodash';
import { SpotifyParams, GenericParams } from '../../../models/params/search-params';
import { GenericService } from '../../../models/generic/generic-service';
import { ProviderSettings } from '../../../models/providers/provider-settings';
import { concat } from 'rxjs';
import { LoadingServiceProvider } from '../../other/loading-service/loading-service';
import { ExternalList } from '../../../models/list/external-list';
import { ArtistExtraInfo } from '../../../models/artist/artist-extra-info';
import { SimilarArtist } from '../../../models/artist/similar-artist';
import { AllServices } from '../../../models/other/all-services';
import { GenerateParams } from '../../../models/params/generate-params';
import { GenerateResult } from '../../../models/other/generate-result';
import { ListenedTrack } from '../../../models/other/listened-track';
import { map } from 'rxjs/operators';
import { UtilsServiceProvider } from '../../other/utils-service/utils-service';
import { ArtistAlbum } from '../../../models/album/artist-album';
import { BasicDiscoArtist } from '../../../models/artist/basic-disco-artist';


/*
  Generated class for the SpotifyServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/

declare var cordova: any;

@Injectable()
export class SpotifyServiceProvider extends GenericService {

  public static PROVIDER = 'Spotify';
  public static DEFAULT_SETTINGS = new ProviderSettings(3, SpotifyServiceProvider.PROVIDER, true, true, true, true, false, true, true, false, true, true);

  private MAX_COUNT = 450;
  private SAME_ALBUM_REGEX = [/\(deluxe.*/g, /\(edited.*/g, /\(.*\[edited\]\)/g, /: reloaded/g, /\(explicit\)/g, /\(.* version.*/g,
    /\(.* edition\)/g, /\(extended\)/g, /\(live.*/g, /: remixed/g, /: the remixes/g]

  private spotifyApi: SpotifyWebApi.SpotifyWebApiJs;

  private apiDelay = 200;
  private loggedIn = false;
  private expireTime: Date;
  private authResolve: any;
  private userProfile: SpotifyApi.CurrentUsersProfileResponse = undefined;

  private providerSettings: ProviderSettings;
  private activeTimers: Array<number> = new Array();

  constructor(private iab: InAppBrowser, public storageService: StorageServiceProvider, public loadingService: LoadingServiceProvider,
    public utilsService: UtilsServiceProvider) {
    super();

    this.spotifyApi = new SpotifyWebApi();

    this.storageService.getSettingsCache().then(settingsCache => {
      var spotifySettings: ProviderSettings;
      if (!settingsCache || !settingsCache.spotifyProviderSettings) {
        spotifySettings = SpotifyServiceProvider.DEFAULT_SETTINGS;
      }
      else {
        this.processVersionDifferences(settingsCache.spotifyProviderSettings);
        spotifySettings = settingsCache.spotifyProviderSettings;
      }
      this.providerSettings = spotifySettings;
      if (spotifySettings.getUsername()) {
        this.storageService.getSettingsCache().then(settingsCache => {
          if (settingsCache && settingsCache.enabledProviders && settingsCache.enabledProviders.indexOf("Spotify") != -1) {
            this.auth();
          }
        })
      }
    })
  }

  private processVersionDifferences(settings: ProviderSettings) {
    if (!settings.generate) {
      settings.setGenerate(this.storageService, true);
    }
    if (!settings.shouldLogIn) {
      settings.setShouldLogIn(this.storageService, true);
    }
    if (!settings.source) {
      settings.setSource(this.storageService, true);
    }
    if (!settings.listening) {
      settings.setListening(this.storageService, true);
    }
    if (!settings.discography) {
      settings.setDiscography(this.storageService, true);
    }
  }

  public getOwnName() {
    return SpotifyServiceProvider.PROVIDER;
  }

  public auth(): Promise<boolean> {
    return new Promise(resolve => {
      var expiredAuth = true;
      if (this.expireTime) {
        var expiredAuth = this.expireTime.getTime() <= new Date().getTime() - 1;
      }
      if (this.loggedIn && !expiredAuth) {
        resolve(true);
        return;
      }
      const config = {
        clientId: "d6a51958940547a4af2165563e479672",
        redirectUrl: "surpriseme://callback",
        scopes: ["user-read-private", "user-follow-read", "user-top-read", "user-library-read", "user-read-currently-playing", "user-read-playback-state",
          "user-read-recently-played", "playlist-modify-private", "playlist-read-private", "playlist-modify-public"],
        tokenExchangeUrl: "https://surprisemeheroku.herokuapp.com/exchange",
        tokenRefreshUrl: "https://surprisemeheroku.herokuapp.com/refresh",
        state: Math.floor(Math.random() * 1024) + 1
      };
      this.authResolve = resolve;
      let options: InAppBrowserOptions = {
        location: 'no',
        hidden: 'no'
      };
      let target = "_system";
      const browser = this.iab.create(
        "https://accounts.spotify.com/authorize?client_id=" +
        encodeURIComponent(config.clientId) +
        "&response_type=token&redirect_uri=" +
        encodeURIComponent(config.redirectUrl) +
        "&scope=" +
        encodeURIComponent(config.scopes.join(' ')) +
        "&state=" +
        config.state,
        target,
        options
      );
    });
  }

  public handleAuthResponse(accessToken: string, expiresIn: number) {
    if (this.authResolve) {
      console.log("Auth Spotify success");
      this.spotifyApi.setAccessToken(accessToken);
      var tmpTime = new Date();
      tmpTime.setSeconds(tmpTime.getSeconds() + expiresIn);
      this.expireTime = tmpTime;
      this.getMe()
        .then((userProfile: SpotifyApi.CurrentUsersProfileResponse) => {
          this.userProfile = userProfile;
          var savedName = userProfile.display_name ? userProfile.display_name : userProfile.id;
          if (this.providerSettings.getUsername() !== savedName) {
            this.providerSettings.setUsername(this.storageService, savedName);
          }
        })
        .finally(() => {
          this.loggedIn = true;
          this.authResolve(true);
          this.authResolve = undefined;
        })
    }
    else {
      console.log("Received Spotify auth response, but no auth resolve is saved!");
    }
  }

  public forgetAuth() {
    cordova.plugins.spotifyAuth.forget();
    this.loggedIn = false;
    this.userProfile = undefined;
    this.providerSettings.setUsername(this.storageService, undefined);
  }

  public isLongAuth(): boolean {
    return true;
  }

  getMe() {
    return new Promise((resolve, error) => {
      this.spotifyApi.getMe()
        .then(userProfile => {
          resolve(userProfile);
        })
        .catch(err => error(err));
    });
  }

  fetchUserArtists(genericParams: GenericParams): Promise<Array<BasicArtist>> {
    return new Promise((resolve, error) => {
      this.loadingService.addToReferenceProgress(1, this.providerSettings.orderId, genericParams.timestamp);
      var userArtists: Array<BasicArtist>;
      this.auth().then((success) => {
        if (!success) {
          error("User not logged in");
          return;
        }
        this.fetchSourceArtistsByName("Followed Artists", genericParams.timestamp)
          .then((artistItemsReceived: Array<BasicArtist>) => {
            userArtists = artistItemsReceived;
            this.loadingService.addToCurrentProgress(this.providerSettings.orderId, genericParams.timestamp);
            resolve(userArtists);
          })
          .catch(e => error('An error occurred'));
      });
    });
  }

  fetchSourceArtists(spotifyParams: SpotifyParams, genericParams: GenericParams): Promise<Array<BasicArtist>> {
    return new Promise((resolve, error) => {
      this.loadingService.addToReferenceProgress(1, spotifyParams.id, genericParams.timestamp);
      this.auth().then((success) => {
        if (!success) {
          error("User not logged in");
          return;
        }
        var source = spotifyParams.spotifySource;
        if (source === 'Followed List') {
          this.fetchListArtists(new Array<BasicArtist>(), spotifyParams.playlist.id, 0, genericParams.timestamp)
            .then((sourceArtists: Array<BasicArtist>) => {
              this.loadingService.addToCurrentProgress(spotifyParams.id, genericParams.timestamp);
              resolve(sourceArtists);
            })
            .catch(e => error('An error occurred'));
        }
        else {
          this.fetchSourceArtistsByName(source, genericParams.timestamp)
            .then(result => {
              this.loadingService.addToCurrentProgress(spotifyParams.id, genericParams.timestamp);
              resolve(result);
            })
            .catch(e => error('An error occurred'));
        }
      });
    });
  }

  fetchSourceArtistsByName(source: string, searchTimestamp: number): Promise<Array<BasicArtist>> {
    return new Promise((resolve, error) => {
      var provider = SpotifyServiceProvider.PROVIDER;
      switch (source) {
        case "Followed Artists":
          //de folosit data.artists.total pt loading
          this.fetchFollowedArtists(0, [], searchTimestamp)
            .then((artistItemsReceived: Array<any>) => {
              var sourceArtists: Array<BasicArtist> = [];
              var maxCount = this.MAX_COUNT;
              var artistItems = artistItemsReceived.forEach(function (artist, index: any) {
                sourceArtists.push(new BasicArtist(artist.name, maxCount, BasicArtist.singleIdToArray(new ProviderId(provider, artist.id))));
              });
              resolve(sourceArtists);
            })
            .catch(e => error('An error occurred'));
          break;
        case "Saved Tracks":
          this.fetchSavedTracks(0, [], searchTimestamp)
            .then((artistItemsReceived: Array<any>) => {
              var sourceArtists: Array<BasicArtist> = [];
              var maxCount = this.MAX_COUNT;
              var trackItems = artistItemsReceived.forEach(function (trackObject, index: any) {
                var track = trackObject.track;
                sourceArtists.push(new BasicArtist(track.artists[0].name, maxCount, BasicArtist.singleIdToArray(new ProviderId(provider, track.artists[0].id))));
              });
              resolve(sourceArtists); return;
            })
            .catch(e => error('An error occurred'));
          break;
        case "4 weeks":
          this.fetchTopArtists("short_term")
            .then((artistItemsReceived: Array<any>) => {
              var sourceArtists: Array<BasicArtist> = [];
              var max = artistItemsReceived.length;
              artistItemsReceived.forEach(function (artist, index: any) {
                sourceArtists.push(new BasicArtist(artist.name, max - index, BasicArtist.singleIdToArray(new ProviderId(provider, artist.id))));
              });
              resolve(sourceArtists); return;
            })
            .catch(e => error('An error occurred'));
          break;
        case "6 months":
          this.fetchTopArtists("medium_term")
            .then((artistItemsReceived: Array<any>) => {
              var sourceArtists: Array<BasicArtist> = [];
              var max = artistItemsReceived.length;
              artistItemsReceived.forEach(function (artist, index: any) {
                sourceArtists.push(new BasicArtist(artist.name, max - index, BasicArtist.singleIdToArray(new ProviderId(provider, artist.id))));
              });
              resolve(sourceArtists); return;
            })
            .catch(e => error('An error occurred'));
          break;
        case "Overall":
          this.fetchTopArtists("long_term")
            .then((artistItemsReceived: Array<any>) => {
              var sourceArtists: Array<BasicArtist> = [];
              var max = artistItemsReceived.length;
              artistItemsReceived.forEach(function (artist, index: any) {
                sourceArtists.push(new BasicArtist(artist.name, max - index, BasicArtist.singleIdToArray(new ProviderId(provider, artist.id))));
              });
              resolve(sourceArtists); return;
            })
            .catch(e => error('An error occurred'));
          break;
        default: console.log("spotify source provider unknown " + source); break;
      }
    });
  }

  fetchSimilarArtists(userArtists: Array<BasicArtist>, sourceArtists: Array<BasicArtist>, genericParams: GenericParams): Promise<Array<SimilarArtist>> {
    return new Promise((resolve, error) => {

      this.auth().then((success) => {
        if (!success) {
          error("User not logged in");
          return;
        }

        const apiCalls = [];
        const spotifyResults = [];

        var historyLimit = genericParams.historyLimit;
        var recArtists: Array<SimilarArtist> = [];
        let i = 0, j = 0;

        if (!historyLimit || historyLimit > sourceArtists.length) {
          historyLimit = sourceArtists.length;
        }
        this.loadingService.addToReferenceProgress(historyLimit, this.providerSettings.orderId, genericParams.timestamp);

        sourceArtists.slice(0, historyLimit).forEach(sourceArtist => {
          if (sourceArtist.getIdForProvider(SpotifyServiceProvider.PROVIDER)) {
            apiCalls.push(this.getRelatedArtistsWithDelay(i * this.apiDelay, sourceArtist.getIdForProvider(SpotifyServiceProvider.PROVIDER), genericParams.timestamp));
            i++; j++;
          }
          else {
            apiCalls.push(this.getRelatedArtistsWithDelayByName(i * this.apiDelay, this.apiDelay, sourceArtist, genericParams.timestamp));
            i = i + 2; j++;
          }
        });

        let receivedEvents = 0;
        const apiCallsWithCatch = apiCalls.map(s => s.catch(e => { console.log("Error: " + e); return null; }));
        concat(...apiCallsWithCatch).subscribe(
          res => {
            spotifyResults.push(res);
            this.loadingService.addToCurrentProgress(this.providerSettings.orderId, genericParams.timestamp);
            receivedEvents++;
          },
          error => {
            console.log("Error that should never happen: " + error);
          },
          () => {
            this.activeTimers = new Array();
            for (let i = 0; i < spotifyResults.length; i++) {
              if (spotifyResults[i] == null) { continue; }
              for (let j = 0, len2 = spotifyResults[i].artists.length; j < len2; j++) {
                var currentArtist = spotifyResults[i].artists[j];
                var artistIndex = userArtists.findIndex(elem => elem.artist.toLowerCase() == currentArtist.name.toLowerCase());
                if (artistIndex < 0) {
                  var score = sourceArtists[i].weight * (len2 - j);
                  artistIndex = recArtists.findIndex(elem => elem.artist.toLowerCase() == currentArtist.name.toLowerCase());
                  if (artistIndex < 0) {
                    var id = currentArtist.id;
                    var basicArtist = new BasicArtist(currentArtist.name, score, BasicArtist.singleIdToArray(new ProviderId(SpotifyServiceProvider.PROVIDER, id)));
                    var sourceSimilarArtists: Array<BasicArtist> = [new BasicArtist(sourceArtists[i].artist, score, sourceArtists[i].id)];
                    recArtists.push(new SimilarArtist(basicArtist.artist, basicArtist.weight, basicArtist.id, sourceSimilarArtists, basicArtist.id));
                  }
                  else {
                    recArtists[artistIndex].weight = recArtists[artistIndex].weight + score;
                    recArtists[artistIndex].insertSourceArtist(sourceArtists[i], score);
                  }
                }
              }
            }

            resolve(SimilarArtist.sortByWeight(recArtists));
          });
      });
    });
  }

  private getRelatedArtistsWithDelay(delay, artistId, searchTimestamp: number) {
    return new Promise((resolve, error) => {
      var timer = setTimeout(() => {
        if (this.loadingService.isLoadActive(searchTimestamp)) {
          this.spotifyApi.getArtistRelatedArtists(artistId)
            .then((data) => {
              resolve(data);
            })
            .catch(e => error('An error occurred'));
        }
        else {
          console.log("Spotify search cancelled");
          this.activeTimers.forEach(timer => {
            clearTimeout(timer);
          });
          this.activeTimers = new Array();
        }
      },
        delay);
      this.activeTimers.push(timer);
    });
  }

  public fetchArtistInfo(artist: BasicArtist): Promise<FullArtist> {
    return new Promise((resolve, error) => {
      var similarArtist: SimilarArtist = new SimilarArtist(artist.artist, artist.weight, artist.id, undefined, undefined);
      var recArtists: Array<SimilarArtist> = [similarArtist];
      this.fetchSimilarArtistsInfo(recArtists, undefined)
        .then(f => resolve(f.pop()))
        .catch(e => error('An error occurred'));
    });
  }

  fetchSimilarArtistsInfo(recArtists: Array<SimilarArtist>, genericParams: GenericParams): Promise<Array<FullArtist>> {
    return new Promise((resolve, error) => {
      this.auth().then((success) => {

        if (!success) {
          error("User not logged in");
          return;
        }

        var searchTimestamp: number = undefined; if (genericParams) searchTimestamp = genericParams.timestamp;
        const apiCalls = [];
        const spotifyResults: Array<FullArtist> = [];

        this.loadingService.addToReferenceProgress(recArtists.length, this.providerSettings.orderId, searchTimestamp);
        for (let i = 0; i < recArtists.length; i++) {
          apiCalls.push(this.fetchInfoForArtist(i * this.apiDelay, recArtists[i], searchTimestamp));
        }

        let receivedEvents = 0;
        const apiCallsWithCatch = apiCalls.map(s => s.catch(e => {
          console.log("Error: " + e);
          return FullArtist.fromSimilarArtist(recArtists[spotifyResults.length]);
        }));
        concat(...apiCallsWithCatch).subscribe(
          (recArtist: FullArtist) => {
            spotifyResults.push(recArtist);
            this.loadingService.addToCurrentProgress(this.providerSettings.orderId, searchTimestamp);
            receivedEvents++;
          },
          error => {
            console.log("Error that should never happen: " + error);
          },
          () => {
            this.activeTimers = new Array();
            resolve(spotifyResults);
          });
      });
    });
  }

  public searchArtist(artistName: string): Promise<FullArtist[]> {
    return new Promise((resolve, error) => {
      this.auth().then((success) => {
        if (!success) {
          error("User not logged in");
          return;
        }
        this.searchForArtist(artistName)
          .then((artists: Array<FullArtist>) => {
            resolve(artists);
          })
          .catch(e => error('An error occurred'));
      });
    });
  }

  fetchArtistExtraInfo(basicArtist: BasicArtist): Promise<ArtistExtraInfo> {
    return new Promise((resolve, error) => {
      if (!basicArtist.getIdForProvider(SpotifyServiceProvider.PROVIDER)) {
        resolve(null);
        return;
      }

      var similarArtists: Array<FullArtist> = new Array();
      var topAlbums: Array<ArtistAlbum> = new Array();
      var topSongs: Array<string> = new Array();
      this.spotifyApi.getArtistRelatedArtists(basicArtist.getIdForProvider(SpotifyServiceProvider.PROVIDER))
        .then((data) => {
          for (let j = 0, len2 = data.artists.length; j < len2 && j < 5; j++) {
            var currentArtist = data.artists[j];
            var icon; if (currentArtist.images[2]) icon = currentArtist.images[2].url;
            var iconLarge; if (currentArtist.images[0]) iconLarge = currentArtist.images[0].url;
            similarArtists.push(new FullArtist(currentArtist.name, this.providerSettings.getSourceWeight(), BasicArtist.singleIdToArray(new ProviderId(SpotifyServiceProvider.PROVIDER, currentArtist.id)),
              icon, iconLarge, currentArtist.genres.slice(0, 5), undefined, undefined, undefined, undefined, undefined, undefined));
          }
          this.spotifyApi.getArtistAlbums(basicArtist.getIdForProvider(SpotifyServiceProvider.PROVIDER), { include_groups: 'album' })
            .then((data) => {
              for (let j = 0, len2 = data.items.length; j < len2 && j < 5; j++) {
                var currentAlbum = data.items[j];
                var icon; if (currentAlbum.images[0]) icon = currentAlbum.images[0].url;
                if (!topAlbums.find(elem => elem.album === currentAlbum.name)) {
                  topAlbums.push(new ArtistAlbum(currentAlbum.name, icon));
                }
              }
              this.spotifyApi.getArtistTopTracks(basicArtist.getIdForProvider(SpotifyServiceProvider.PROVIDER), 'from_token')
                .then((data) => {
                  for (let j = 0, len2 = data.tracks.length; j < len2 && j < 5; j++) {
                    var currentTrack = data.tracks[j];
                    topSongs.push(currentTrack.name);
                  }
                  var artistExtraInfo: ArtistExtraInfo = new ArtistExtraInfo(similarArtists, topAlbums, topSongs);
                  resolve(artistExtraInfo);
                })
                .catch(e => error('An error occurred'));
            })
            .catch(e => error('An error occurred'));
        })
        .catch(e => error('An error occurred'));
    });
  }

  fetchAlbumsForClarification(basicArtist: BasicArtist): Promise<ArtistAlbum[]> {
    return new Promise((resolve, error) => {
      var topAlbums: Array<ArtistAlbum> = new Array();
      if (!basicArtist.getIdForProvider(SpotifyServiceProvider.PROVIDER)) {
        resolve(topAlbums);
        return;
      }
      this.spotifyApi.getArtistAlbums(basicArtist.getIdForProvider(SpotifyServiceProvider.PROVIDER), { include_groups: 'album' })
        .then((data) => {
          let maxIt = 5;
          for (let j = 0, len2 = data.items.length; j < len2 && j < maxIt; j++) {
            var currentAlbum = data.items[j];
            var icon; if (currentAlbum.images[0]) icon = currentAlbum.images[0].url;
            if (!topAlbums.find(elem => elem.album === currentAlbum.name)) {
              topAlbums.push(new ArtistAlbum(currentAlbum.name, icon));
            }
            else {
              maxIt++;
            }
          }
          resolve(topAlbums);
        })
        .catch(e => error('An error occurred'));
    })
  }

  fetchArtistScrobbles(basicArtist: BasicArtist): Promise<number> {
    throw new Error("Method not implemented.");
  }

  fetchTrackScrobbles(basicArtist: BasicArtist, track: string): Promise<number> {
    throw new Error("Method not implemented.");
  }

  fetchListening(): Promise<ListenedTrack> {
    return new Promise((resolve, error) => {
      this.auth().then((success) => {
        if (!success) {
          error("User not logged in");
          return;
        }
        this.spotifyApi.getMyCurrentPlayingTrack()
          .then(track => {
            if (track) {
              var basicArtist = new BasicArtist(track.item.artists[0].name, this.providerSettings.getSourceWeight(), BasicArtist.singleIdToArray(new ProviderId(SpotifyServiceProvider.PROVIDER, track.item.artists[0].id)));
              var albumImage = undefined; if (track.item.album.images && track.item.album.images.length > 0) albumImage = track.item.album.images[0].url;
              var duration = undefined; if (Number(track.item.duration_ms)) duration = Number(track.item.duration_ms) / 1000;
              var id = undefined; if (track.item.id) id = track.item.id;
              if (id) {
                setTimeout(() => {
                  this.spotifyApi.containsMySavedTracks([id])
                    .then(contains => {
                      resolve(new ListenedTrack(basicArtist, track.item.album.name, track.item.name, duration, true, undefined, albumImage, contains[0]));
                    })
                    .catch(e => { console.log(e); error(e); });
                }, this.apiDelay);
              }
              else {
                resolve(new ListenedTrack(basicArtist, track.item.album.name, track.item.name, duration, true, undefined, albumImage, false));
              }
            }
            else {
              setTimeout(() => {
                this.spotifyApi.getMyRecentlyPlayedTracks()
                  .then(r => {
                    if (r.items && r.items.length > 0) {
                      var track = r.items[0].track as any;
                      var basicArtist = new BasicArtist(track.artists[0].name, this.providerSettings.getSourceWeight(), BasicArtist.singleIdToArray(new ProviderId(SpotifyServiceProvider.PROVIDER, track.artists[0].id)));
                      var albumImage = undefined; if (track.album.images && track.album.images.length > 0) albumImage = track.album.images[0].url;
                      var duration = undefined; if (Number(track.duration_ms)) duration = Number(track.duration_ms) / 1000;
                      var id = undefined; if (track.id) id = track.id;
                      if (id) {
                        setTimeout(() => {
                          this.spotifyApi.containsMySavedTracks([id])
                            .then(contains => {
                              resolve(new ListenedTrack(basicArtist, track.album.name, track.name, duration, false, undefined, albumImage, contains[0]));
                            })
                            .catch(e => { console.log(e); error(e); });
                        }, this.apiDelay);
                      }
                      else {
                        resolve(new ListenedTrack(basicArtist, track.album.name, track.name, duration, false, undefined, albumImage, false));
                      }
                    }
                    else {
                      resolve(undefined);
                    }
                  })
                  .catch(e => { console.log(e); error(e); });
              }, this.apiDelay);
            }
          })
          .catch(e => {
            console.log(e); error('An error occurred');
          });
      });
    });
  }

  generatePlaylist(generateParams: GenerateParams): Promise<GenerateResult> {
    return new Promise((resolve, error) => {
      this.loadingService.searchTimestamp = generateParams.timestamp;
      this.loadingService.setLoadingPhase('Artist Tracks', [this.providerSettings.orderId], 0, 90);
      this.loadingService.addToReferenceProgress(generateParams.customList.listArtists.length, this.providerSettings.orderId, generateParams.timestamp);
      this.auth().then((success) => {
        if (!success) {
          error("User not logged in");
          return;
        }
        const apiCalls = [];
        const tracksURIs = [];
        if (!generateParams.customList.listArtists || generateParams.customList.listArtists.length == 0) {
          error("List is empty");
          return;
        }
        let extraDelayIndex = 0;
        generateParams.customList.listArtists.forEach((artist, index) => {
          let id = artist.getIdForProvider(SpotifyServiceProvider.PROVIDER);
          apiCalls.push(this.getTopTracksWithDelay(artist, this.apiDelay * (index + extraDelayIndex), generateParams.timestamp));
          if (!id) extraDelayIndex++;
        })

        let index = 0;
        let notFoundArtists: Array<BasicArtist> = new Array();
        let noSongsArtists: Array<BasicArtist> = new Array();

        const apiCallsWithCatch = apiCalls.map(s => s.catch(e => { console.log("Error: " + e); return null; }));
        concat(...apiCallsWithCatch).subscribe(
          (topTracksResponse: SpotifyApi.ArtistsTopTracksResponse) => {
            this.loadingService.addToCurrentProgress(this.providerSettings.orderId, generateParams.timestamp);
            if (topTracksResponse) {
              var topSongsToChooseFrom = topTracksResponse.tracks.slice(0, generateParams.songsToChooseFrom);
              if (topSongsToChooseFrom.length > 0) {
                this.utilsService.shuffleArray(topSongsToChooseFrom);
                var chosenTracks = topSongsToChooseFrom.slice(0, generateParams.songsPerArtist);
                chosenTracks.forEach(track => tracksURIs.push(track.uri));
              }
              else {
                noSongsArtists.push(generateParams.customList.listArtists[index]);
              }
            }
            else {
              notFoundArtists.push(generateParams.customList.listArtists[index]);
            }
            index++;
          },
          error => {
            console.log("Error that should never happen: " + error);
          },
          () => {
            this.activeTimers = new Array();
            if (tracksURIs.length > 0) {
              this.loadingService.setLoadingPhase('Generating Playlist', [this.providerSettings.orderId], 90, 10);
              this.loadingService.addToReferenceProgress(Math.floor((tracksURIs.length - 1) / 100 + 1), this.providerSettings.orderId, generateParams.timestamp);
              if (generateParams.shuffle) {
                this.utilsService.shuffleArray(tracksURIs);
              }
            }
            var userId = this.userProfile.id;
            if (generateParams.listToAddTo) {
              this.addAllTracksToPlaylist(generateParams.listToAddTo.id, tracksURIs, generateParams.timestamp).then(() => resolve(new GenerateResult(notFoundArtists, noSongsArtists)));
            }
            else {
              this.spotifyApi.createPlaylist(userId, { name: generateParams.playlistName, public: !generateParams.privatePlaylist, description: generateParams.description })
                .then(pl => {
                  this.addAllTracksToPlaylist(pl.id, tracksURIs, generateParams.timestamp).then(() => resolve(new GenerateResult(notFoundArtists, noSongsArtists)));
                })
                .catch(e => error('An error occurred'));
            }
          });
      })
    })
  }

  private getTopTracksWithDelay(artist: BasicArtist, delay: number, timestamp: number) {
    return new Promise((resolve, error) => {
      let artistId = artist.getIdForProvider(SpotifyServiceProvider.PROVIDER);
      if (!artistId) {
        this.getRecArtistInfoWithDelayByName(delay, artist, timestamp)
          .then((receivedArtist: FullArtist) => {
            artistId = receivedArtist.getIdForProvider(SpotifyServiceProvider.PROVIDER);
            if (!artistId) {
              resolve(undefined);
              return;
            }
            var timer = setTimeout(() => {
              if (this.loadingService.isLoadActive(timestamp)) {
                this.spotifyApi.getArtistTopTracks(artistId, 'from_token').then(r => resolve(r)).catch(e => { console.log("am prins eroare spotify api"); error(e); });
              }
              else {
                console.log("Spotify search cancelled");
                this.activeTimers.forEach(timer => {
                  clearTimeout(timer);
                });
                this.activeTimers = new Array();
              }
            }, this.apiDelay);
            this.activeTimers.push(Number(timer));
          })
          .catch(e => error(e));
      }
      else {
        var timer = setTimeout(() => {
          if (this.loadingService.isLoadActive(timestamp)) {
            this.spotifyApi.getArtistTopTracks(artistId, 'from_token').then(r => resolve(r)).catch(e => error(e));
          }
          else {
            console.log("Spotify search cancelled");
            this.activeTimers.forEach(timer => {
              clearTimeout(timer);
            });
            this.activeTimers = new Array();
          }
        }, delay);
        this.activeTimers.push(Number(timer));
      }
    })
  }

  private addAllTracksToPlaylist(plId: string, tracksURIs, timestamp: number) {
    return new Promise((resolve, error) => {
      const apiCalls = [];
      for (var i = 0; i <= (tracksURIs.length - 1) / 100; i++) {
        apiCalls.push(this.addTracksToPlaylistWithDelay(plId, tracksURIs.slice(i * 100, (i + 1) * 100), i * this.apiDelay, timestamp));
      }
      concat(...apiCalls).subscribe(
        r => { this.loadingService.addToCurrentProgress(this.providerSettings.orderId, timestamp); },
        e => { this.loadingService.addToCurrentProgress(this.providerSettings.orderId, timestamp); console.log(e); error('An error occurred'); },
        () => { this.activeTimers = new Array(); resolve(); }
      );
    });
  }

  private addTracksToPlaylistWithDelay(plId: string, trackURIs: Array<string>, delay: number, timestamp: number) {
    return new Promise((resolve, error) => {
      var timer = setTimeout(() => {
        if (this.loadingService.isLoadActive(timestamp)) {
          this.spotifyApi.addTracksToPlaylist(plId, trackURIs).then(r => resolve(r)).catch(e => error(e));
        }
        else {
          console.log("Spotify search cancelled");
          this.activeTimers.forEach(timer => {
            clearTimeout(timer);
          });
          this.activeTimers = new Array();
        }
      }, delay);
      this.activeTimers.push(Number(timer));
    });
  }

  public fetchPrivateLists(): Promise<Array<ExternalList>> {
    return new Promise((resolve, error) => {
      this.auth().then((success) => {
        if (!success) {
          error("User not logged in");
          return;
        }
        var userId = this.userProfile.id;
        this.getPrivateLists(userId, 0, new Array<ExternalList>())
          .then(l => resolve(l))
          .catch(e => error('An error occurred'));
      })
    });
  }

  private getPrivateLists(userId: string, offsetId: number, externalLists: Array<ExternalList>): Promise<Array<ExternalList>> {
    return new Promise((resolve, error) => {
      this.spotifyApi.getUserPlaylists(userId, { limit: 50, offset: offsetId })
        .then(playlists => {
          var totalItems = playlists.total;
          var limitItems = playlists.limit;
          playlists.items.forEach(playlist => externalLists.push(new ExternalList(playlist.name, playlist.id, SpotifyServiceProvider.PROVIDER)));
          offsetId = offsetId + limitItems;
          if (offsetId < totalItems) {
            setTimeout(() => { this.getPrivateLists(userId, offsetId, externalLists).then(l => resolve(l)).catch(e => error(e)); }, this.apiDelay);
          }
          else {
            resolve(externalLists);
          }
        })
        .catch(e => error('An error occurred'));
    })
  }

  private fetchListArtists(listArtists: Array<BasicArtist>, playlistId: string, offsetId: number, searchTimestamp: number) {
    return new Promise((resolve, error) => {
      this.spotifyApi.getPlaylistTracks(playlistId, { limit: 100, offset: offsetId, fields: 'total,limit,items.track.artists(name,id)' })
        .then(r => {
          var totalItems = r.total;
          var limitItems = r.limit;
          r.items.forEach(item => {
            if (item.track.artists[0])
              listArtists.push(new BasicArtist(item.track.artists[0].name, Number(1), BasicArtist.singleIdToArray(new ProviderId(SpotifyServiceProvider.PROVIDER, item.track.artists[0].id))));
          });
          offsetId = offsetId + limitItems;
          if (offsetId < totalItems) {
            setTimeout(() => {
              if (this.loadingService.isLoadActive(searchTimestamp)) {
                this.fetchListArtists(listArtists, playlistId, offsetId, searchTimestamp).then(l => resolve(l)).catch(e => error(e));
              }
              else {
                console.log("Spotify search cancelled");
              }
            }, this.apiDelay);
          }
          else {
            resolve(BasicArtist.mergeListenedWithAdd(listArtists));
          }
        })
        .catch(e => console.log("An error occurred"));
    })
  }

  getProviderImage(enabled: boolean): string {
    if (enabled) return './assets/images/spotify.png'; else return './assets/images/spotify-greyed.png';
  }

  getProviderSettings(): ProviderSettings {
    if (this.providerSettings) {
      return this.providerSettings;
    }
    else {
      return SpotifyServiceProvider.DEFAULT_SETTINGS;
    }
  }

  getDefaultOperationsWithoutAuth(): string[] {
    var operationsNoAuth: Array<string> = new Array();
    return operationsNoAuth;
  }

  getApiDelay(): number {
    return this.apiDelay;
  }

  async getAllArtistAlbums(discoArtist: BasicDiscoArtist): Promise<ArtistAlbum[]> {
    var authed = await this.auth();
    if (!authed) throw new Error("User not logged in")
    var discographyFilters = (await this.storageService.getSettingsCache()).discographyFilters;
    if (!discoArtist.artist.getIdForProvider(SpotifyServiceProvider.PROVIDER)) {
      var similarArtist: SimilarArtist = new SimilarArtist(discoArtist.artist.artist, discoArtist.artist.weight, discoArtist.artist.id, undefined, undefined);
      var recArtists: Array<SimilarArtist> = [similarArtist];
      var fullArtist = (await this.fetchSimilarArtistsInfo(recArtists, undefined)).pop();
      if (!fullArtist || !fullArtist.getIdForProvider(SpotifyServiceProvider.PROVIDER)) {
        throw new Error("Artist not found");
      }
      discoArtist.artist.addProvider(new ProviderId(SpotifyServiceProvider.PROVIDER, fullArtist.getIdForProvider(SpotifyServiceProvider.PROVIDER)));
    }
    var basicArtist = discoArtist.artist;
    var rawAlbums = await this.spotifyApi.getArtistAlbums(basicArtist.getIdForProvider(SpotifyServiceProvider.PROVIDER), { include_groups: 'album', limit: "50" })
    var artistAlbums: Array<ArtistAlbum> = new Array();
    for (let j = 0, len2 = rawAlbums.items.length; j < len2; j++) {
      var currentAlbum = rawAlbums.items[j] as SpotifyApi.AlbumObjectFull;
      var icon; if (currentAlbum.images[0]) icon = currentAlbum.images[0].url;
      if (!artistAlbums.find(elem => elem.album === currentAlbum.name)) {
        artistAlbums.push(new ArtistAlbum(currentAlbum.name, icon, currentAlbum.id, new Date(Date.parse(currentAlbum.release_date))));
      }
    }
    artistAlbums = artistAlbums.filter(album => !artistAlbums.find(album2 => this.isSameAlbum(album, album2) && album.album !== album2.album)
      && !discographyFilters.find(filter => new RegExp(filter.toLowerCase()).test(album.album.toLowerCase())))
    return orderBy(artistAlbums, 'releaseDate', 'desc');
  }

  private isSameAlbum(album: ArtistAlbum, album2: ArtistAlbum): boolean {
    for (let i = 0; i < this.SAME_ALBUM_REGEX.length; i++) {
      var regex = this.SAME_ALBUM_REGEX[i]
      if (album2.album.toLowerCase() === album.album.toLowerCase().replace(regex, "").trim()) {
        return true;
      }
    }
    return false;
  }

  private fetchInfoForArtist(delay, artist: SimilarArtist, searchTimestamp: number) {
    return new Promise((resolve, error) => {
      if (artist.getIdForProvider(SpotifyServiceProvider.PROVIDER)) {
        var timer = setTimeout(() => {
          if (this.loadingService.isLoadActive(searchTimestamp)) {
            this.getArtistById(artist.getIdForProvider(SpotifyServiceProvider.PROVIDER))
              .then((receivedArtist: FullArtist) => {
                if (receivedArtist) {
                  var fullArtist = new FullArtist(artist.artist, artist.weight, artist.id, receivedArtist.iconSmall, receivedArtist.iconLarge,
                    receivedArtist.genres, undefined, receivedArtist.listeners, undefined, receivedArtist.url, artist.sourceArtists, artist.providers);
                }
                resolve(fullArtist);
              })
              .catch(e => error('An error occurred'));
          }
          else {
            console.log("Spotify search cancelled");
            this.activeTimers.forEach(timer => {
              clearTimeout(timer);
            });
            this.activeTimers = new Array();
          }
        },
          delay);
        this.activeTimers.push(timer);
      }
      else {
        this.getRecArtistInfoWithDelayByName(delay, artist, searchTimestamp)
          .then((receivedArtist: FullArtist) => {
            receivedArtist.sourceArtists = artist.sourceArtists;
            receivedArtist.providers = artist.providers;
            resolve(receivedArtist);
          })
          .catch(e => error('An error occurred'));
      }
    });
  }

  private fetchFollowedArtists(afterId: any, alreadyFetched: Array<any>, searchTimestamp: number) {
    return new Promise((resolve, error) => {
      this.spotifyApi.getFollowedArtists({ limit: 50, after: afterId })
        .then(data => {
          var artistItems = data.artists.items;
          alreadyFetched = alreadyFetched.concat(artistItems);
          afterId = data.artists.cursors.after;
          if (afterId) {
            setTimeout(() => {
              if (this.loadingService.isLoadActive(searchTimestamp)) {
                this.fetchFollowedArtists(afterId, alreadyFetched, searchTimestamp)
                  .then(totalFetched => {
                    resolve(totalFetched);
                  })
                  .catch(e => error('An error occurred'));
              }
              else {
                console.log("Spotify search cancelled");
              }
            }, this.apiDelay);
          }
          else {
            resolve(alreadyFetched); //alreadyFetched devine totalFetched -- am gasit toti artistii
          }
        })
        .catch(e => error('An error occurred'));
    });
  }

  private fetchSavedTracks(offsetId: any, alreadyFetched: Array<any>, searchTimestamp: number) {
    return new Promise((resolve, error) => {
      this.spotifyApi.getMySavedTracks({ limit: 50, offset: offsetId })
        .then(data => {
          var trackItems = data.items;
          var totalItems = data.total;
          var limitItems = data.limit;
          alreadyFetched = alreadyFetched.concat(trackItems);
          offsetId = offsetId + limitItems;
          if (offsetId < totalItems) {
            setTimeout(() => {
              if (this.loadingService.isLoadActive(searchTimestamp)) {
                this.fetchSavedTracks(offsetId, alreadyFetched, searchTimestamp)
                  .then(totalFetched => {
                    resolve(totalFetched);
                  })
                  .catch(e => error('An error occurred'));
              }
              else {
                console.log("Spotify search cancelled");
              }
            }, this.apiDelay);
          }
          else {
            resolve(alreadyFetched);
          }
        })
        .catch(e => { console.log(e); error('An error occurred'); });
    });
  }

  private fetchTopArtists(timeRange: any) {
    return new Promise((resolve, error) => {
      this.spotifyApi.getMyTopArtists({ limit: 50, time_range: timeRange })
        .then(data => {
          var trackItems = data.items;
          resolve(trackItems);
        })
        .catch(e => error('An error occurred'));
    });
  }

  private getRelatedArtistsWithDelayByName(delay1, delay2, artist: BasicArtist, searchTimestamp: number) {
    return new Promise((resolve, error) => {
      var timer = setTimeout(() => {
        if (this.loadingService.isLoadActive(searchTimestamp)) {
          this.searchForArtist(artist.artist)
            .then((receivedArtists: Array<FullArtist>) => {
              if (!receivedArtists) {
                resolve(null);
              }
              else {
                var receivedArtistsFiltered = receivedArtists.filter((a, index) => a.artist.toLowerCase() === artist.artist.toLowerCase() || index == 0);
                if (receivedArtistsFiltered.length == 0) {
                  console.log("No artists on Spotify with the name " + artist.artist + " , will skip recs");
                  resolve(null);
                  return;
                }
                else if (receivedArtistsFiltered.length > 1) {
                  this.applyMultipleAlbumsLogicForRelatedArtists(artist, receivedArtistsFiltered, delay2, searchTimestamp).then(f => resolve(f));
                }
                else {
                  var receivedArtist = receivedArtistsFiltered[0];
                  var id = receivedArtist.getIdForProvider(SpotifyServiceProvider.PROVIDER);
                  artist.addProvider(new ProviderId(SpotifyServiceProvider.PROVIDER, id));
                  this.getRelatedArtistsWithDelay(delay2, id, searchTimestamp)
                    .then(data => {
                      resolve(data);
                    })
                    .catch(e => { console.log(e); resolve(null) });
                }
              }
            })
            .catch(e => { console.log(e); resolve(null) });
        }
        else {
          console.log("Spotify search cancelled");
          this.activeTimers.forEach(timer => {
            clearTimeout(timer);
          });
          this.activeTimers = new Array();
        }
      },
        delay1);
      this.activeTimers.push(timer);
    });
  }

  private getRecArtistInfoWithDelayByName(delay, basicArtist: BasicArtist, searchTimestamp: number) {
    return new Promise((resolve, error) => {
      var timer = setTimeout(() => {
        if (this.loadingService.isLoadActive(searchTimestamp)) {
          this.searchForArtist(basicArtist.artist)
            .then((fullArtists: Array<FullArtist>) => {
              if (!fullArtists) {
                resolve(FullArtist.fromBasicArtist(basicArtist));
              }
              else {
                var receivedArtistsFiltered = fullArtists.filter((a, index) => a.artist.toLowerCase() === basicArtist.artist.toLowerCase() || index == 0);
                if (receivedArtistsFiltered.length == 0) {
                  resolve(FullArtist.fromBasicArtist(basicArtist));
                }
                else if (receivedArtistsFiltered.length > 1) {
                  this.applyMultipleAlbumsLogicForArtistInfo(basicArtist, receivedArtistsFiltered).then(f => resolve(f));
                }
                else {
                  var receivedArtist = receivedArtistsFiltered[0];
                  resolve(receivedArtist);
                }
              }
            })
            .catch(e => resolve(FullArtist.fromBasicArtist(basicArtist)));
        }
        else {
          console.log("Spotify search cancelled");
          this.activeTimers.forEach(timer => {
            clearTimeout(timer);
          });
          this.activeTimers = new Array();
        }
      },
        delay);
      this.activeTimers.push(timer);
    });
  }

  private applyMultipleAlbumsLogicForRelatedArtists(artist: BasicArtist, receivedArtistsFiltered: Array<FullArtist>, delay2: number, searchTimestamp: number) {
    return new Promise((resolve, error) => {
      AllServices.providerService.getServiceProvider(artist.id[0].provider).fetchAlbumsForClarification(artist)
        .then((albums: Array<ArtistAlbum>) => {
          if (albums.length > 0) {
            var receivedArtist;
            this.getArtistByMultipleAlbums(artist, albums)
              .then((fullArtist: FullArtist) => {
                if (!fullArtist) {
                  resolve(null);
                }
                else {
                  this.getRelatedArtistsWithDelay(delay2, fullArtist.getIdForProvider(SpotifyServiceProvider.PROVIDER), searchTimestamp)
                    .then(data => {
                      resolve(data);
                    })
                    .catch(e => { console.log(e); resolve(null) });
                }
              })
              .catch(e => { console.log(e); resolve(null) });
          }
          else {
            receivedArtist = receivedArtistsFiltered.filter(a => a.artist.toLowerCase() === artist.artist.toLowerCase())[0];
            this.getRelatedArtistsWithDelay(delay2, receivedArtist.getIdForProvider(SpotifyServiceProvider.PROVIDER), searchTimestamp)
              .then(data => {
                resolve(data);
              })
              .catch(e => { console.log(e); resolve(null) });
          }
        })
        .catch(e => { console.log(e); resolve(null) });
    });
  }

  private applyMultipleAlbumsLogicForArtistInfo(basicArtist: BasicArtist, receivedArtistsFiltered: Array<FullArtist>) {
    return new Promise((resolve, error) => {
      AllServices.providerService.getServiceProvider(basicArtist.id[0].provider).fetchAlbumsForClarification(basicArtist)
        .then((albums: Array<ArtistAlbum>) => {
          if (albums.length > 0) {
            this.getArtistByMultipleAlbums(basicArtist, albums)
              .then(fullArtist => {
                if (!fullArtist) {
                  fullArtist = FullArtist.fromBasicArtist(basicArtist);
                }
                resolve(fullArtist);
              });
          }
          else {
            var receivedArtist = receivedArtistsFiltered.filter(a => a.artist.toLowerCase() === basicArtist.artist.toLowerCase())[0];
            resolve(receivedArtist);
          }
        })
        .catch(e => resolve(FullArtist.fromBasicArtist(basicArtist)));
    })
  }

  private async getArtistByMultipleAlbums(basicArtist: BasicArtist, albums: Array<ArtistAlbum>) {
    for (let i = 0; i < albums.length; i++) {
      let album = albums[i];
      let fullArtist = null;
      try {
        fullArtist = await this.getArtistByAlbum(basicArtist, album.album, this.apiDelay);
      } catch (error) {
        console.log(error);
      }
      if (fullArtist) {
        return fullArtist;
      }
    }
    return null;
  }

  private searchForArtist(artist): Promise<Array<FullArtist>> {
    return new Promise((resolve, error) => {
      this.spotifyApi.searchArtists(artist)
        .then(data => {
          var artists = data.artists.items;
          if (artists[0]) {
            var searchResults: Array<FullArtist> = new Array();
            artists.forEach(artist => {
              var icon; if (artist.images[2]) icon = artist.images[2].url;
              var iconLarge; if (artist.images[0]) iconLarge = artist.images[0].url;
              var followers; if (artist.followers) followers = Number(artist.followers.total);
              var url; if (artist.external_urls.spotify) url = artist.external_urls.spotify;
              var fullArtist = new FullArtist(artist.name, this.providerSettings.getSourceWeight(), BasicArtist.singleIdToArray(new ProviderId(SpotifyServiceProvider.PROVIDER, artist.id)), icon, iconLarge, artist.genres.slice(0, 5),
                undefined, followers, undefined, url, undefined, undefined);
              searchResults.push(fullArtist);
            });
            resolve(searchResults);
          }
          else {
            console.log("No artist found on Spotify for " + artist);
            resolve(null);
          }
        })
        .catch(e => error('An error occurred'));
    });
  }

  private getArtistById(id): Promise<FullArtist> {
    return new Promise((resolve, error) => {
      this.spotifyApi.getArtist(id)
        .then(artist => {
          if (artist) {
            var icon; if (artist.images[2]) icon = artist.images[2].url;
            var iconLarge; if (artist.images[0]) iconLarge = artist.images[0].url;
            var followers; if (artist.followers) followers = Number(artist.followers.total);
            var url; if (artist.external_urls.spotify) url = artist.external_urls.spotify;
            var searchResult = new FullArtist(artist.name, this.providerSettings.getSourceWeight(), BasicArtist.singleIdToArray(new ProviderId(SpotifyServiceProvider.PROVIDER, artist.id)), icon, iconLarge, artist.genres.slice(0, 5),
              undefined, followers, undefined, url, undefined, undefined);
            resolve(searchResult);
          }
          else {
            resolve(null);
          }
        })
        .catch(e => error('An error occurred'));
    });
  }

  private getArtistByAlbum(artist: BasicArtist, searchAlbum: string, delay: number) {
    return new Promise((resolve, error) => {
      setTimeout(() => {
        var q = "album:" + searchAlbum + " artist:" + artist.artist;
        this.spotifyApi.searchAlbums(q)
          .then(data => {
            var albums = data.albums.items;
            if (albums[0]) {
              var album = albums[0] as any;
              if (album.artists && album.artists.length > 0) {
                var id = album.artists[0].id;
                var name = album.artists[0].name;
                if (name.toLowerCase() === artist.artist.toLowerCase()) {
                  this.getArtistById(id)
                    .then(fullArtist => {
                      artist.id.forEach(provider => fullArtist.addProvider(provider));
                      resolve(fullArtist);
                    })
                    .catch(e => error('An error occurred'));
                  return;
                }
              }
            }
            resolve(null);
          })
          .catch(e => error('An error occurred'));
      }, delay);
    })
  }

}
