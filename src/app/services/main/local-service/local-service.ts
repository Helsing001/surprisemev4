import { Injectable } from '@angular/core';
import { MylistsServiceProvider } from '../../other/mylists-service/mylists-service';
import { GenericService } from '../../../models/generic/generic-service';
import { BasicArtist, ProviderId } from '../../../models/artist/basic-artist';
import { FullArtist } from '../../../models/artist/full-artist';
import { StorageServiceProvider } from '../../other/storage-service/storage-service';
import { LocalParams, GenericParams } from '../../../models/params/search-params';
import { MyLists } from '../../../models/list/my-lists';
import { groupBy, reduce, values, orderBy, cloneDeep } from 'lodash';
import { ProviderSettings } from '../../../models/providers/provider-settings';
import { LoadingServiceProvider } from '../../other/loading-service/loading-service';
import { ArtistExtraInfo } from '../../../models/artist/artist-extra-info';
import { SimilarArtist } from '../../../models/artist/similar-artist';
import { GenerateParams } from '../../../models/params/generate-params';
import { GenerateResult } from '../../../models/other/generate-result';
import { ProviderOperations } from '../../../models/providers/provider-operations';
import { CustomList } from '../../../models/list/custom-list';
import { ListenedTrack } from '../../../models/other/listened-track';
import { ExternalList } from '../../../models/list/external-list';
import { Platform } from '@ionic/angular';
import { AllServices } from '../../../models/other/all-services';
import { MusicApp } from '../../../models/other/music-app';
import { ArtistAlbum } from '../../../models/album/artist-album';
import { BasicDiscoArtist } from '../../../models/artist/basic-disco-artist';

declare var cordova: any;

@Injectable()
export class LocalServiceProvider extends GenericService {

  public static PROVIDER = 'Local';
  public static DEFAULT_SETTINGS = new ProviderSettings(1, LocalServiceProvider.PROVIDER, true, false, false, false, true, false, true, true, true, false);

  private providerSettings: ProviderSettings;
  private myLists: MyLists;

  private notificationListener;
  private startedListener: boolean = false;
  private listenedTrack: ListenedTrack;

  constructor(private myListsService: MylistsServiceProvider, public storageService: StorageServiceProvider, public loadingService: LoadingServiceProvider,
    private platform: Platform) {
    super();
    myListsService.getMyLists().then(myLists => this.myLists = myLists);

    this.storageService.getSettingsCache().then(settingsCache => {
      var localSettings: ProviderSettings;
      if (!settingsCache || !settingsCache.localProviderSettings) {
        localSettings = LocalServiceProvider.DEFAULT_SETTINGS;
      }
      else {
        this.processVersionDifferences(settingsCache.localProviderSettings);
        localSettings = settingsCache.localProviderSettings;
      }
      this.providerSettings = localSettings;
    })

    this.startNotificationService().catch(e => { });
  }

  private processVersionDifferences(settings: ProviderSettings) {
    if (!settings.source) {
      settings.setSource(this.storageService, true);
    }
    if (!settings.scrobble) {
      settings.setScrobble(this.storageService, true);
    }
    if (!settings.listening) {
      settings.setListening(this.storageService, true);
    }
  }

  getOwnName(): string {
    return LocalServiceProvider.PROVIDER;
  }

  auth(): Promise<boolean> {
    return new Promise(resolve => {
      resolve(true);
    });
  }

  forgetAuth() { }

  public isLongAuth(): boolean {
    return false;
  }    

  fetchUserArtists(genericParams: GenericParams): Promise<BasicArtist[]> {
    return new Promise(resolve => {
      this.loadingService.addToReferenceProgress(1, this.providerSettings.orderId, genericParams.timestamp);
      var userArtists: Array<BasicArtist> = new Array();
      this.myLists.getAllLists().forEach(customList => {
        if (customList.partOfLibrary) {
          var listArtistsToBeAdded = customList.listArtists.filter(listArtist => !userArtists.find(listenedArtist => listenedArtist.artist.toLowerCase() === listArtist.artist.toLowerCase()));
          userArtists = userArtists.concat(listArtistsToBeAdded);
        }
      });
      this.loadingService.addToCurrentProgress(this.providerSettings.orderId, genericParams.timestamp);
      resolve(userArtists);
    })
  }

  fetchSourceArtists(localParams: LocalParams, genericParams: GenericParams): Promise<BasicArtist[]> {
    return new Promise(resolve => {
      this.loadingService.addToReferenceProgress(1, localParams.id, genericParams.timestamp);
      var sourceArtists = this.getSourceArtists(localParams);
      this.loadingService.addToCurrentProgress(localParams.id, genericParams.timestamp);
      resolve(sourceArtists as Array<BasicArtist>);
    });
  }

  fetchSimilarArtists(userArtists: BasicArtist[], sourceArtists: BasicArtist[], genericParams: GenericParams): Promise<SimilarArtist[]> {
    throw new Error("Method not implemented.");
  }

  fetchArtistInfo(recArtists: BasicArtist): Promise<FullArtist> {
    throw new Error("Method not implemented.");
  }

  fetchSimilarArtistsInfo(recArtists: Array<SimilarArtist>, genericParams: GenericParams): Promise<Array<FullArtist>> {
    throw new Error("Method not implemented.");
  }

  searchArtist(artistName: string): Promise<FullArtist[]> {
    throw new Error("Method not implemented.");
  }

  fetchArtistExtraInfo(basicArtist: BasicArtist): Promise<ArtistExtraInfo> {
    throw new Error("Method not implemented.");
  }

  fetchAlbumsForClarification(basicArtist: BasicArtist): Promise<ArtistAlbum[]> {
    throw new Error("Method not implemented.");
  }

  fetchArtistScrobbles(basicArtist: BasicArtist): Promise<number> {
    return new Promise((resolve, error) => {
      let scrobbles: number = 0;
      this.myLists.getAllLists().forEach(customList => {
        if (customList.partOfLibrary && customList.listType !== CustomList.BANNED) {
          var artists = customList.listArtists.filter(listArtist => listArtist.artist.toLowerCase() === basicArtist.artist.toLowerCase());
          if (artists && artists.length > 0) {
            scrobbles += Number(artists[0].weight.toFixed(0));
          }
        }
      })
      resolve(scrobbles);
    })
  }

  fetchTrackScrobbles(basicArtist: BasicArtist, track: string): Promise<number> {
    return new Promise((resolve, error) => {
      resolve(0);
    });
  }

  fetchListening(): Promise<ListenedTrack> {
    return new Promise((resolve, error) => {
      if (this.startedListener) {
        resolve(this.listenedTrack);
      }
      else {
        this.startNotificationService().catch(e => error(e));
      }
    });
  }

  fetchPrivateLists(): Promise<ExternalList[]> {
    throw new Error("Method not implemented.");
  }

  generatePlaylist(generateParams: GenerateParams): Promise<GenerateResult> {
    throw new Error("Method not implemented.");
  }

  getProviderImage(enabled: boolean): string {
    if (enabled) return './assets/images/local.png'; else return './assets/images/local-greyed.png';
  }

  getProviderSettings(): ProviderSettings {
    if (this.providerSettings) {
      return this.providerSettings;
    }
    else {
      return LocalServiceProvider.DEFAULT_SETTINGS;
    }
  }

  getDefaultOperationsWithoutAuth(): string[] {
    var operationsNoAuth: Array<string> = new Array();
    operationsNoAuth.push(ProviderOperations.LIBRARY);
    operationsNoAuth.push(ProviderOperations.SOURCE);
    operationsNoAuth.push(ProviderOperations.SCROBBLE);
    return operationsNoAuth;
  }

  getApiDelay(): number {
    throw new Error("Method not implemented.");
  }

  getAllArtistAlbums(discoArtist: BasicDiscoArtist): Promise<ArtistAlbum[]> {
    throw new Error("Method not implemented.");
  }    

  private startNotificationService() {
    return new Promise((resolve, error) => {
      if (this.platform.is("cordova")) {
        this.notificationListener = cordova.plugins.NotificationListener;
      }
      else {
        error("unavailable");
      }
      this.notificationListener.isRunning(
        status => {
          console.log("Notification service running: " + status.isRunning);
          if (!status.isRunning) {
            this.notificationListener.toggle(() => {
              console.log("Toggle done");
            }, () => {
              console.error("Error calling notification toggle");
            });
            error("no permission");
          }
          else {
            this.addNotificationListener().catch(e => error(e));
          }
        },
        () => {
          console.error("Error checking if notification service is running");
          error("unavailable");
        });
    })
  }

  private addNotificationListener() {
    return new Promise((resolve, error) => {
      this.startedListener = true;
      this.notificationListener.addListener(
        data => {
          var musicApps: Array<MusicApp> = AllServices.providerService.getMusicApps();
          var appPackage = data.package;
          if (musicApps.find(app => app.appPackage === appPackage)) {
            var basicArtist = new BasicArtist(data.text, this.providerSettings.getSourceWeight(), []);
            this.listenedTrack = new ListenedTrack(basicArtist, "NA", data.title, undefined, true, undefined, undefined, false);
          }
        },
        e => {
          this.startedListener = false;
          console.log(e);
          error("unavailable");
        });
    })
  }

  private getSourceArtists(localParams: LocalParams): Array<BasicArtist> {
    var sourceArtists: Array<BasicArtist> = new Array();
    localParams.sourceLists.forEach(customListName => {
      var customList = this.myLists.getAllLists().find(customListInFind => customListInFind.listName == customListName);
      if (customList) {
        sourceArtists = sourceArtists.concat(customList.listArtists);
      }
    });

    var c = cloneDeep(sourceArtists);
    var nameMap: Map<string, string> = new Map();
    c.forEach(s => {
      var lc = s.artist.toLowerCase();
      nameMap.set(lc, s.artist);
      s.artist = lc;
    })

    var g = groupBy(c, 'artist');
    var r = reduce(g, function (result, value: Array<BasicArtist>, key: string) {
      var finalArtist = new BasicArtist(key, 0, new Array());
      value.forEach(artist => {
        finalArtist.weight = Number(finalArtist.weight) + Number(artist.weight);
        artist.id.forEach((providerId: ProviderId) => {
          finalArtist.addProvider(providerId);
        })
      })
      result[key] = finalArtist;
      return result;
    }, {});
    var v = values(r);
    var o = orderBy(v, 'count', 'desc') as Array<BasicArtist>;
    o.forEach(s => s.artist = nameMap.get(s.artist));
    return o;
  }

}
