import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { FullArtist } from '../../../models/artist/full-artist';
import { map } from 'rxjs/operators';

@Injectable()
export class TheAudioDbServiceProvider {

    private apiURL = "https://theaudiodb.com/api/v1/json/";
    private apiKey = "523532/";
    private byName = "search.php?s=";
    private byMbId = "artist-mb.php?i=";

    private apiDelay = 250;

    constructor(public http: Http) {

    }

    public replaceImage(fullArtist: FullArtist, mbId: string, delayIndex: number = 0) {
        return new Promise((resolve, error) => {            
            var delay = delayIndex * this.apiDelay;
            setTimeout(() => {
                var url;
                if (mbId) {
                    url = this.getUrlForMbIdSearch(mbId);
                }
                else {
                    url = this.getUrlForNameSearch(fullArtist.artist);
                }
                this.http.get(url).pipe(map(res => res.json())).toPromise()
                    .then(data => {
                        if (!data.artists || data.artists.length == 0) {
                            if (mbId) {
                                url = this.getUrlForNameSearch(fullArtist.artist);
                                this.http.get(url).pipe(map(res => res.json())).toPromise()
                                    .then(data => {
                                        this.replaceImageByData(fullArtist, data);
                                        resolve();
                                    })
                                    .catch(err => error(err));
                            }
                            else {
                                resolve();
                            }
                        }
                        else {
                            this.replaceImageByData(fullArtist, data);
                            resolve();
                        }
                    })
                    .catch(err => error(err));
            }, delay);
        })
    }

    private replaceImageByData(fullArtist: FullArtist, data: any) {
        if (data.artists && data.artists[0]) {
            if (data.artists[0].strArtistThumb) {
                fullArtist.iconLarge = data.artists[0].strArtistThumb;
                fullArtist.iconSmall = data.artists[0].strArtistThumb;
            }
            else if (data.artists[0].strArtistFanart) {
                fullArtist.iconLarge = data.artists[0].strArtistFanart;
                fullArtist.iconSmall = data.artists[0].strArtistFanart;
            }
        }
    }

    private getUrlForMbIdSearch(mbId: string) {
        mbId = encodeURIComponent(mbId);
        return this.apiURL + this.apiKey + this.byMbId + mbId;
    }

    private getUrlForNameSearch(name: string) {
        name = encodeURIComponent(name);
        return this.apiURL + this.apiKey + this.byName + name;
    }

}