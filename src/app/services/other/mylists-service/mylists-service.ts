import { Injectable } from '@angular/core';
import { StorageServiceProvider } from '../storage-service/storage-service';
import { MyLists } from '../../../models/list/my-lists';
import { CustomList } from '../../../models/list/custom-list';
import { BasicArtist, ProviderId } from '../../../models/artist/basic-artist';
import { FullArtist } from '../../../models/artist/full-artist';
import { LocalServiceProvider } from '../../main/local-service/local-service';
import { ExportedLists } from '../../../models/list/exported-lists';
import { File, FileEntry } from '@ionic-native/file/ngx';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { FileChooser } from '@ionic-native/file-chooser/ngx';
import { FilePath } from '@ionic-native/file-path/ngx';
import { AllServices } from '../../../models/other/all-services';
import { UtilsServiceProvider } from '../utils-service/utils-service';

/*
  Generated class for the MylistsServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class MylistsServiceProvider {

  private static EXPORT_DIR: string = "exports";
  private static EXPORT_EXTENSION: string = "smexport";

  private myLists: MyLists;

  constructor(private storageService: StorageServiceProvider, private utilsService: UtilsServiceProvider, private file: File,
    private socialSharing: SocialSharing, private fileChooser: FileChooser, private filePath: FilePath) {
    AllServices.myListsService = this;
  }

  public getMyLists(): Promise<MyLists> {
    return new Promise(resolve => {
      if (this.myLists) {
        resolve(this.myLists);
      }
      else {
        this.storageService.getSettingsCache().then(settingsCache => {
          if (!this.myLists) {
            if (!settingsCache || !settingsCache.myLists) {
              var myLists = new MyLists(undefined);
              this.myLists = myLists;
              // this.storageService.setMyLists(this.myLists);
            }
            else {
              this.myLists = settingsCache.myLists;
            }
          }
          resolve(this.myLists);
        })
      }
    });
  }

  public addRemoveFav(basicArtist: BasicArtist) {
    if (!this.myLists.favList.listArtists.find(artist => artist.artist == basicArtist.artist)) {
      var listArtist: BasicArtist = new BasicArtist(basicArtist.artist, basicArtist.weight, basicArtist.id);
      this.myLists.favList.addToList(listArtist);
      if (this.myLists.banList.listArtists.find(artist => artist.artist == basicArtist.artist)) {
        this.myLists.banList.removeFromList(basicArtist.artist);
      }
      this.storageService.setMyLists(this.myLists);
    }
    else {
      this.myLists.favList.removeFromList(basicArtist.artist);
      this.storageService.setMyLists(this.myLists);
    }
  }

  public addRemoveBan(basicArtist: BasicArtist) {
    if (!this.myLists.banList.listArtists.find(artist => artist.artist == basicArtist.artist)) {
      var listArtist: BasicArtist = new BasicArtist(basicArtist.artist, basicArtist.weight, basicArtist.id);
      this.myLists.banList.addToList(listArtist);
      if (this.myLists.favList.listArtists.find(artist => artist.artist == basicArtist.artist)) {
        this.myLists.favList.removeFromList(basicArtist.artist);
      }
      this.storageService.setMyLists(this.myLists);
    }
    else {
      this.myLists.banList.removeFromList(basicArtist.artist);
      this.storageService.setMyLists(this.myLists);
    }
  }

  public addArtistToNewList(customList: CustomList, basicArtist: BasicArtist) {
    if (!customList.listArtists.find(artist => artist.artist == basicArtist.artist)) {
      customList.addToList(new BasicArtist(basicArtist.artist, basicArtist.weight, basicArtist.id));
    }
    if (!this.myLists.customLists.find(customListB => customListB.listName == customList.listName)) {
      this.myLists.customLists.push(customList);
    }
    this.storageService.setMyLists(this.myLists);
  }

  public addArtistsToNewList(customList: CustomList, basicArtistList: Array<BasicArtist>) {
    basicArtistList.forEach(basicArtist => {
      if (!customList.listArtists.find(artist => artist.artist == basicArtist.artist)) {
        customList.addToList(new BasicArtist(basicArtist.artist, basicArtist.weight, basicArtist.id));
      }
    });
    if (!this.myLists.customLists.find(customListB => customListB.listName == customList.listName)) {
      this.myLists.customLists.push(customList);
    }
    this.storageService.setMyLists(this.myLists);
  }

  public addRemoveArtistFromLists(customLists: Array<CustomList>, basicArtist: BasicArtist) {
    this.myLists.customLists.forEach(customList => {
      //daca lista este bifata de utilizator  
      if (customLists.find(customListB => customListB.listName == customList.listName)) {
        //daca artistul nu exista in lista
        if (!customList.listArtists.find(artist => artist.artist == basicArtist.artist)) {
          //el trebuie adaugat
          customList.addToList(new BasicArtist(basicArtist.artist, basicArtist.weight, basicArtist.id));
        }
      }
      //daca lista nu este bifata de utilizator
      else {
        //daca artistul exista in lista
        if (customList.listArtists.find(artist => artist.artist == basicArtist.artist)) {
          //el trebuie eliminat
          customList.removeFromList(basicArtist.artist);
        }
      }

    });
    this.storageService.setMyLists(this.myLists);
  }

  public addArtistsToLists(customLists: Array<CustomList>, basicArtistList: Array<BasicArtist>) {
    customLists.forEach(customList => {
      basicArtistList.forEach(basicArtist => {
        if (!customList.listArtists.find(artist => artist.artist == basicArtist.artist)) {
          customList.addToList(new BasicArtist(basicArtist.artist, basicArtist.weight, basicArtist.id));
        }
      });
    });
    this.storageService.setMyLists(this.myLists);
  }

  public removeArtistsFromList(customList: CustomList, selectedArtists: Array<string>) {
    var listArtists = customList.listArtists.filter((listArtist: BasicArtist) => selectedArtists.indexOf(listArtist.artist) == -1);
    customList.setList(listArtists);
    this.storageService.setMyLists(this.myLists);
  }

  public addRemovePartOfLibrary(customList: CustomList) {
    customList.partOfLibrary = !customList.partOfLibrary;
    this.storageService.setMyLists(this.myLists);
  }

  public renameList(customList: CustomList, listName: string) {
    customList.listName = listName;
    this.storageService.setMyLists(this.myLists);
    return customList;
  }

  public deleteList(customList: CustomList) {
    this.myLists.customLists = this.myLists.customLists.filter(customList2 => customList2.listName != customList.listName);
    this.storageService.setMyLists(this.myLists);
  }

  public addOrReplaceLists(customLists: Array<CustomList>) {
    if (customLists.length == 0) return;
    customLists.forEach(customList => {
      if (customList.listType === 'fav') {
        this.myLists.favList = customList;
      }
      else if (customList.listType === 'ban') {
        this.myLists.banList = customList;
      }
      else {
        this.myLists.customLists = this.myLists.customLists.filter(m => m.listName !== customList.listName);
        this.myLists.customLists.push(customList);
      }
    })
    this.storageService.setMyLists(this.myLists);
  }

  public setWeightToArtists(customList: CustomList, selectedArtists: Array<string>, weight: number) {
    var listArtists = customList.listArtists.map((listArtist: BasicArtist) => {
      if (selectedArtists.indexOf(listArtist.artist) !== -1) {
        listArtist.weight = weight;
      }
      return listArtist;
    });
    customList.setList(listArtists);
    this.storageService.setMyLists(this.myLists);
  }

  public share(customLists: Array<CustomList>, exportName: string) {
    return new Promise((resolve, error) => {
      var exportedLists: ExportedLists = new ExportedLists();
      exportedLists.customLists.push(...customLists);
      this.file.checkDir(this.file.dataDirectory, MylistsServiceProvider.EXPORT_DIR)
        .then(result => {
          this.utilsService.createFileExport(exportName, exportedLists, MylistsServiceProvider.EXPORT_EXTENSION, MylistsServiceProvider.EXPORT_DIR)
            .then((fileEntry: FileEntry) => {
              this.socialSharing.share('Check this awesome SupriseMe list', 'SupriseMe list - ' + exportName, fileEntry.nativeURL)
                .then(result => resolve('Done'))
                .catch(err => error(err))
                .finally(() => {
                  setTimeout(() => {
                    fileEntry.remove(() => { })
                  }, 1000);
                })
            })
            .catch(err => error(err));
        })
        .catch(err => {
          this.file.createDir(this.file.dataDirectory, MylistsServiceProvider.EXPORT_DIR, false)
            .then(result => {
              this.utilsService.createFileExport(exportName, exportedLists, MylistsServiceProvider.EXPORT_EXTENSION, MylistsServiceProvider.EXPORT_DIR)
                .then((fileEntry: FileEntry) => {
                  this.socialSharing.share('Check this awesome SupriseMe list', 'SupriseMe list - ' + exportName, fileEntry.nativeURL)
                    .then(result => resolve('Done'))
                    .catch(err => error(err))
                    .finally(() => {
                      setTimeout(() => {
                        fileEntry.remove(() => { })
                      }, 1000);
                    })
                })
                .catch(err => error(err));
            })
            .catch(err => error(err));
        });
    });
  }

  public import() {
    return new Promise((resolve, error) => {
      this.fileChooser.open()
        .then(uri => {
          this.filePath.resolveNativePath(uri)
            .then(resolvedFilePath => {
              var resolvedPath = resolvedFilePath.substring(0, resolvedFilePath.lastIndexOf('/'));
              var resolvedFile = resolvedFilePath.substring(resolvedFilePath.lastIndexOf('/') + 1, resolvedFilePath.length);
              this.file.readAsText(resolvedPath, resolvedFile)
                .then(content => {
                  var exportedLists: ExportedLists = JSON.parse(content) as ExportedLists;
                  resolve(exportedLists);
                })
                .catch(err => error(error));
            })
            .catch(err => error(error));
        })
        .catch(err => error(error));
    });
  }

  public saveMyLists() {
    this.storageService.setMyLists(this.myLists);
  }
}
