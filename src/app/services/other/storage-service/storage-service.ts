import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { MyLists } from '../../../models/list/my-lists';
import { ProviderSettings } from '../../../models/providers/provider-settings';
import { CloudSettings } from '@ionic-native/cloud-settings/ngx';
import { Platform } from '@ionic/angular';
import { HistoryResult } from '../../../models/params/queued-params';
import { MusicApp } from '../../../models/other/music-app';
import { DiscoArtists } from '../../../models/other/disco-artists';
import { SearchTemplate } from '../../../models/params/search-template';

/*
  Generated class for the SettingsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class StorageServiceProvider {

  public settingsCache: SettingsCache;
  public discoArtists: DiscoArtists;

  constructor(private storage: Storage, private cloudSettings: CloudSettings, private platform: Platform) {
    this.getSettingsCache();
  }

  public getSettingsCache(): Promise<SettingsCache> {
    return new Promise(resolve => {
      if (this.settingsCache) {
        resolve(this.settingsCache);
      }
      else {
        this.getSettingsFromStorage().then(storageSettings => {
          this.getSettingsFromCloud().then(cloudSettings => {
            if (!this.settingsCache) {
              this.saveLatestLocally(storageSettings, cloudSettings);
            }
            resolve(this.settingsCache);
          });
        })
      }
    })
  }

  private getSettingsFromStorage(): Promise<SettingsCache> {
    return new Promise(resolve => {
      this.storage.get("settings.all").then((settingsCache: SettingsCache) => {
        if (settingsCache) {
          this.setPrototypeInCacheAndVersionDifferences(settingsCache);
          resolve(settingsCache);
        }
        else {
          resolve(new SettingsCache());
        }
      })
    });
  }

  private getSettingsFromCloud(): Promise<SettingsCache> {
    return new Promise(resolve => {
      if (!this.platform.is("cordova")) {
        resolve(undefined);
      }
      else {
        this.cloudSettings.exists()
          .then(exists => {
            if (exists) {
              this.cloudSettings.load()
                .then((settings: any) => { this.setPrototypeInCacheAndVersionDifferences(settings); resolve(settings); })
                .catch((error: any) => { console.error(error); resolve(undefined); });
            }
            else {
              resolve(undefined);
            }
          })
          .catch((error: any) => { console.error(error); resolve(undefined); });
      }
    });
  }

  private setPrototypeInCacheAndVersionDifferences(settingsCache: SettingsCache) {
    settingsCache.myLists = new MyLists(settingsCache.myLists);
    if (settingsCache.lastFmProviderSettings) { settingsCache.lastFmProviderSettings = Object.setPrototypeOf(settingsCache.lastFmProviderSettings, ProviderSettings.prototype); }
    if (settingsCache.spotifyProviderSettings) { settingsCache.spotifyProviderSettings = Object.setPrototypeOf(settingsCache.spotifyProviderSettings, ProviderSettings.prototype); }
    if (settingsCache.localProviderSettings) { settingsCache.localProviderSettings = Object.setPrototypeOf(settingsCache.localProviderSettings, ProviderSettings.prototype); }
    if (settingsCache.deezerProviderSettings) { settingsCache.deezerProviderSettings = Object.setPrototypeOf(settingsCache.deezerProviderSettings, ProviderSettings.prototype); }
    if (settingsCache.queueHistoryArray) {
      settingsCache.queueHistoryArray.forEach(h => { Object.setPrototypeOf(h, HistoryResult.prototype); h.setPrototypes(); });
    }
    if (settingsCache.musicApps) {
      settingsCache.musicApps.forEach(m => { Object.setPrototypeOf(m, MusicApp.prototype); m.setPrototypes(); });
    }
    if (!settingsCache.discographyFilters) { settingsCache.discographyFilters = new Array(); }
    if (!settingsCache.searchTemplates) { settingsCache.searchTemplates = new Array(); }
    else {
      settingsCache.searchTemplates.forEach(t => { Object.setPrototypeOf(t, SearchTemplate.prototype); t.setPrototypes(); });
    }
  }

  public setEnabledProviders(providers: Array<string>) {
    this.settingsCache.enabledProviders = providers;
    this.saveSettingsToStorage();
  }

  public setLibraryProviders(providers: Array<string>) {
    this.settingsCache.libraryProviders = providers;
    this.saveSettingsToStorage();
  }

  public setSourceProviders(providers: Array<string>) {
    this.settingsCache.sourceProviders = providers;
    this.saveSettingsToStorage();
  }

  public setSimilarityProviders(providers: Array<string>) {
    this.settingsCache.similarityProviders = providers;
    this.saveSettingsToStorage();
  }

  public setInfoProvider(provider: string) {
    this.settingsCache.infoProvider = provider;
    this.saveSettingsToStorage();
  }

  public setScrobbleProvider(provider: string) {
    this.settingsCache.scrobbleProvider = provider;
    this.saveSettingsToStorage();
  }

  public setListeningProvider(provider: string) {
    this.settingsCache.listeningProvider = provider;
    this.saveSettingsToStorage();
  }

  public setDiscographyProvider(provider: string) {
    this.settingsCache.discographyProvider = provider;
    this.saveSettingsToStorage();
  }

  public setProviderSettings(provider: string, settings: ProviderSettings) {
    switch (provider) {
      case "Last.fm": this.settingsCache.lastFmProviderSettings = settings; break;
      case "Spotify": this.settingsCache.spotifyProviderSettings = settings; break;
      case "Local": this.settingsCache.localProviderSettings = settings; break;
      case "Deezer": this.settingsCache.deezerProviderSettings = settings; break;
    }
    this.saveSettingsToStorage();
  }

  public setMyLists(myLists: MyLists) {
    this.settingsCache.myLists = myLists;
    this.saveSettingsToStorage();
  }

  public setAppRanBefore() {
    this.settingsCache.appRanBefore = 1;
    this.saveSettingsToStorage();
  }

  public setMusicApps(musicApps: Array<MusicApp>) {
    this.settingsCache.musicApps = musicApps;
    this.saveSettingsToStorage();
  }

  public setKeepAwake(type: string, keepAwake: boolean) {
    switch (type) {
      case "loading": this.settingsCache.keepAwakeLoading = keepAwake; break;
      case "listening": this.settingsCache.keepAwakeListening = keepAwake; break;
    }
    this.saveSettingsToStorage();
  }

  public setQueueHistory(queueHistoryArray: Array<HistoryResult>) {
    this.settingsCache.queueHistoryArray = queueHistoryArray;
    this.saveSettingsToStorage();
  }

  public setMaxQueueHistory(maxQueueHistory: number) {
    this.settingsCache.maxQueueHistory = maxQueueHistory;
    this.saveSettingsToStorage();
  }

  public setDiscographyFilters(filters: Array<string>) {
    this.settingsCache.discographyFilters = filters;
    this.saveSettingsToStorage();
  }

  public saveSearchTemplate(searchTemplate: SearchTemplate) {
    this.settingsCache.searchTemplates.push(searchTemplate);
    this.saveSettingsToStorage();
  }  

  public deleteSearchTemplate(searchTemplate: SearchTemplate) {
    this.settingsCache.searchTemplates = this.settingsCache.searchTemplates.filter(s => s.name !== searchTemplate.name);
    this.saveSettingsToStorage();
  }    

  public clearStorage() {
    this.storage.clear();
  }

  public saveSettingsToStorage() {
    this.settingsCache.timestamp = new Date().getTime();
    this.storage.set("settings.all", this.settingsCache);
    if (this.platform.is("cordova")) {
      this.cloudSettings.save(this.settingsCache, true)
        .catch((error: any) => console.error(error));
    }
  }

  saveLatestLocally(storageSettings: SettingsCache, cloudSettings: SettingsCache) {
    if (!cloudSettings) {
      this.settingsCache = storageSettings;
    }
    else if (!storageSettings.timestamp) {
      console.log("Restored cloudSettings");
      this.settingsCache = cloudSettings;
    }
    else {
      if (Number(cloudSettings.timestamp) > Number(storageSettings.timestamp)) {
        console.log("Restored cloudSettings");
        this.settingsCache = cloudSettings;
      }
      else {
        this.settingsCache = storageSettings;
      }
    }
  }

  public async getDiscoArtists(): Promise<DiscoArtists> {
    if (!this.discoArtists) {
      var discoArtists: DiscoArtists = await this.storage.get("disco.artists");
      if (discoArtists) {
        discoArtists = Object.setPrototypeOf(discoArtists, DiscoArtists.prototype);
        discoArtists.setPrototypes();
        this.discoArtists = discoArtists;
      }
      else {
        this.discoArtists = new DiscoArtists(new Array());
      }
    }
    return this.discoArtists;
  }

  public saveDiscoArtists() {
    this.storage.set("disco.artists", this.discoArtists);
  }

}

export class SettingsCache {

  public timestamp: number;

  public enabledProviders: Array<string>;
  public libraryProviders: Array<string>;
  public sourceProviders: Array<string>;
  public similarityProviders: Array<string>;
  public infoProvider: string;
  public scrobbleProvider: string;
  public listeningProvider: string;
  public discographyProvider: string;

  public keepAwakeListening: boolean = true;
  public keepAwakeLoading: boolean = true;

  public lastFmProviderSettings: ProviderSettings;
  public spotifyProviderSettings: ProviderSettings;
  public localProviderSettings: ProviderSettings;
  public deezerProviderSettings: ProviderSettings;

  public myLists: MyLists;
  public appRanBefore: number;
  public musicApps: Array<MusicApp>;

  public queueHistoryArray: Array<HistoryResult>;
  public maxQueueHistory: number = 10;
  public discographyFilters: Array<string> = new Array();
  public searchTemplates: Array<SearchTemplate> = new Array();

}
