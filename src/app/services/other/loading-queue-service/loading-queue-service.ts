import { Injectable } from '@angular/core';
import { SearchParams } from '../../../models/params/search-params';
import { GenerateParams } from '../../../models/params/generate-params';
import { FullArtist } from '../../../models/artist/full-artist';
import { SearchServiceProvider } from '../../main/search-service/search-service';
import { ProviderService } from '../provider-service/provider-service';
import { GenerateResult } from '../../../models/other/generate-result';
import { LoadingServiceProvider } from '../loading-service/loading-service';
import { DatePipe } from '@angular/common';
import { StorageServiceProvider } from '../storage-service/storage-service';
import { QueuedParams, HistoryResult } from '../../../models/params/queued-params';
import { DiscographyServiceProvider } from '../discography-service/discography-service';
import { RefreshParams } from '../../../models/params/refresh-params';
import { RefreshResult } from '../../../models/other/refresh-result';

@Injectable()
export class LoadingQueueServiceProvider {

  public queueParamsArray: Array<QueuedParams> = new Array();
  public queueHistoryArray: Array<HistoryResult> = new Array();

  constructor(private searchProvider: SearchServiceProvider, private providerService: ProviderService, private loadingService: LoadingServiceProvider,
    private storageService: StorageServiceProvider, private discographyService: DiscographyServiceProvider) {
    this.storageService.getSettingsCache().then(settingsCache => {
      if (settingsCache.queueHistoryArray) {
        this.queueHistoryArray = settingsCache.queueHistoryArray;
      }
    })
  }

  public queueSearch(searchParams: SearchParams): Promise<Array<FullArtist>> {
    return new Promise((resolve, error) => {
      this.queueParamsArray.push(new QueuedParams(QueuedParams.TYPE_SEARCH, searchParams, resolve, error));
      if (this.queueParamsArray.length == 1) {
        this.processNextParam();
      }
    })
  }

  public queueGenerate(generateParams: GenerateParams): Promise<GenerateResult> {
    return new Promise((resolve, error) => {
      this.queueParamsArray.push(new QueuedParams(QueuedParams.TYPE_GENERATE, generateParams, resolve, error));
      if (this.queueParamsArray.length == 1) {
        this.processNextParam();
      }
    })
  }

  public queueRefreshDisco(refreshParams: RefreshParams): Promise<RefreshResult> {
    return new Promise((resolve, error) => {
      this.queueParamsArray.push(new QueuedParams(QueuedParams.TYPE_REFRESH, refreshParams, resolve, error));
      if (this.queueParamsArray.length == 1) {
        this.processNextParam();
      }
    })
  }

  public getLastResult(): HistoryResult {
    if (this.queueHistoryArray.length == 0) return undefined;
    return this.queueHistoryArray[this.queueHistoryArray.length - 1];
  }

  public getLastResults(): Array<HistoryResult> {
    return this.queueHistoryArray;
  }

  public getCurrentParams(): QueuedParams {
    if (this.queueParamsArray.length == 0) return undefined;
    return this.queueParamsArray[0];
  }

  public getFutureParams(): Array<QueuedParams> {
    if (this.queueParamsArray.length <= 1) return new Array<QueuedParams>();
    return this.queueParamsArray.slice(1);
  }

  public cancelCurrent() {
    if (this.queueParamsArray.length == 1) {
      this.loadingService.resetLoad();
      this.queueParamsArray.pop();
    }
    else {
      this.loadingService.resetLoad();
      this.removeCurrentParam();
    }
  }

  public cancelQueued(queuedParams: QueuedParams) {
    var timestamp = this.getTimestamp(queuedParams);
    this.queueParamsArray = this.queueParamsArray.filter(q => this.getTimestamp(q) != timestamp);
  }

  public removeHistory(historyResult: HistoryResult) {
    var timestamp = this.getTimestamp(historyResult.queueParams);
    this.queueHistoryArray = this.queueHistoryArray.filter(h => this.getTimestamp(h.queueParams) != timestamp);
    this.storageService.setQueueHistory(this.queueHistoryArray);
  }

  public removeAllHistory() {
    if (this.queueHistoryArray.length > 0) {
      this.queueHistoryArray = new Array();
      this.storageService.setQueueHistory(this.queueHistoryArray);
    }
  }

  removeAllQueued() {
    if (this.queueParamsArray.length > 0) {
      this.queueParamsArray = this.queueParamsArray.slice(0, 1);
    }
  }

  public removeAll() {
    if (this.queueParamsArray.length > 0) {
      this.loadingService.resetLoad();
      this.queueParamsArray = new Array();
    }
    this.removeAllHistory();
  }

  public getInfoMessage(params: QueuedParams, datepipe: DatePipe): string {
    var message;
    switch (params.type) {
      case QueuedParams.TYPE_SEARCH: message = params.searchParams.getSearchInfoMessage(datepipe); break;
      case QueuedParams.TYPE_GENERATE: message = params.generateParams.getGenerateInfoMessage(); break;
      case QueuedParams.TYPE_REFRESH: message = params.refreshParams.getGenerateInfoMessage(); break;
      default: message = "Unknown search type";
    }
    return message;
  }

  private processNextParam() {
    var queuedParams: QueuedParams = this.queueParamsArray[0];
    this.callService(queuedParams)
      .then(r => {
        queuedParams.resolve(r);
        this.moveToHistory(r);
      })
      .catch(e => {
        queuedParams.error(e);
        this.queueParamsArray.shift();
      })
      .finally(() => {
        if (this.queueParamsArray.length > 0) {
          this.processNextParam();
        }
      })
  }

  private callService(queuedParams: QueuedParams) {
    return new Promise((resolve, error) => {
      switch (queuedParams.type) {
        case QueuedParams.TYPE_SEARCH:
          this.searchProvider.searchForRec(queuedParams.searchParams)
            .then(recArtists => { this.loadingService.resetLoad(); resolve(recArtists); })
            .catch(e => { this.loadingService.resetLoad(); error(e); });
          break;
        case QueuedParams.TYPE_GENERATE:
          this.providerService.getServiceProvider(queuedParams.generateParams.provider).generatePlaylist(queuedParams.generateParams)
            .then(generateResult => { this.loadingService.resetLoad(); resolve(generateResult); })
            .catch(e => { this.loadingService.resetLoad(); error(e); });
          break;
        case QueuedParams.TYPE_REFRESH:
          this.discographyService.refreshAll(queuedParams.refreshParams)
            .then(refreshResult => { this.loadingService.resetLoad(); resolve(refreshResult); })
            .catch(e => { this.loadingService.resetLoad(); error(e); });
          break;
      }
    });
  }

  private moveToHistory(results: any) {
    var params = this.queueParamsArray.shift();
    if (this.queueHistoryArray.length == this.storageService.settingsCache.maxQueueHistory) {
      this.queueHistoryArray.shift();
    }
    this.queueHistoryArray.push(new HistoryResult(params.type, params, results));
    this.storageService.setQueueHistory(this.queueHistoryArray);
  }

  private removeCurrentParam() {
    this.queueParamsArray.shift();
    if (this.queueParamsArray.length > 0) {
      this.processNextParam();
    }
  }

  private getTimestamp(queuedParams: QueuedParams) {
    var timestamp: number;
    switch (queuedParams.type) {
      case QueuedParams.TYPE_SEARCH: timestamp = queuedParams.searchParams.genericParams.timestamp; break;
      case QueuedParams.TYPE_GENERATE: timestamp = queuedParams.generateParams.timestamp; break;
      case QueuedParams.TYPE_REFRESH: timestamp = queuedParams.refreshParams.timestamp; break;
      default: timestamp = -1;
    }
    return timestamp;
  }

}