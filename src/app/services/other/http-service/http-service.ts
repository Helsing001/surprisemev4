import { Injectable } from '@angular/core';
import { HTTP } from '@ionic-native/http/ngx';
import { Http } from '@angular/http';
import { Platform } from '@ionic/angular';
import { map } from 'rxjs/operators';

@Injectable()
export class HttpServiceProvider {

  constructor(public httpCordova: HTTP, public httpClassic: Http, private platform: Platform) {
    
  }

  public httpGet(url: string): Promise<any> {
    return new Promise((resolve, error) => {
      if (this.platform.is("cordova")) {
        this.httpCordova.get(url, {}, {})
          .then(response => {
            resolve(JSON.parse(response.data));
          })
          .catch(e => error(e));
      }
      else {
        this.httpClassic.get(url)
          .pipe(map(res => res.json())).toPromise()
          .then(response => {
            resolve(response);
          })
          .catch(e => error(e));
      }
    })
  }

}