import { Injectable } from '@angular/core';
import { ProviderSettings } from '../../../models/providers/provider-settings';
import { GenericService } from '../../../models/generic/generic-service';
import { StorageServiceProvider, SettingsCache } from '../storage-service/storage-service';
import { orderBy } from 'lodash';
import { LocalServiceProvider } from '../../main/local-service/local-service';
import { LastFmServiceProvider } from '../../main/last-fm-service/last-fm-service';
import { SpotifyServiceProvider } from '../../main/spotify-service/spotify-service';
import { SearchParams, LastFmParams, SpotifyParams, LocalParams } from '../../../models/params/search-params';
import { CustomList } from '../../../models/list/custom-list';
import { AllServices } from '../../../models/other/all-services';
import { ProviderOperations } from '../../../models/providers/provider-operations';
import { MusicApp } from '../../../models/other/music-app';
import { LoadingController } from '@ionic/angular';
import { DeezerServiceProvider } from '../../main/deezer-service/deezer-service';

/*
  Generated class for the ProviderServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ProviderService {

  public static PROVIDER_SERVICE: ProviderService;

  private DEFAULT_SIMILARITY_PROVIDER = "Last.fm";
  private DEFAULT_INFO_PROVIDER = "Last.fm";

  private DEFAULT_SCROBBLE_PROVIDER = "Local";
  private PREFERRED_SCROBBLE_PROVIDER = "Last.fm"

  private DEFAULT_LISTENING_PROVIDER = undefined;
  private PREFERRED_LISTENING_PROVIDER = "Last.fm"

  private DEFAULT_DISCOGRAPHY_PROVIDER = "Last.fm"  
  private PREFERRED_DISCOGRAPHY_PROVIDER = "Deezer"

  private enabledProviders: Array<string> = ['Local', 'Last.fm', 'Spotify', 'Deezer'];
  private libraryProviders: Array<string> = ['Local', 'Last.fm', 'Spotify', 'Deezer'];
  private sourceProviders: Array<string> = ['Local', 'Last.fm', 'Spotify', 'Deezer'];
  private similarityProviders: Array<string> = ['Last.fm', 'Spotify', 'Deezer'];
  private infoProvider: string = 'Last.fm';
  private scrobbleProvider: string = 'Local';
  private listeningProvider: string = undefined;
  private discographyProvider: string = 'Last.fm';

  private musicApps: Array<MusicApp> = new Array();

  private isReady: boolean = false;
  private readySubs: Array<any> = new Array();

  constructor(public localServiceProvider: LocalServiceProvider, public lastFmServiceProvider: LastFmServiceProvider,
    public spotifyServiceProvider: SpotifyServiceProvider, public deezerServiceProvider: DeezerServiceProvider,
    public storageService: StorageServiceProvider, private loadingController: LoadingController) {
    storageService.getSettingsCache().then(settingsCache => {
      if (settingsCache && settingsCache.enabledProviders) { this.enabledProviders = settingsCache.enabledProviders; } else { this.enabledProviders = this.getDefaultEnabledProviders(); }
      if (settingsCache && settingsCache.libraryProviders) { this.libraryProviders = settingsCache.libraryProviders; } else { this.libraryProviders = this.getDefaultProvidersFor(ProviderOperations.LIBRARY); }
      if (settingsCache && settingsCache.sourceProviders) { this.sourceProviders = settingsCache.sourceProviders; } else { this.sourceProviders = this.getDefaultProvidersFor(ProviderOperations.SOURCE); }
      if (settingsCache && settingsCache.similarityProviders) { this.similarityProviders = settingsCache.similarityProviders; } else { this.similarityProviders = this.getDefaultProvidersFor(ProviderOperations.SIMILARITY); }
      if (settingsCache && settingsCache.infoProvider) { this.infoProvider = settingsCache.infoProvider; } else { this.infoProvider = this.DEFAULT_INFO_PROVIDER }
      if (settingsCache && settingsCache.scrobbleProvider) { this.scrobbleProvider = settingsCache.scrobbleProvider; } else { this.scrobbleProvider = this.DEFAULT_SCROBBLE_PROVIDER }
      if (settingsCache && settingsCache.listeningProvider) { this.listeningProvider = settingsCache.listeningProvider; } else { this.listeningProvider = this.DEFAULT_LISTENING_PROVIDER }
      if (settingsCache && settingsCache.discographyProvider) { this.discographyProvider = settingsCache.discographyProvider; } else { this.discographyProvider = this.DEFAULT_DISCOGRAPHY_PROVIDER }
      if (settingsCache && settingsCache.musicApps) { this.musicApps = settingsCache.musicApps; }
      this.notifyReady();
    })
    AllServices.providerService = this;
  }

  public whenReady(): Promise<any> {
    return new Promise(resolve => {
      if (this.isReady) {
        resolve();
      }
      else {
        this.readySubs.push(resolve);
      }
    })
  }

  public getDefaultEnabledProviders(): Array<string> {
    var defaultProviders: Array<string> = new Array();
    this.getAllProviderSettings().forEach(providerSettings => {
      if (providerSettings.alwaysEnabled) {
        defaultProviders.push(providerSettings.name);
      }
      else {
        var defaultOperations = this.getServiceProvider(providerSettings.name).getDefaultOperationsWithoutAuth();
        if (defaultOperations && defaultOperations.length > 0) {
          defaultProviders.push(providerSettings.name);
        }
      }
    });
    return defaultProviders;
  }

  public getDefaultProvidersFor(operation: string): Array<string> {
    var defaultProviders: Array<string> = new Array();
    this.getAllProviderSettings().forEach(providerSettings => {
      var defaultOperations = this.getServiceProvider(providerSettings.name).getDefaultOperationsWithoutAuth();
      if (defaultOperations && defaultOperations.includes(operation)) {
        defaultProviders.push(providerSettings.name);
      }
    });
    return defaultProviders;
  }

  public getProviderSettings(provider: string) {
    return this.getServiceProvider(provider).getProviderSettings();
  }

  public getAllProviderSettings(): Array<ProviderSettings> {
    var allProviderSettings: Array<ProviderSettings> = new Array();
    this.getAllProviders().forEach(provider => {
      allProviderSettings.push(provider.getProviderSettings());
    });
    allProviderSettings = orderBy(allProviderSettings, 'orderId', 'asc');
    return allProviderSettings;
  }

  public getEnabledProviderSettings(): Array<ProviderSettings> {
    var enabledProviderSettings: Array<ProviderSettings> = new Array();
    this.getAllProviderSettings().forEach(providerSettings => {
      if (this.isProviderEnabled(providerSettings.name)) {
        enabledProviderSettings.push(providerSettings);
      }
    });
    return enabledProviderSettings;
  }

  public isProviderEnabled(provider: string): boolean {
    return this.enabledProviders.indexOf(provider) != -1;
  }

  public setEnabledProviders(newEnabledProviders: Array<string>) {
    var tmpLibList: Array<string> = [];
    var tmpSrcList: Array<string> = [];
    var tmpSimList: Array<string> = [];
    newEnabledProviders.forEach(provider => {
      if (this.libraryProviders.indexOf(provider) != -1) {
        tmpLibList.push(provider);
      }
      if (this.sourceProviders.indexOf(provider) != -1) {
        tmpSrcList.push(provider);
      }
      if (this.similarityProviders.indexOf(provider) != -1) {
        tmpSimList.push(provider);
      }
    })

    this.libraryProviders = tmpLibList;
    this.storageService.settingsCache.libraryProviders = this.libraryProviders;

    this.sourceProviders = tmpSrcList;
    this.storageService.settingsCache.sourceProviders = this.sourceProviders;

    this.similarityProviders = tmpSimList;
    if (this.similarityProviders.length == 0) {
      this.similarityProviders.push(this.DEFAULT_SIMILARITY_PROVIDER);
    }
    this.storageService.settingsCache.similarityProviders = this.similarityProviders;

    if (newEnabledProviders.indexOf(this.infoProvider) == -1) {
      this.infoProvider = this.DEFAULT_INFO_PROVIDER;
    }
    this.storageService.settingsCache.infoProvider = this.infoProvider;

    if (newEnabledProviders.indexOf(this.scrobbleProvider) == -1) {
      this.scrobbleProvider = this.DEFAULT_SCROBBLE_PROVIDER;
    }
    this.storageService.settingsCache.scrobbleProvider = this.scrobbleProvider;

    if (newEnabledProviders.indexOf(this.listeningProvider) == -1) {
      this.listeningProvider = this.DEFAULT_LISTENING_PROVIDER;
    }
    this.storageService.settingsCache.listeningProvider = this.listeningProvider;

    if (newEnabledProviders.indexOf(this.discographyProvider) == -1) {
      this.discographyProvider = this.DEFAULT_DISCOGRAPHY_PROVIDER;
    }
    this.storageService.settingsCache.discographyProvider = this.discographyProvider;    

    let providersToAuth: Array<string> = new Array();
    this.getAllProviderSettings().forEach(providerSettings => {
      if (newEnabledProviders.indexOf(providerSettings.name) == -1) {
        if (this.isProviderEnabled(providerSettings.name)) {
          var serviceProvider = this.getServiceProvider(providerSettings.name);
          serviceProvider.forgetAuth();
          providerSettings.resetWeights(this.storageService);
        }
      }
      else {
        if (!this.isProviderEnabled(providerSettings.name) && !providerSettings.getUsername()) {
          providersToAuth.push(providerSettings.name);
        }
      }
    });

    if (providersToAuth.length > 0) {
      this.authProvidersInSeq(providersToAuth).catch(error => { console.log(error); });
    }

    this.enabledProviders = newEnabledProviders;
    this.storageService.settingsCache.enabledProviders = newEnabledProviders;
    this.storageService.saveSettingsToStorage();
  }

  private authProvidersInSeq(providers: Array<string>) {
    return new Promise((resolve, error) => {
      if (this.getServiceProvider(providers[0]).isLongAuth()) {
        const loading = this.loadingController.create({
          message: 'Please wait...'
        }).then(l => l.present().then(r => {
          this.callAuth(providers, true).then(r => resolve(r)).catch(e => error(e));
        }))
      }
      else {
        this.callAuth(providers, false).then(r => resolve(r)).catch(e => error(e));
      }
    });
  }

  private callAuth(providers: Array<string>, longAuth: boolean) {
    return new Promise((resolve, error) => {
      this.getServiceProvider(providers[0]).auth()
        .then(result => {
          if (longAuth) {
            this.loadingController.dismiss();
          }
          if (result) {
            this.enableDefaultSettings(providers[0], false);
          }
          else {
            this.enableDefaultSettings(providers[0], false, false);
          }
          if (providers.length > 1) {
            this.authProvidersInSeq(providers.slice(1)).then(r => resolve(1)).catch(e => error(e));
          }
          else {
            resolve(1);
          }
        })
        .catch(e => error(e));
    });
  }

  public getAllProvidersFor(operation: string): Array<ProviderSettings> {
    var operationProviderSettings: Array<ProviderSettings> = new Array();
    this.getAllProviderSettings().forEach(providerSettings => {
      var ps = providerSettings as any;
      if (this.isProviderEnabled(providerSettings.name) && providerSettings[operation]) {
        operationProviderSettings.push(providerSettings);
      }
    });
    return operationProviderSettings;
  }

  public getActiveProvidersFor(operation: string): Array<ProviderSettings> {
    var enabledProviders: Array<ProviderSettings> = new Array();
    this.getAllProvidersFor(operation).forEach(providerSettings => {
      if (this.isEnabledFor(providerSettings.name, operation)) {
        enabledProviders.push(providerSettings);
      }
    });
    return enabledProviders;
  }

  public getActiveProviderFor(operation: string): ProviderSettings {
    switch (operation) {
      case ProviderOperations.INFO: return this.getProviderSettings(this.infoProvider);
      case ProviderOperations.SCROBBLE: return this.getProviderSettings(this.scrobbleProvider);
      case ProviderOperations.LISTENING: return this.listeningProvider ? this.getProviderSettings(this.listeningProvider) : undefined;
      case ProviderOperations.DISCOGRAPHY: return this.getProviderSettings(this.discographyProvider);
    }
  }

  public isEnabledFor(provider: string, operation: string): boolean {
    switch (operation) {
      case ProviderOperations.LIBRARY: return this.libraryProviders.indexOf(provider) != -1;
      case ProviderOperations.SOURCE: return this.sourceProviders.indexOf(provider) != -1;
      case ProviderOperations.SIMILARITY: return this.similarityProviders.indexOf(provider) != -1;
      case ProviderOperations.INFO: return this.infoProvider == provider;
      case ProviderOperations.SCROBBLE: return this.scrobbleProvider == provider;
      case ProviderOperations.LISTENING: return this.listeningProvider == provider;
      case ProviderOperations.DISCOGRAPHY: return this.discographyProvider == provider;
    }
  }

  public setProvidersFor(providers: Array<string>, operation: string) {
    switch (operation) {
      case ProviderOperations.LIBRARY: this.libraryProviders = providers; this.storageService.setLibraryProviders(providers); break;
      case ProviderOperations.SOURCE: this.sourceProviders = providers; this.storageService.setSourceProviders(providers); break;
      case ProviderOperations.SIMILARITY: this.similarityProviders = providers; this.storageService.setSimilarityProviders(providers); break;
    }
  }

  public setProviderFor(provider: string, operation: string) {
    switch (operation) {
      case ProviderOperations.INFO: this.infoProvider = provider; this.storageService.setInfoProvider(provider); break;
      case ProviderOperations.SCROBBLE: this.scrobbleProvider = provider; this.storageService.setScrobbleProvider(provider); break;
      case ProviderOperations.LISTENING: this.listeningProvider = provider; this.storageService.setListeningProvider(provider); break;
      case ProviderOperations.DISCOGRAPHY: this.discographyProvider = provider; this.storageService.setDiscographyProvider(provider); break;
    }
  }

  public getAllLogInProviders(): Array<ProviderSettings> {
    var logInProviderSettings: Array<ProviderSettings> = new Array();
    this.getAllProviderSettings().forEach(providerSettings => {
      if (providerSettings && providerSettings.shouldLogIn) {
        logInProviderSettings.push(providerSettings);
      }
    });
    return logInProviderSettings;
  }

  public getMusicApps(): Array<MusicApp> {
    return this.musicApps;
  }

  public setMusicApps(musicApps: Array<MusicApp>) {
    this.musicApps = musicApps;
    this.storageService.setMusicApps(musicApps);
  }

  public isMusicApp(musicApp: MusicApp) {
    return this.musicApps.findIndex((m => m.appPackage === musicApp.appPackage)) !== -1;
  }

  public enableDefaultSettings(provider: string, activateEnabled: boolean = true, authed: boolean = true) {
    var defaultOperations: Array<string>;
    if (!authed) {
      defaultOperations = this.getServiceProvider(provider).getDefaultOperationsWithoutAuth();
    }
    if (activateEnabled && !this.enabledProviders.includes(provider)) {
      this.enabledProviders.push(provider);
      this.storageService.settingsCache.enabledProviders = this.enabledProviders;
    }
    if (this.getProviderSettings(provider).library && !this.libraryProviders.includes(provider)) {
      if (authed || (defaultOperations && defaultOperations.includes(ProviderOperations.LIBRARY))) {
        this.libraryProviders.push(provider);
        this.storageService.settingsCache.libraryProviders = this.libraryProviders;
      }
    }
    if (this.getProviderSettings(provider).source && !this.sourceProviders.includes(provider)) {
      if (authed || (defaultOperations && defaultOperations.includes(ProviderOperations.SOURCE))) {
        this.sourceProviders.push(provider);
        this.storageService.settingsCache.sourceProviders = this.sourceProviders;
      }
    }
    if (this.getProviderSettings(provider).similarity && !this.similarityProviders.includes(provider)) {
      if (authed || (defaultOperations && defaultOperations.includes(ProviderOperations.SIMILARITY))) {
        this.similarityProviders.push(provider);
        this.storageService.settingsCache.similarityProviders = this.similarityProviders;
      }
    }
    if (provider === this.PREFERRED_SCROBBLE_PROVIDER && (authed || (defaultOperations && defaultOperations.includes(ProviderOperations.SCROBBLE)))) {
      this.scrobbleProvider = provider;
      this.storageService.settingsCache.scrobbleProvider = this.scrobbleProvider;
    }
    if (provider === this.PREFERRED_LISTENING_PROVIDER && (authed || (defaultOperations && defaultOperations.includes(ProviderOperations.LISTENING)))) {
      this.listeningProvider = provider;
      this.storageService.settingsCache.listeningProvider = this.listeningProvider;
    }
    if (provider === this.PREFERRED_DISCOGRAPHY_PROVIDER && (authed || (defaultOperations && defaultOperations.includes(ProviderOperations.DISCOGRAPHY)))) {
      this.discographyProvider = provider;
      this.storageService.settingsCache.discographyProvider = this.discographyProvider;
    }    
    this.storageService.saveSettingsToStorage();
  }

  public disableAuthOperations(provider: string) {
    var defaultOperations = this.getServiceProvider(provider).getDefaultOperationsWithoutAuth();
    if (this.getProviderSettings(provider).library && this.libraryProviders.includes(provider) && (!defaultOperations || !defaultOperations.includes(ProviderOperations.LIBRARY))) {
      this.libraryProviders = this.libraryProviders.filter(e => e !== provider);
      this.storageService.settingsCache.libraryProviders = this.libraryProviders;
    }
    if (this.getProviderSettings(provider).source && this.sourceProviders.includes(provider) && (!defaultOperations || !defaultOperations.includes(ProviderOperations.SOURCE))) {
      this.sourceProviders = this.sourceProviders.filter(e => e !== provider);
      this.storageService.settingsCache.sourceProviders = this.sourceProviders;
    }
    if (this.getProviderSettings(provider).similarity && this.similarityProviders.includes(provider) && (!defaultOperations || !defaultOperations.includes(ProviderOperations.SIMILARITY))) {
      this.similarityProviders = this.similarityProviders.filter(e => e !== provider);
      if (this.similarityProviders.length == 0) {
        this.similarityProviders.push(this.DEFAULT_SIMILARITY_PROVIDER);
      }
      this.storageService.settingsCache.similarityProviders = this.similarityProviders;
    }
    if (this.infoProvider === provider && (!defaultOperations || !defaultOperations.includes(ProviderOperations.INFO))) {
      this.infoProvider = this.DEFAULT_INFO_PROVIDER;
      this.storageService.settingsCache.infoProvider = this.infoProvider;
    }
    if (this.scrobbleProvider === provider && (!defaultOperations || !defaultOperations.includes(ProviderOperations.SCROBBLE))) {
      this.scrobbleProvider = this.DEFAULT_SCROBBLE_PROVIDER;
      this.storageService.settingsCache.scrobbleProvider = this.scrobbleProvider;
    }
    if (this.listeningProvider === provider && (!defaultOperations || !defaultOperations.includes(ProviderOperations.LISTENING))) {
      this.listeningProvider = this.DEFAULT_LISTENING_PROVIDER;
      this.storageService.settingsCache.listeningProvider = this.listeningProvider;
    }
    if (this.discographyProvider === provider && (!defaultOperations || !defaultOperations.includes(ProviderOperations.DISCOGRAPHY))) {
      this.discographyProvider = this.DEFAULT_DISCOGRAPHY_PROVIDER;
      this.storageService.settingsCache.discographyProvider = this.discographyProvider;
    }    
    this.storageService.saveSettingsToStorage();
  }

  public getAllProviders(): Array<GenericService> {
    var providers: Array<GenericService> = new Array();
    providers.push(this.lastFmServiceProvider);
    providers.push(this.spotifyServiceProvider);
    providers.push(this.localServiceProvider);
    providers.push(this.deezerServiceProvider);
    return providers;
  }

  public getServiceProvider(provider: string): GenericService {
    switch (provider) {
      case LastFmServiceProvider.PROVIDER: return this.lastFmServiceProvider;
      case SpotifyServiceProvider.PROVIDER: return this.spotifyServiceProvider;
      case LocalServiceProvider.PROVIDER: return this.localServiceProvider;
      case DeezerServiceProvider.PROVIDER: return this.deezerServiceProvider;
    }
  }

  public getServiceParams(searchParams: SearchParams, provider: string) {
    switch (provider) {
      case LastFmServiceProvider.PROVIDER: return searchParams.lastFmParams;
      case SpotifyServiceProvider.PROVIDER: return searchParams.spotifyParams;
      case LocalServiceProvider.PROVIDER: return searchParams.localParams;
      case DeezerServiceProvider.PROVIDER: return searchParams.deezerParams;
    }
  }

  private notifyReady() {
    this.isReady = true;
    this.readySubs.forEach(resolve => resolve());
    this.readySubs = new Array();
  }

}
