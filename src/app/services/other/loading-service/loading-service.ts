import { Injectable } from '@angular/core';

/*
  Generated class for the SettingsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class LoadingServiceProvider {

    constructor() { }

    private currentEvents: Map<number, number> = new Map();
    private currentTotal: Map<number, number> = new Map();
    private currentProviders: Array<number> = new Array();

    private basePercent: number;
    private percentRange: number;

    public loadingPhase: string;
    public loadingPercent: number = 0;

    public minimized: boolean = true;

    public searchTimestamp: number = -1;

    public addToReferenceProgress(refValue: number, providerId: number, timestamp: number) {
        if (timestamp === this.searchTimestamp) {
            this.currentTotal.set(providerId, Number(this.currentTotal.get(providerId)) + Number(refValue));
            this.calculateLoadingPercent();
        }
    }

    public addToCurrentProgress(providerId: number, timestamp: number) {
        if (timestamp === this.searchTimestamp) {
            this.currentEvents.set(providerId, Number(this.currentEvents.get(providerId)) + Number(1));
            this.calculateLoadingPercent();
        }
    }

    public setLoadingPhase(loadingPhase: string, providerIds: Array<number>, basePercent: number, percentRange: number) {
        this.loadingPhase = loadingPhase;
        this.basePercent = basePercent;
        this.percentRange = percentRange;
        this.currentEvents = new Map();
        this.currentTotal = new Map();
        providerIds.forEach(id => {
            this.currentTotal.set(id, 0);
            this.currentEvents.set(id, 0);
        });
        this.currentProviders = providerIds;
        this.loadingPercent = basePercent;
    }

    public isLoadActive(timestamp: number) {
        return timestamp === undefined || timestamp === this.searchTimestamp;
    }

    public resetLoad(){
        this.searchTimestamp = -1;
        this.loadingPercent = 0;
    }

    private calculateLoadingPercent() {
        var currentPercent = this.basePercent;
        this.currentProviders.forEach(provider => {
            var total = this.currentTotal.get(provider) != 0 ? this.currentTotal.get(provider) : 1;
            currentPercent += Number.parseInt(Number(this.percentRange / this.currentProviders.length * this.currentEvents.get(provider) / total).toFixed(0));
        })
        this.loadingPercent = currentPercent;
    }

}
