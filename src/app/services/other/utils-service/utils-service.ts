import { Injectable } from '@angular/core';
import { File, FileEntry } from '@ionic-native/file/ngx';

@Injectable()
export class UtilsServiceProvider {

  constructor(private file: File) { }

  public shuffleArray(array) {
    for (let i = array.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [array[i], array[j]] = [array[j], array[i]];
    }
  }

  public async createFileExport(exportName: string, exportedLists: any, extension: string, dir: string): Promise<FileEntry> {
    var finalExportName = exportName + "." + extension;
    var jsonExport = JSON.stringify(exportedLists);
    var exportPath = this.file.dataDirectory + dir + "/";
    var beautyExportPath = exportPath.replace("file://", "") + finalExportName;
    return await this.file.writeFile(exportPath, finalExportName, jsonExport, { append: false, replace: true })
  }

}
