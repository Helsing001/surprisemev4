import { Injectable } from '@angular/core';
import { BasicArtist } from '../../../models/artist/basic-artist';
import { DiscoArtists } from '../../../models/other/disco-artists';
import { BasicDiscoArtist } from '../../../models/artist/basic-disco-artist';
import { StorageServiceProvider } from '../storage-service/storage-service';
import { ProviderOperations } from '../../../models/providers/provider-operations';
import { ProviderService } from '../provider-service/provider-service';
import { LoadingServiceProvider } from '../loading-service/loading-service';
import { RefreshParams } from '../../../models/params/refresh-params';
import { RefreshResult } from '../../../models/other/refresh-result';
import { ArtistAlbum } from '../../../models/album/artist-album';
import { File, FileEntry } from '@ionic-native/file/ngx';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { FileChooser } from '@ionic-native/file-chooser/ngx';
import { FilePath } from '@ionic-native/file-path/ngx';
import { UtilsServiceProvider } from '../utils-service/utils-service';
import { orderBy } from 'lodash';

@Injectable()
export class DiscographyServiceProvider {

  private static EXPORT_DIR: string = "exports";
  private static EXPORT_NAME: string = "followed";
  private static EXPORT_EXTENSION: string = "smexport";

  private discoArtists: DiscoArtists = new DiscoArtists(new Array());

  private initialized: boolean = false;
  private resolveUninit: Array<any> = new Array();

  constructor(private storageService: StorageServiceProvider, public providerService: ProviderService, public loadingService: LoadingServiceProvider,
    private utilsService: UtilsServiceProvider, private file: File, private socialSharing: SocialSharing, private fileChooser: FileChooser,
    private filePath: FilePath) {
    this.initialize();
  }

  public getDiscoArtists(): Promise<DiscoArtists> {
    return new Promise(resolve => {
      if (this.initialized) resolve(this.discoArtists);
      else this.resolveUninit.push(resolve);
    })
  }

  public findDiscoArtist(artist: BasicArtist): BasicDiscoArtist {
    return this.discoArtists.artists.find(discoArtist => {
      return this.isSameArtist(artist, discoArtist);
    });
  }

  public isArtistFollowed(artist: BasicArtist): boolean {
    return this.findDiscoArtist(artist) !== undefined
  }

  public followArtist(artist: BasicArtist) {
    var date = new Date();
    this.discoArtists.addDiscoArtist(new BasicDiscoArtist(artist, 0, date, undefined));
    this.storageService.saveDiscoArtists();
  }

  public unfollowArtist(artist: BasicArtist) {
    this.discoArtists.artists = this.discoArtists.artists.filter(discoArtist => {
      return !this.isSameArtist(artist, discoArtist);
    });
    this.storageService.saveDiscoArtists();
  }

  public updateLastListened(artist: BasicDiscoArtist, albums: Array<ArtistAlbum>, newLastListened: Date) {
    let changed = artist.update(newLastListened, albums)
    if(changed) this.storageService.saveDiscoArtists();
  }

  public async refresh(artist: BasicDiscoArtist): Promise<Array<ArtistAlbum>> {
    let discoProvider = this.providerService.getActiveProviderFor(ProviderOperations.DISCOGRAPHY);
    var artistAlbums =
      await this.providerService.getServiceProvider(discoProvider.name).getAllArtistAlbums(artist);
    let changed = artist.update(artist.lastListened, artistAlbums)
    if(changed) this.storageService.saveDiscoArtists();
    return artistAlbums;
  }

  public async refreshAll(refreshParams: RefreshParams): Promise<RefreshResult> {
    let artists = refreshParams.artists;
    let discoProvider = this.providerService.getActiveProviderFor(ProviderOperations.DISCOGRAPHY);
    let artistsWithNewAlbums = new Array();
    let notFoundArtists = new Array();

    let refreshTimestamp = new Date().getTime();
    this.loadingService.searchTimestamp = refreshTimestamp;
    this.loadingService.setLoadingPhase("Discos", [discoProvider.orderId], 0, 100);
    this.loadingService.addToReferenceProgress(artists.length, discoProvider.orderId, refreshTimestamp);

    for (let i = 0; i < artists.length && this.loadingService.isLoadActive(refreshTimestamp); i++) {
      var listArtist = artists[i];
      try {
        var artistAlbums =
          await this.providerService.getServiceProvider(discoProvider.name).getAllArtistAlbums(listArtist);       
        let changed = listArtist.update(listArtist.lastListened, artistAlbums)
      } catch (err) {
        if (err instanceof Error && err.message === "Artist not found") {
          notFoundArtists.push(listArtist)
        }
      }
      this.loadingService.addToCurrentProgress(discoProvider.orderId, refreshTimestamp);
    }
    if (this.loadingService.isLoadActive(refreshTimestamp)) {
      this.storageService.saveDiscoArtists();
      return new RefreshResult(orderBy(artistsWithNewAlbums, 'artist.artist'), orderBy(notFoundArtists, 'artist.artist'));
    }
  }

  public async share() {
    try {
      var checked = await this.file.checkDir(this.file.dataDirectory, DiscographyServiceProvider.EXPORT_DIR)
    } catch (err) {
      var result = await this.file.createDir(this.file.dataDirectory, DiscographyServiceProvider.EXPORT_DIR, false)
    }
    var fileEntry: FileEntry = await this.utilsService.createFileExport(DiscographyServiceProvider.EXPORT_NAME, this.discoArtists,
      DiscographyServiceProvider.EXPORT_EXTENSION, DiscographyServiceProvider.EXPORT_DIR)
    try {
      var shareResult = await this.socialSharing.share('Follow these awesome artists', 'Followed on SurpriseMe', fileEntry.nativeURL);
      return 'Done';
    } finally {
      await new Promise(resolve => setTimeout(resolve, 1000));
      fileEntry.remove(() => { });
    }
  }

  public async import(): Promise<DiscoArtists> {
    var uri = await this.fileChooser.open()
    var resolvedFilePath = await this.filePath.resolveNativePath(uri)
    var resolvedPath = resolvedFilePath.substring(0, resolvedFilePath.lastIndexOf('/'))
    var resolvedFile = resolvedFilePath.substring(resolvedFilePath.lastIndexOf('/') + 1, resolvedFilePath.length)
    var content = await this.file.readAsText(resolvedPath, resolvedFile)
    var discoArtists: DiscoArtists = JSON.parse(content) as DiscoArtists;
    Object.setPrototypeOf(discoArtists, DiscoArtists.prototype);
    discoArtists.setPrototypes();
    this.discoArtists = discoArtists;
    this.storageService.discoArtists = discoArtists;
    this.storageService.saveDiscoArtists();
    return discoArtists;
  }

  private isSameArtist(basicArtist: BasicArtist, discoArtist: BasicDiscoArtist): boolean {
    return (
      discoArtist.artist.artist === basicArtist.artist ||
      discoArtist.artist.id.some(providerId =>
        basicArtist.id.some(providerId2 => providerId.id === providerId2.id && providerId.provider === providerId2.provider)
      )
    )
  }

  private async initialize() {
    this.discoArtists = await this.storageService.getDiscoArtists();
    this.initialized = true;
    this.resolveUninit.forEach(r => r(this.discoArtists));
  }

}
