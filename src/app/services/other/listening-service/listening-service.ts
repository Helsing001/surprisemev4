import { Injectable } from '@angular/core';
import { interval, Observable, Subscription, Subscriber } from 'rxjs';
import { ListenedTrack } from '../../../models/other/listened-track';
import { ProviderService } from '../provider-service/provider-service';
import { ProviderOperations } from '../../../models/providers/provider-operations';
import { LocalServiceProvider } from '../../main/local-service/local-service';

@Injectable()
export class ListeningServiceProvider {

    private apiDelay = 1000;
    private errorThreshold = 5;

    private timer: Subscription = undefined;
    private subscriber: Subscriber<ListenedTrack> = undefined;
    private track: ListenedTrack = undefined;
    private userAuth: boolean = true;
    private errorCount: number = 0;
    private betaWarningSent: boolean = false;

    constructor(private providerService: ProviderService) { }

    public startMonitoring(): Observable<ListenedTrack> {
        console.log("started monitoring");
        const observable: Observable<ListenedTrack> = new Observable(subscriber => {
            this.subscriber = subscriber;
        });

        var listeningProvider = undefined;
        if (this.providerService.getActiveProviderFor(ProviderOperations.LISTENING)) listeningProvider = this.providerService.getActiveProviderFor(ProviderOperations.LISTENING).name;
        var scrobbleProvider = this.providerService.getActiveProviderFor(ProviderOperations.SCROBBLE).name;
        this.authProvider(listeningProvider, ProviderOperations.LISTENING).then(() => {
            if (listeningProvider && scrobbleProvider !== listeningProvider) {
                this.authProvider(scrobbleProvider, ProviderOperations.SCROBBLE).then(() => {
                    this.timer = this.createTimer();
                });
            }
            else {
                this.timer = this.createTimer();
            }
        });

        return observable;
    }

    public stopMonitoring() {
        console.log("will stop monitoring");
        if (this.timer && !this.timer.closed) {
            this.timer.unsubscribe();
            this.subscriber.complete();
            this.track = undefined;
            this.userAuth = true;
            this.errorCount = 0;
            this.betaWarningSent = false;
        }
    }

    public createTimer() {
        return interval(this.apiDelay).subscribe((val) => {
            if (!this.userAuth) {
                this.subscriber.error({ header: "Please authenticate service", subheader: "Authenticate the listening and scrobbles provider in the Settings menu or when you open this page." });
            }
            else {
                if (this.providerService.getActiveProviderFor(ProviderOperations.LISTENING)) {
                    var provider = this.providerService.getActiveProviderFor(ProviderOperations.LISTENING).name;
                    if (provider === LocalServiceProvider.PROVIDER) {
                        if(this.providerService.getMusicApps().length == 0){
                            this.subscriber.error({ header: "Provider Error", subheader: "Please enable at least one music app in the Settings menu." });
                            return;
                        }
                        if (!this.betaWarningSent) {
                            this.betaWarningSent = true;
                            this.subscriber.error({ header: "Temporary Error", subheader: "The listening provider is in a beta phase, meaning it might not always work." });
                        }
                    }
                    this.providerService.getServiceProvider(this.providerService.getActiveProviderFor(ProviderOperations.LISTENING).name).fetchListening()
                        .then(track => {
                            if (this.isSameProvider(provider)) {
                                if (track) {
                                    if (track.scrobbles !== undefined && this.providerService.getActiveProviderFor(ProviderOperations.LISTENING).name === this.providerService.getActiveProviderFor(ProviderOperations.SCROBBLE).name) {
                                        this.sendNewTrack(track);
                                    }
                                    else {
                                        this.providerService.getServiceProvider(this.providerService.getActiveProviderFor(ProviderOperations.SCROBBLE).name).fetchTrackScrobbles(track.artist, track.track)
                                            .then(scrobbles => {
                                                if (this.isSameProvider(provider)) {
                                                    track.scrobbles = scrobbles;
                                                    this.sendNewTrack(track);
                                                }
                                            })
                                            .catch(e => {
                                                if (this.isSameProvider(provider)) {
                                                    console.log(e);
                                                    track.scrobbles = undefined;
                                                    this.sendNewTrack(track);
                                                }
                                            });
                                    }
                                }
                                else {
                                    this.sendNoTrack();
                                }
                            }
                        })
                        .catch(e => {
                            console.log(e);
                            switch (e) {
                                case "no permission": this.subscriber.error({ header: "Provider Error", subheader: "No notification access permission, please grant it in Android settings" }); break;
                                default:
                                    this.errorCount++;
                                    if (this.errorCount >= this.errorThreshold) {
                                        this.errorCount = 0;
                                        this.subscriber.error({ header: "Temporary Error", subheader: "The listening provider is unavailable at the moment." });
                                    }
                            }
                        });
                }
                else {
                    if (!this.timer.closed && this.subscriber) {
                        this.subscriber.error({ header: "No Listening Provider", subheader: "Enable a listening provider in the Settings menu." });
                    }
                }
            }
        });
    }

    public authProvider(provider: string, operation: string) {
        return new Promise((resolve, error) => {
            if (provider) {
                if (!this.providerService.getDefaultProvidersFor(operation).includes(provider) &&
                    !this.providerService.getProviderSettings(provider).getUsername()) {
                    this.providerService.getServiceProvider(provider).auth()
                        .then((success: boolean) => {
                            if (!success) {
                                this.userAuth = false;
                            }
                        })
                        .finally(() => resolve());
                }
                else {
                    resolve();
                }
            }
            else {
                resolve();
            }
        })
    }

    private isSameProvider(lastProvider: string) {
        return !this.timer.closed && lastProvider === this.providerService.getActiveProviderFor(ProviderOperations.LISTENING).name;
    }

    private shouldSendNewTrack(oldTrack: ListenedTrack, newTrack: ListenedTrack): boolean {
        if ((!oldTrack && newTrack) ||
            oldTrack.artist.artist !== newTrack.artist.artist ||
            oldTrack.track !== newTrack.track ||
            oldTrack.nowPlaying !== newTrack.nowPlaying ||
            oldTrack.scrobbles !== newTrack.scrobbles ||
            oldTrack.loved !== newTrack.loved) {
            return true;
        }
        else {
            return false;
        }
    }

    private sendNewTrack(track) {
        if (this.shouldSendNewTrack(this.track, track)) {
            this.track = track;
            if (this.subscriber) {
                this.subscriber.next(track);
            }
        }
    }

    private sendNoTrack() {
        if (this.subscriber) {
            this.subscriber.next(undefined);
        }
    }

}