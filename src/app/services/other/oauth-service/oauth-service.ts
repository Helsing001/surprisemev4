import { Injectable } from '@angular/core';
import { SpotifyServiceProvider } from '../../main/spotify-service/spotify-service';
import { DeezerServiceProvider } from '../../main/deezer-service/deezer-service';

@Injectable()
export class OauthServiceProvider {

    constructor(public spotifyServiceProvider: SpotifyServiceProvider, public deezerServiceProvider: DeezerServiceProvider){}

    public handleOAuthResponse(url: string) {
        if(url.includes("surpriseme://callback")){
          var matches = url.match("^.*access_token=(.*?)&.*expires_in=(.*?)&.*");
          this.spotifyServiceProvider.handleAuthResponse(matches[1], Number(matches[2]));
        }
        else if(url.includes("deezer")){
          var matches = url.match("^.*access_token=(.*?)&.*expires=(.*?)$");
          this.deezerServiceProvider.handleAuthResponse(matches[1], Number(matches[2]));          
        }
        else {
          console.error("invalid callback url: " + url);
        }
    }

}