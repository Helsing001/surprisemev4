import { BasicArtist } from "./basic-artist";
import { ProviderId } from "./basic-artist";
import { SimilarArtist } from './similar-artist';

export class FullArtist extends BasicArtist {

    constructor(artist: string, weight: number, public id: Array<ProviderId>, public iconSmall: string, public iconLarge,
        public genres: Array<string>, public description: string, public listeners: number, public playcount: number,
        public url: string, public sourceArtists: Array<BasicArtist>, public providers: Array<ProviderId>) {
        super(artist, weight, id);
    }

    public static fromBasicArtist(basicArtist: BasicArtist): FullArtist {
        return new FullArtist(basicArtist.artist, basicArtist.weight, basicArtist.id, undefined, undefined, undefined,
            undefined, undefined, undefined, undefined, undefined, undefined);
    }

    public static fromSimilarArtist(similarArtist: SimilarArtist): FullArtist {
        return new FullArtist(similarArtist.artist, similarArtist.weight, similarArtist.id, undefined, undefined, undefined,
            undefined, undefined, undefined, undefined, similarArtist.sourceArtists, similarArtist.providers);
    }

    public mergeWithSimilarArtistInfo(similarArtist: SimilarArtist) {
      this.sourceArtists = similarArtist.sourceArtists;
      this.providers = similarArtist.providers;
    }

    public setPrototypes() {
        if (this.id) {
            this.id.forEach(id => Object.setPrototypeOf(id, ProviderId.prototype));
        }
        if (this.providers) {
            this.providers.forEach(id => Object.setPrototypeOf(id, ProviderId.prototype));
        }
        if (this.sourceArtists) {
            this.sourceArtists.forEach(artist => {
                artist = Object.setPrototypeOf(artist, BasicArtist.prototype);
                artist.setPrototypes();
            });
        }
    }

}