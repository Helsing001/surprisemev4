import { BasicArtist, ProviderId } from './basic-artist';
import { concat as lConcat, cloneDeep, groupBy, reduce, values, orderBy } from 'lodash';

export class SimilarArtist extends BasicArtist {

  constructor(public artist: string, public weight: number, public id: Array<ProviderId>, public sourceArtists: Array<BasicArtist>, public providers: Array<ProviderId>) {
    super(artist, weight, id);
  }

  public static fromBasicArtist(basicArtist: BasicArtist): SimilarArtist {
    return new SimilarArtist(basicArtist.artist, basicArtist.weight, basicArtist.id, undefined, undefined);
  }

  public static mergeSimilarWithAdd(firstArtists: Array<SimilarArtist>, secondArtists: Array<SimilarArtist>) {
    var c = cloneDeep(lConcat(firstArtists, secondArtists));

    var nameMap: Map<string, string> = new Map();
    c.forEach(s => {
      var lc = s.artist.toLowerCase();
      nameMap.set(lc, s.artist);
      s.artist = lc;
    })

    var g = groupBy(c, 'artist');
    var r = reduce(g, (result, value: Array<SimilarArtist>, key: string) => {
      var finalArtist: SimilarArtist;
      value.forEach(artist => {
        if (!finalArtist) {
          finalArtist = artist;
        }
        else {
          finalArtist.weight = Number(finalArtist.weight) + Number(artist.weight);
          artist.id.forEach((providerId: ProviderId) => {
            finalArtist.addProvider(providerId);
          })
          artist.providers.forEach((providerId: ProviderId) => {
            finalArtist.addRecProvider(providerId);
          })
          finalArtist.sourceArtists = BasicArtist.mergeListenedWithAdd2(finalArtist.sourceArtists, artist.sourceArtists);
        }
      })
      finalArtist.sourceArtists = finalArtist.sourceArtists.slice(0, 5);
      result[key] = finalArtist;
      return result;
    }, {});
    var v = values(r);
    var o = orderBy(v, 'weight', 'desc') as Array<SimilarArtist>;
    o.forEach(s => s.artist = nameMap.get(s.artist));

    return o;
  }

  public static sortByWeight(recArtists: Array<SimilarArtist>): Array<SimilarArtist> {
    return recArtists.sort((a, b) => {
      if (a.weight < b.weight) {
        return 1;
      }
      else if (a.weight > b.weight) {
        return -1;
      }
      else {
        return 0;
      }
    });
  }

  public insertSourceArtist(sourceArtist: BasicArtist, score: number) {
    var addedArtist = new BasicArtist(sourceArtist.artist, score, sourceArtist.id);
    for (let k = 0; k < this.sourceArtists.length && k < 5; k++) {
      if (score > this.sourceArtists[k].weight) {
        this.sourceArtists.splice(k, 0, addedArtist);
        if (this.sourceArtists.length > 5) this.sourceArtists.pop();
        return;
      }
    }
    if (this.sourceArtists.length < 5) this.sourceArtists.push(addedArtist);
  }

  public getIdForRecProvider(provider: string) {
    var providerId = this.providers.find(providerId => providerId.provider == provider);
    if (providerId) {
      return providerId.id;
    }
    else {
      return undefined;
    }
  }

  public addRecProvider(providerId: ProviderId) {
    if (!this.getIdForRecProvider(providerId.provider)) {
      this.providers.push(providerId);
    }
  }

}