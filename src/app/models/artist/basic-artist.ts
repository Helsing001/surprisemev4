import { concat as lConcat, groupBy, reduce, values, orderBy, cloneDeep } from 'lodash';

export class BasicArtist {

    constructor(public artist: string, public weight: number, public id: Array<ProviderId>){
    }

    public static singleIdToArray(providerId : ProviderId) : Array<ProviderId> {
        var idList : Array<ProviderId> = new Array();
        idList.push(providerId);
        return idList;
    }

    public static mergeListenedWithAdd(c: Array<BasicArtist>) {
      var g = groupBy(c, 'artist');
      var r = reduce(g, function (result, value: Array<BasicArtist>, key: string) {
        var finalArtist = new BasicArtist(key, 0, new Array());
        value.forEach(artist => {
          finalArtist.weight = Number(finalArtist.weight) + Number(artist.weight);
          artist.id.forEach((providerId: ProviderId) => {
            finalArtist.addProvider(providerId);
          })
        })
        result[key] = finalArtist;
        return result;
      }, {});
      var v = values(r);
      var o = orderBy(v, 'weight', 'desc');
      return o as Array<BasicArtist>;
    }    

    public static mergeListenedWithAdd2(firstArtists: Array<BasicArtist>, secondArtists: Array<BasicArtist>) {
      var c = cloneDeep(lConcat(firstArtists, secondArtists));
  
      var nameMap: Map<string, string> = new Map();
      c.forEach(s => {
        var lc = s.artist.toLowerCase();
        nameMap.set(lc, s.artist);
        s.artist = lc;
      })
  
      var g = groupBy(c, 'artist');
      var r = reduce(g, function (result, value: Array<BasicArtist>, key: string) {
        var finalArtist = new BasicArtist(key, 0, new Array());
        value.forEach(artist => {
          finalArtist.weight = Number(finalArtist.weight) + Number(artist.weight);
          artist.id.forEach((providerId: ProviderId) => {
            finalArtist.addProvider(providerId);
          })
        })
        result[key] = finalArtist;
        return result;
      }, {});
      var v = values(r);
      var o = orderBy(v, 'weight', 'desc') as Array<BasicArtist>;
      o.forEach(s => s.artist = nameMap.get(s.artist));
      return o;
    }    

    public getIdForProvider(provider : string) {
        var providerId =  this.id.find(providerId => providerId.provider == provider);
        if(providerId){
            return providerId.id;
        }
        else{
            return undefined;
        }
    }

    public addProvider(providerId : ProviderId){
        if(!this.getIdForProvider(providerId.provider)) {
            this.id.push(providerId);
        }
    }  
    
    public setPrototypes(){
        if (this.id) {
            this.id.forEach(id => Object.setPrototypeOf(id, ProviderId.prototype));
        }        
    }

}

export class ProviderId {
    constructor(public provider: string, public id: string){
    }
}