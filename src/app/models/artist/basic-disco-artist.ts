import { BasicArtist } from './basic-artist';
import { ArtistAlbum } from '../album/artist-album';

export class BasicDiscoArtist {

  constructor(public artist: BasicArtist, public albumCount: number, public lastListened: Date, public latestAlbum: Date) {
  }

  public setPrototypes() {
    if (this.artist) {
      this.artist = Object.setPrototypeOf(this.artist, BasicArtist.prototype);
      this.artist.setPrototypes();
    }
    if (this.lastListened) {
      this.lastListened = new Date(Date.parse(this.lastListened as any));
    }
    if (this.latestAlbum) {
      this.latestAlbum = new Date(Date.parse(this.latestAlbum as any));
    }    
  }

  public update(newLastListened: Date, albums: Array<ArtistAlbum>): boolean {
    let changed = false;
    this.lastListened = newLastListened;
    let unlistenedAlbums = albums.filter(album => newLastListened == undefined || album.releaseDate.getTime() > newLastListened.getTime())
    let newAlbumCount = unlistenedAlbums.length;
    let newLatestAlbum = Math.min(...unlistenedAlbums.map(album => album.releaseDate ? album.releaseDate.getTime() : undefined))
    if(newLatestAlbum == Number.POSITIVE_INFINITY) newLatestAlbum = undefined;    
    if(this.albumCount != newAlbumCount) {
      this.albumCount = newAlbumCount;
      changed = true;
    }
    if((!this.latestAlbum && newLatestAlbum) || (this.latestAlbum && newLatestAlbum != this.latestAlbum.getTime())) {
      this.latestAlbum = newLatestAlbum ? new Date(newLatestAlbum) : undefined;
      changed = true;
    }     
    return changed;
  }

}
