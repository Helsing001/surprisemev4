import { FullArtist } from './full-artist';
import { ArtistAlbum } from '../album/artist-album';

export class ArtistExtraInfo {
  constructor(public similarArtists: Array<FullArtist>, public topAlbums: Array<ArtistAlbum>, public topSongs: Array<string>) { }
}
