import { BasicDiscoArtist } from '../artist/basic-disco-artist';

export class DiscoArtists {

  constructor(public artists: Array<BasicDiscoArtist>) {
  }

  public addDiscoArtist(discoArtist: BasicDiscoArtist) {
    this.artists.push(discoArtist);
  }

  public setPrototypes() {
    if (this.artists) {
      this.artists.forEach(artist => {
        artist = Object.setPrototypeOf(artist, BasicDiscoArtist.prototype);
        artist.setPrototypes();
      });
    }
  }

}
