import { ProviderService } from '../../services/other/provider-service/provider-service';
import { StorageServiceProvider } from '../../services/other/storage-service/storage-service';
import { MylistsServiceProvider } from '../../services/other/mylists-service/mylists-service';

//DE FOLOSIT doar dupa ce sunt sigur ca s-au creat toti providerii, doar cand am dep circulare si nu pot injecta
export class AllServices {

    public static providerService : ProviderService; 
    public static myListsService : MylistsServiceProvider;

}