import { BasicDiscoArtist } from '../artist/basic-disco-artist';

export class RefreshResult {

  constructor(public artistsWithNewAlbums: Array<BasicDiscoArtist>, public notFoundArtists: Array<BasicDiscoArtist>) { }

  public setPrototypes() {
    if (this.artistsWithNewAlbums) {
      this.artistsWithNewAlbums.forEach(a => {
        Object.setPrototypeOf(a, BasicDiscoArtist.prototype);
        a.setPrototypes();
      });
    }
    if (this.notFoundArtists) {
      this.notFoundArtists.forEach(a => {
        Object.setPrototypeOf(a, BasicDiscoArtist.prototype);
        a.setPrototypes();
      });
    }
  }

}