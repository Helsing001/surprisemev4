import { BasicArtist } from '../artist/basic-artist';

export class GenerateResult {

    constructor(public notFoundArtists: Array<BasicArtist>, public noSongsArtists: Array<BasicArtist>) { }

    public hasArtists() {
        if ((this.notFoundArtists && this.notFoundArtists.length > 0) || (this.noSongsArtists && this.noSongsArtists.length > 0)) return true;
        else return false;
    }

    public setPrototypes() {
        if (this.notFoundArtists) {
            this.notFoundArtists.forEach(a => {
                Object.setPrototypeOf(a, BasicArtist.prototype);
                a.setPrototypes();
            });
        }
        if (this.noSongsArtists) {
            this.noSongsArtists.forEach(a => {
                Object.setPrototypeOf(a, BasicArtist.prototype);
                a.setPrototypes();
            });
        }
    }

}