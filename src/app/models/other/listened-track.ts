import { BasicArtist } from '../artist/basic-artist';

export class ListenedTrack {
    constructor(public artist: BasicArtist, public album: string, public track: string, 
      public duration: number, public nowPlaying: boolean, public scrobbles: number, public image: string, public loved: boolean) { }
}
