import { CustomList } from './custom-list';

export class ExportedLists {

    public customLists: Array<CustomList> = new Array();

}