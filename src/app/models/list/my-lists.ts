import { CustomList } from "./custom-list";
import { BasicArtist } from "../artist/basic-artist";

export class MyLists {
    public customLists: Array<CustomList> = new Array();
    public favList: CustomList;
    public banList: CustomList;

    constructor(myLists: MyLists) {
        if (!myLists) {
            this.favList = new CustomList("Favorites", CustomList.FAVORITE, true);
            this.banList = new CustomList("Banned", CustomList.BANNED, true);
        }
        else{
            this.constructFullMyLists(myLists);
        }
    }

    private constructFullMyLists(myLists : MyLists){
        this.favList = this.constructFullCustomList(myLists.favList);
        this.banList = this.constructFullCustomList(myLists.banList);
        myLists.customLists.forEach((customList : CustomList) =>{
            this.customLists.push(this.constructFullCustomList(customList));
        });
    }

    public constructFullCustomList(customList : CustomList) : CustomList{
        var fullList = new CustomList(customList.listName, customList.listType, customList.partOfLibrary);
        if(customList.artwork) fullList.artwork = customList.artwork;
        var listArtistArray = customList.listArtists;
        listArtistArray = Object.setPrototypeOf(listArtistArray, Array.prototype);
        listArtistArray.forEach((listArtist : BasicArtist) => {
            var l = new BasicArtist(listArtist.artist, listArtist.weight, Object.setPrototypeOf(listArtist.id, Array.prototype));
            fullList.addToList(l, false);
        });
        return fullList;
    }

    public getAllLists() : Array<CustomList> {
        var allLists : Array<CustomList> = new Array();
        allLists.push(this.favList);
        allLists.push(this.banList);
        allLists.push(...this.customLists);
        return allLists;
    }
}