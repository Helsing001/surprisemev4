import { BasicArtist } from "../artist/basic-artist";
import { SearchServiceProvider } from '../../services/main/search-service/search-service';
import { ProviderService } from '../../services/other/provider-service/provider-service';
import { LastFmServiceProvider } from '../../services/main/last-fm-service/last-fm-service';
import { Provider } from '@angular/compiler/src/core';
import { AllServices } from '../other/all-services';
import { ProviderOperations } from '../providers/provider-operations';

export class CustomList {

    public static FAVORITE = "fav";
    public static BANNED = "ban";
    public static CUSTOM = "cus";

    public static DEFAULT_ARTWORK = "./assets/images/my-lists.png";

    public listArtists: Array<BasicArtist> = new Array();
    public artwork: string = CustomList.DEFAULT_ARTWORK;

    constructor(public listName: string, public listType: string, public partOfLibrary: boolean) { }

    public addToList(listArtist: BasicArtist, refreshArtwork: boolean = true) {
        this.listArtists.push(listArtist);
        if (refreshArtwork && this.listArtists.length == 1) {
            this.refreshArtwork();
        }
    }

    public removeFromList(artistName: string) {
        var firstArtistName = this.listArtists[0].artist;
        this.listArtists = this.listArtists.filter(artist => artist.artist !== artistName);
        if (firstArtistName === artistName) {
            this.refreshArtwork();
        }
    }

    public setList(listArtists: Array<BasicArtist>) {
        var firstArtistName = this.listArtists.length > 0 ? this.listArtists[0].artist : undefined;
        this.listArtists = listArtists;
        if (listArtists.length == 0 || firstArtistName !== listArtists[0].artist) {
            this.refreshArtwork();
        }
    }

    public refreshArtwork() {
        if (this.listArtists.length == 0) {
            this.artwork = CustomList.DEFAULT_ARTWORK;
            return;
        }
        var providerService = AllServices.providerService;
        var providerSettings = providerService.getActiveProviderFor(ProviderOperations.INFO);
        providerService.getServiceProvider(providerSettings.name).fetchArtistInfo(this.listArtists[0]).then(a => {
            if (a.iconLarge && !a.iconLarge.includes("lastfm")) {
                this.artwork = a.iconLarge;
                AllServices.myListsService.saveMyLists();
            }
        });
    }

    public setPrototypes() {
        if (this.listArtists) {
            this.listArtists.forEach(a => {
                Object.setPrototypeOf(a, BasicArtist.prototype);
                a.setPrototypes();
            })
        }
    }

}