import { SearchParams } from './search-params';

export class SearchTemplate {

    constructor(public name: string, public searchParams: SearchParams) {}

    public setPrototypes() {
        Object.setPrototypeOf(this.searchParams, SearchParams.prototype);
        this.searchParams.setPrototypes();
    }

}
