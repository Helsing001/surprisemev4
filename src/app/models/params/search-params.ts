import { ExternalList } from '../list/external-list';
import { DatePipe } from '@angular/common';

export class SearchParams {

    constructor(public genericParams: GenericParams, public lastFmParams: Array<LastFmParams>, public spotifyParams: Array<SpotifyParams>, 
        public localParams: Array<LocalParams>, public deezerParams: Array<DeezerParams>) {
        genericParams.timestamp = new Date().getTime();
    }

    public getSearchInfoMessage(datepipe: DatePipe): string {
        var message = this.genericParams.getGenericParamsDisplay();
        this.localParams.forEach(params => {
            message += params.getLocalParamsDisplay();
        });
        this.lastFmParams.forEach(params => {
            message += params.getLastmFmParamsDisplay(datepipe);
        });
        this.spotifyParams.forEach(params => {
            message += params.getSpotifyParamsDisplay();
        });
        this.deezerParams.forEach(params => {
            message += params.getDeezerParamsDisplay();
        });        
        return message;
    }

    public setPrototypes() {
        Object.setPrototypeOf(this.genericParams, GenericParams.prototype);
        this.lastFmParams.forEach(params => Object.setPrototypeOf(params, LastFmParams.prototype));
        this.localParams.forEach(params => Object.setPrototypeOf(params, LocalParams.prototype));
        this.spotifyParams.forEach(params => { Object.setPrototypeOf(params, SpotifyParams.prototype); params.setPrototypes(); });
        if(this.deezerParams) this.deezerParams.forEach(params => { Object.setPrototypeOf(params, DeezerParams.prototype); params.setPrototypes(); });
    }

}

export class GenericParams {

    constructor() { }

    public historyLimit: number = 10;
    public resultLimit: number = 50;
    public showSources: boolean = false;
    public ignoreLibrary: boolean = false;
    public excludeLibrary: boolean = false;
    public simpleSearch: boolean = false;
    public timestamp: number;

    public getGenericParamsDisplay(): string {
        var message = "<p>General</p>";
        message += "<ul>";
        if (!this.showSources) message += '<li>History Limit: ' + this.historyLimit + "</li>";
        message += '<li>Results Limit: ' + this.resultLimit + "</li>";
        message += '<li>Show Sources: ' + this.showSources + "</li>";
        message += '<li>Simple Search: ' + this.simpleSearch + "</li>";
        if (this.showSources) message += '<li>Exclude Library: ' + this.excludeLibrary + "</li>";
        else message += '<li>Ignore Library: ' + this.ignoreLibrary + "</li>";
        message += "</ul>";
        return message;
    }

}

export class CommonParams {
    constructor(public id: number, public weight: number) { }
}

export class LastFmParams extends CommonParams {

    constructor(public id: number, public weight: number) {
        super(id, weight);
    }

    public lastfmPeriod = "Overall";
    public from: number;
    public to: number;
    public tag: string = "";

    public getLastmFmParamsDisplay(datepipe: DatePipe): string {
        var message = "<p>Last.fm</p>"
        message += "<ul>";
        message += '<li>Weight: ' + this.weight + "</li>";
        message += '<li>' + this.lastfmPeriod;
        if (this.lastfmPeriod === "Tag") message += ": " + this.tag;
        if (this.lastfmPeriod === "Custom") message += ": " + this.getLastFmPeriodDisplay(datepipe, this.from, this.to);
        message += "</li></ul>";
        return message;
    }

    private getLastFmPeriodDisplay(datepipe: DatePipe, from, to) {
        if (from && to) {
            var dateFrom = new Date(from * 1000);
            var dateTo = new Date(to * 1000);
            return datepipe.transform(dateFrom, 'dd MMM yy') + "-" + datepipe.transform(dateTo, 'dd MMM yy');
        }
        else return "";
    }

}

export class SpotifyParams extends CommonParams {
    public spotifySource = "Followed Artists";
    public playlist: ExternalList;

    constructor(public id: number, public weight: number) {
        super(id, weight);
    }

    public getSpotifyParamsDisplay(): string {
        var message = "<p>Spotify</p>";
        message += "<ul>";
        message += '<li>Weight: ' + this.weight + "</li>";
        message += '<li>' + this.spotifySource;
        if (this.spotifySource === "Followed List") message += ": " + this.playlist.name;
        message += "</li></ul>";
        return message;
    }

    public setPrototypes() {
        if (this.playlist) {
            Object.setPrototypeOf(this.playlist, ExternalList.prototype);
        }
    }
}

export class LocalParams extends CommonParams {
    public sourceLists: Array<string> = ['Favorites'];

    constructor(public id: number, public weight: number) {
        super(id, weight);
    }

    public getLocalParamsDisplay(): string {
        var message = "<p>Local</p>";
        message += "<ul>";
        message += '<li>Weight: ' + this.weight + "</li>";
        this.sourceLists.forEach(list => message += '<li>' + list + "</li>");
        message += "</ul>";
        return message;
    }

}

export class DeezerParams extends CommonParams {
    public deezerSource = "Followed Artists";
    public playlist: ExternalList;

    constructor(public id: number, public weight: number) {
        super(id, weight);
    }

    public getDeezerParamsDisplay(): string {
        var message = "<p>Deezer</p>";
        message += "<ul>";
        message += '<li>Weight: ' + this.weight + "</li>";
        message += '<li>' + this.deezerSource;
        if (this.deezerSource === "Followed List") message += ": " + this.playlist.name;
        message += "</li></ul>";
        return message;
    }

    public setPrototypes() {
        if (this.playlist) {
            Object.setPrototypeOf(this.playlist, ExternalList.prototype);
        }
    }
}