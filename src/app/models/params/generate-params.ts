import { CustomList } from '../list/custom-list';
import { ExternalList } from '../list/external-list';

export class GenerateParams {

    public timestamp: number;

    constructor(public customList: CustomList, public playlistName: string, public privatePlaylist: boolean,
        public description: string, public songsPerArtist: number, public songsToChooseFrom: number, public shuffle: boolean,
        public listToAddTo: ExternalList, public provider: string) {
        this.timestamp = new Date().getTime();
    }

    public getGenerateInfoMessage(): string {
        var message = "<p>Generate</p>";
        message += "<ul>";
        message += '<li>Provider: ' + this.provider + "</li>";
        message += '<li>Source List: ' + this.customList.listName + "</li>";
        message += '<li>Playlist: ' + this.playlistName + "</li>";
        message += '<li>Private: ' + this.privatePlaylist + "</li>";
        message += '<li>Shuffle: ' + this.shuffle + "</li>";
        message += '<li>Per Artist: ' + this.songsPerArtist + "</li>";
        message += '<li>From top: ' + this.songsToChooseFrom + "</li>";
        message += "</ul>";
        return message;
    }

    public setPrototypes() {
        if (this.customList) {
            Object.setPrototypeOf(this.customList, CustomList.prototype);
            this.customList.setPrototypes();
        }
        if (this.listToAddTo) {
            Object.setPrototypeOf(this.listToAddTo, ExternalList.prototype);
        }
    }

}