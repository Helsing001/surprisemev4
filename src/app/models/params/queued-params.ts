import { SearchParams } from './search-params';
import { GenerateParams } from './generate-params';
import { FullArtist } from '../artist/full-artist';
import { GenerateResult } from '../other/generate-result';
import { ProviderId, BasicArtist } from '../artist/basic-artist';
import { BasicDiscoArtist } from '../artist/basic-disco-artist';
import { RefreshParams } from './refresh-params';
import { RefreshResult } from '../other/refresh-result';

export class QueuedParams {

  public static TYPE_SEARCH: string = "s";
  public static TYPE_GENERATE: string = "g";
  public static TYPE_REFRESH: string = "r";

  public searchParams: SearchParams;
  public generateParams: GenerateParams;
  public refreshParams: RefreshParams;

  constructor(public type: any, private params: any, public resolve, public error) {
    switch (type) {
      case QueuedParams.TYPE_SEARCH: this.searchParams = params; break;
      case QueuedParams.TYPE_GENERATE: this.generateParams = params; break;
      case QueuedParams.TYPE_REFRESH: this.refreshParams = params; break;
    }
  }

  public setPrototypes() {
    if (this.searchParams) {
      Object.setPrototypeOf(this.searchParams, SearchParams.prototype);
      this.searchParams.setPrototypes();
    }
    if (this.generateParams) {
      Object.setPrototypeOf(this.generateParams, GenerateParams.prototype);
      this.generateParams.setPrototypes();
    }
    if (this.refreshParams) {
      Object.setPrototypeOf(this.refreshParams, RefreshParams.prototype);
      this.refreshParams.setPrototypes();
    }
  }

}

export class HistoryResult {
  public searchResults: Array<FullArtist>;
  public generateResult: GenerateResult;
  public refreshResult: RefreshResult;

  constructor(public type: any, public queueParams: QueuedParams, private results: any) {
    switch (type) {
      case QueuedParams.TYPE_SEARCH: this.searchResults = results; break;
      case QueuedParams.TYPE_GENERATE: this.generateResult = results; break;
      case QueuedParams.TYPE_REFRESH: this.refreshResult = results; break;
    }
    queueParams.error = undefined;
    queueParams.resolve = undefined;
    results = undefined;
  }

  public setPrototypes() {
    if (this.searchResults) {
      this.searchResults.forEach(a => {
        Object.setPrototypeOf(a, FullArtist.prototype);
        a.setPrototypes();
      });
    }
    if (this.generateResult) {
      Object.setPrototypeOf(this.generateResult, GenerateResult.prototype);
      this.generateResult.setPrototypes();
    }
    if (this.refreshResult) {
      Object.setPrototypeOf(this.refreshResult, RefreshResult.prototype);
      this.refreshResult.setPrototypes();
    }
    if (this.queueParams) {
      Object.setPrototypeOf(this.queueParams, QueuedParams.prototype);
      this.queueParams.setPrototypes();
    }
  }

}