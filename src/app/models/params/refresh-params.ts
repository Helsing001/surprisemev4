import { BasicDiscoArtist } from '../artist/basic-disco-artist';

export class RefreshParams {

  public timestamp: number;

  constructor(public artists: Array<BasicDiscoArtist>) {
    this.timestamp = new Date().getTime();
  }

  public getGenerateInfoMessage(): string {
    var message = "<p>Refreshing discography for " + this.artists.length + " artists</p>";
    return message;
  }

  public setPrototypes() {
    this.artists.forEach(artist => {
      Object.setPrototypeOf(artist, BasicDiscoArtist.prototype);
      artist.setPrototypes();
    })
  }

}
