import { BasicArtist } from "../artist/basic-artist";
import { FullArtist } from "../artist/full-artist";
import { ProviderSettings } from "../providers/provider-settings";
import { ArtistExtraInfo } from '../artist/artist-extra-info';
import { SimilarArtist } from '../artist/similar-artist';
import { GenerateParams } from '../params/generate-params';
import { GenerateResult } from '../other/generate-result';
import { ListenedTrack } from '../other/listened-track';
import { ExternalList } from '../list/external-list';
import { GenericParams } from '../params/search-params';
import { ArtistAlbum } from '../album/artist-album';
import { BasicDiscoArtist } from '../artist/basic-disco-artist';

export abstract class GenericService {

    abstract getOwnName() : string;
    abstract auth() : Promise<boolean>;
    abstract forgetAuth();
    abstract isLongAuth() : boolean;
    abstract fetchUserArtists(genericParams: GenericParams) : Promise<Array<BasicArtist>>;
    abstract fetchSourceArtists(params : any, genericParams: GenericParams) : Promise<Array<BasicArtist>>;
    abstract fetchSimilarArtists(userArtists: Array<BasicArtist>, sourceArtists: Array<BasicArtist>, genericParams: GenericParams) : Promise<Array<SimilarArtist>>;
    abstract fetchSimilarArtistsInfo(recArtists: Array<SimilarArtist>, genericParams: GenericParams) : Promise<Array<FullArtist>>;
    abstract fetchArtistInfo(artist: BasicArtist) : Promise<FullArtist>;
    abstract searchArtist(artistName : string) : Promise<Array<FullArtist>>;
    abstract fetchArtistExtraInfo(basicArtist: BasicArtist) : Promise<ArtistExtraInfo>;
    abstract fetchAlbumsForClarification(basicArtist: BasicArtist): Promise<Array<ArtistAlbum>>;
    abstract fetchArtistScrobbles(basicArtist: BasicArtist) : Promise<number>;
    abstract fetchTrackScrobbles(basicArtist: BasicArtist, track: string) : Promise<number>;
    abstract fetchListening() : Promise<ListenedTrack>;
    abstract fetchPrivateLists() : Promise<Array<ExternalList>>;
    abstract generatePlaylist(generateParams: GenerateParams) : Promise<GenerateResult>;
    abstract getProviderSettings() : ProviderSettings;
    abstract getDefaultOperationsWithoutAuth() : Array<string>;
    abstract getProviderImage(enabled : boolean) : string;
    abstract getApiDelay() : number;
    abstract getAllArtistAlbums(discoArtist: BasicDiscoArtist): Promise<Array<ArtistAlbum>>

}