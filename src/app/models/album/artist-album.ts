export class ArtistAlbum {
  constructor(public album: string, public image: string, public albumId: string = undefined, public releaseDate: Date = undefined) { }

  public static mergeByName(arr: Array<ArtistAlbum>): Array<ArtistAlbum> {
    return arr.map(e => e.album)
      .map((e, i, final) => final.indexOf(e) === i && i)
      .filter((e) => arr[e]).map(e => arr[e]);
  }

}
