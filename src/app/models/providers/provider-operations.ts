export class ProviderOperations {

    public static LIBRARY : string = "library";
    public static SOURCE : string = "source";
    public static SIMILARITY : string = "similarity";
    public static INFO : string = "info";
    public static GENERATE : string = "generate";
    public static SCROBBLE : string = "scrobble";
    public static LISTENING : string = "listening";
    public static DISCOGRAPHY : string = "discography";

}