import { StorageServiceProvider } from "../../services/other/storage-service/storage-service";

export class ProviderSettings {

    public readonly orderId : number;
    public readonly name : string;
    public readonly library :  boolean;
    public readonly similarity :  boolean;
    public readonly info :  boolean;
    public readonly alwaysEnabled :  boolean;
    public generate : boolean;
    public shouldLogIn : boolean;
    public source : boolean;
    public scrobble : boolean;
    public listening : boolean;
    public discography : boolean;

    private username: string;  
    private sourceWeight: number = 50;
    private similarityWeight: number = 50;

    constructor (orderId : number, name : string, library : boolean, similarity : boolean, info : boolean, generate : boolean, 
                alwaysEnabled : boolean, shouldLogIn : boolean, source : boolean, scrobble : boolean, listening : boolean, discography: boolean){  
        this.orderId = orderId;
        this.name = name;
        this.library = library;
        this.similarity = similarity;
        this.info = info;
        this.generate = generate;
        this.alwaysEnabled = alwaysEnabled;
        this.shouldLogIn = shouldLogIn;
        this.source = source;
        this.scrobble = scrobble;
        this.listening = listening;
        this.discography = discography;
    }

    public resetWeights(storageService : StorageServiceProvider){
        this.sourceWeight = 50;
        this.similarityWeight = 50;    
    }

    public getUsername () {  
		return this.username;
	}

    public setUsername (storageService : StorageServiceProvider, username : string) {
        this.username = username;
        storageService.setProviderSettings(this.name, this);         
    }

    public getSourceWeight() {
        return this.sourceWeight;
    }

    public setSourceWeight(storageService : StorageServiceProvider, sourceWeight : number) { 
        this.sourceWeight = sourceWeight;
        storageService.setProviderSettings(this.name, this);        
    }

    public getSimilarityWeight() {
        return this.similarityWeight;
    }

    public setSimilarityWeight(storageService : StorageServiceProvider, similarityWeight : number) {
        this.similarityWeight = similarityWeight;
        storageService.setProviderSettings(this.name, this);        
    }  
    
    public setGenerate(storageService : StorageServiceProvider, generate : boolean) {
        this.generate = generate;
        storageService.setProviderSettings(this.name, this);        
    }    
    
    public setShouldLogIn(storageService : StorageServiceProvider, shouldLogIn : boolean) {
        this.shouldLogIn = shouldLogIn;
        storageService.setProviderSettings(this.name, this);        
    }      

    public setSource(storageService : StorageServiceProvider, source : boolean) {
        this.source = source;
        storageService.setProviderSettings(this.name, this);        
    }        

    public setScrobble(storageService : StorageServiceProvider, scrobble : boolean) {
        this.scrobble = scrobble;
        storageService.setProviderSettings(this.name, this);        
    }   
    
    public setListening(storageService : StorageServiceProvider, listening : boolean) {
        this.listening = listening;
        storageService.setProviderSettings(this.name, this);        
    }      

    public setDiscography(storageService : StorageServiceProvider, discography : boolean) {
      this.discography = discography;
      storageService.setProviderSettings(this.name, this);        
  }      

}