import { Pipe, PipeTransform } from '@angular/core';
import { FullArtist } from '../../models/artist/full-artist';

/**
 * Generated class for the GenreFilterPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'genreFilter',
})
export class GenreFilterPipe implements PipeTransform {
  /**
   * Takes a value and makes it lowercase.
   */
  transform(items: FullArtist[], filter: string): FullArtist[] {
    if (!items || !filter) {
      return items;
    }
    // filter items array, items which match and return true will be kept, false will be filtered out
    return items.filter((artist: FullArtist) => artist.genres && artist.genres.find(genre => genre.indexOf(filter.toLocaleLowerCase()) != -1));//artist.genres.indexOf(filter) !== -1);
  }
}
