import { NgModule } from '@angular/core';
import { GenreFilterPipe } from './genre-filter/genre-filter';
import { ThousandSuffixesPipePipe } from './genre-filter/thousand-suffixes-pipe';
@NgModule({
	declarations: [GenreFilterPipe, ThousandSuffixesPipePipe],
	imports: [],
	exports: [GenreFilterPipe, ThousandSuffixesPipePipe]
})
export class PipesModule {}
