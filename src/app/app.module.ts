import { NgModule } from '@angular/core';
import { BrowserModule, HAMMER_GESTURE_CONFIG, HammerGestureConfig } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { LastFmServiceProvider } from './services/main/last-fm-service/last-fm-service';
import { StorageServiceProvider } from './services/other/storage-service/storage-service';
import { SpotifyServiceProvider } from './services/main/spotify-service/spotify-service';
import { SearchServiceProvider } from './services/main/search-service/search-service';
import { MylistsServiceProvider } from './services/other/mylists-service/mylists-service';
import { LocalServiceProvider } from './services/main/local-service/local-service';
import { ProviderService } from './services/other/provider-service/provider-service';
import { IonicStorageModule } from '@ionic/storage';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { ArtistInfoPopoverPage } from './pages/popover/artist-info-popover/artist-info-popover';
import { RecListPopoverPage } from './pages/popover/rec-list-popover/rec-list-popover';
import { MyListPopoverPage } from './pages/popover/my-list-popover/my-list-popover';
import { LoadingServiceProvider } from './services/other/loading-service/loading-service';
import { ShellModule } from './shell/shell.module';
import { AspectRatioComponent } from './shell/aspect-ratio/aspect-ratio.component';
import { File } from '@ionic-native/file/ngx';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { MyListsPopoverPage } from './pages/popover/my-lists-popover/my-lists-popover.page';
import { FileChooser } from '@ionic-native/file-chooser/ngx';
import { FilePath } from '@ionic-native/file-path/ngx';
import { CalendarModule } from 'ion2-calendar';
import { TheAudioDbServiceProvider } from './services/other/theaudiodb-service/theuadiodb-service';
import { GeneratePlaylistModal } from './pages/secondary/generate-playlist/generate-playlist.page';
import { ListeningServiceProvider } from './services/other/listening-service/listening-service';
import { Insomnia } from '@ionic-native/insomnia/ngx';
import { CloudSettings } from '@ionic-native/cloud-settings/ngx';
import { LoadingQueueServiceProvider } from './services/other/loading-queue-service/loading-queue-service';
import { NgCircleProgressModule } from 'ng-circle-progress';
import { QueuedResultsPopoverPage } from './pages/popover/queued-results-popover/queued-results-popover.page';
import { DataServiceProvider } from './services/other/data-service/data-service';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { OauthServiceProvider } from './services/other/oauth-service/oauth-service';
import { Market } from '@ionic-native/market/ngx';
import { ArtistInfoPopoverPageModule } from './pages/popover/artist-info-popover/artist-info-popover.module';
import { MyListPopoverPageModule } from './pages/popover/my-list-popover/my-list-popover.module';
import { MyListsPopoverPageModule } from './pages/popover/my-lists-popover/my-lists-popover.module';
import { RecListPopoverPageModule } from './pages/popover/rec-list-popover/rec-list-popover.module';
import { QueuedResultsPopoverPageModule } from './pages/popover/queued-results-popover/queued-results-popover.module';
import { GeneratePlaylistPageModule } from './pages/secondary/generate-playlist/generate-playlist.module';
import { DeezerServiceProvider } from './services/main/deezer-service/deezer-service';
import { Deeplinks } from '@ionic-native/deeplinks/ngx';
import { HTTP } from '@ionic-native/http/ngx';
import { HttpServiceProvider } from './services/other/http-service/http-service';
import { UtilsServiceProvider } from './services/other/utils-service/utils-service';
import { DiscographyServiceProvider } from './services/other/discography-service/discography-service';
import { FollowedPopoverPageModule } from './pages/popover/followed-popover/followed-popover.module';
import { ArtistAlbumsPageModule } from './pages/secondary/artist-albums/artist-albums.module';

export class CustomHammerConfig extends HammerGestureConfig {
  overrides = {
      'press': { time: 1000 },  //set press delay for 1 second
      'pinch': { enable: false },
      'rotate': { enable: false }
  }
}
@NgModule({
  declarations: [
    AppComponent
  ],
  entryComponents: [
   ],
  imports: [
    BrowserModule, 
    FormsModule,
    HttpModule, //HttpClientModule e recomandat
    NgCircleProgressModule.forRoot({
      backgroundStrokeWidth: 0,
      backgroundPadding: 7,
      space: -3,
      toFixed: 0,
      outerStrokeWidth: 4,
      outerStrokeColor: '#808080',
      innerStrokeWidth: 2,
      innerStrokeColor: '#e7e8ea',
      animationDuration: 500,
      animation: true,
      startFromZero: false,
      responsive: true,
      showUnits: true,
      showTitle: true,
      showSubtitle: false,
      showImage: false,
      renderOnClick: false,
      titleFontSize: "60",
      subtitleFontSize: "25"
    }),    
    IonicModule.forRoot(),
    AppRoutingModule,
    ShellModule,
    IonicStorageModule.forRoot(),
    CalendarModule,
    ArtistInfoPopoverPageModule,
    MyListPopoverPageModule,
    MyListsPopoverPageModule,
    RecListPopoverPageModule,
    QueuedResultsPopoverPageModule,
    FollowedPopoverPageModule,
    GeneratePlaylistPageModule,
    ArtistAlbumsPageModule
  ],
  providers: [
    StatusBar,
    SplashScreen,
    File,
    FileChooser,
    FilePath, 
    SocialSharing,
    Insomnia,
    CloudSettings,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    LastFmServiceProvider,
    StorageServiceProvider,
    SpotifyServiceProvider,
    DeezerServiceProvider,
    SearchServiceProvider,
    MylistsServiceProvider,
    LocalServiceProvider,
    LoadingServiceProvider,
    TheAudioDbServiceProvider,
    ListeningServiceProvider,
    LoadingQueueServiceProvider,
    DataServiceProvider,
    OauthServiceProvider,
    HttpServiceProvider,
    UtilsServiceProvider,
    DiscographyServiceProvider,
    { provide: HAMMER_GESTURE_CONFIG, useClass: CustomHammerConfig },
    ProviderService,
    InAppBrowser,
    Market,
    Deeplinks,
    HTTP
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
