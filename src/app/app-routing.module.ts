import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { 
    path: '',
    redirectTo: 'surprise-me-page',
    pathMatch: 'full'
  },
  {
    path: 'surprise-me-page',
    loadChildren: () => import('./pages/main/surprise-me/surprise-me.module.').then(m => m.SupriseMePageModule)
  },
  {
    path: 'my-lists-page',
    loadChildren: () => import('./pages/main/my-lists/my-lists.module').then(m => m.MyListsPageModule)
  },  
  {
    path: 'search-page',
    loadChildren: () => import('./pages/main/search/search.module').then(m => m.SearchPageModule)
  },  
  {
    path: 'settings-page',
    loadChildren: () => import('./pages/main/settings/settings.module').then(m => m.SettingsPageodule)
  },  
  {
    path: 'my-list-artists-page',
    loadChildren: () => import('./pages/secondary/my-list-artists/my-list-artists.module').then(m => m.MyListArtistsPageModule)
  }, 
  {
    path: 'loading-page',
    loadChildren: () => import('./pages/secondary/loading/loading.module').then(m => m.LoadingPageModule)
  }, 
  {
    path: 'rec-results-page',
    loadChildren: () => import('./pages/secondary/rec-results/rec-results.module').then(m => m.RecResultsPageModule)
  },   
  {
    path: 'artist-info-page',
    loadChildren: () => import('./pages/secondary/artist-info/artist-info.module').then(m => m.ArtistInfoPageModule)
  },
  {
    path: 'getting-started-page',
    loadChildren: () => import('./pages/main/getting-started/getting-started.module').then( m => m.GettingStartedPageModule)
  },
  {
    path: 'queued-results-page',
    loadChildren: () => import('./pages/main/queued-results/queued-results.module').then( m => m.QueuedResultsPageModule)
  },
  {
    path: 'support-me-page',
    loadChildren: () => import('./pages/main/support-me/support-me.module').then( m => m.SupportMePageModule)
  },
  {
    path: 'followed',
    loadChildren: () => import('./pages/main/followed/followed.module').then( m => m.FollowedPageModule)
  }  
]; 

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
}) 
export class AppRoutingModule {}
